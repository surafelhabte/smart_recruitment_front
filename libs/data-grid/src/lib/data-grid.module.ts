import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './data-table/data-table.component';
import {
  PageService,
  SortService,
  FilterService,
  GroupService,
  GridModule,
  ReorderService,
  ResizeService,
  ToolbarService,
  SearchService,
  CommandColumnService,
  EditService,
  ColumnChooserService,
  ExcelExportService
} from '@syncfusion/ej2-angular-grids';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    GridModule,
    TextBoxModule,
    DialogModule,
    CheckBoxModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [DataTableComponent],
  exports: [DataTableComponent],
  providers: [
    ExcelExportService,
    PageService,
    SortService,
    FilterService,
    GroupService,
    ReorderService,
    ResizeService,
    ToolbarService,
    SearchService,
    CommandColumnService,
    EditService,
    ColumnChooserService
  ]
})
export class DataGridModule { }
