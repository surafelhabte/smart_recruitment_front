import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  EventEmitter,
  Output,
  OnChanges
} from '@angular/core';
import {
  PageSettingsModel,
  EditSettingsModel,
  TextWrapSettingsModel,
  SearchSettingsModel,
  CommandModel,
  IRow,
  Column,
  GridModel,
  BooleanEditCell
} from '@syncfusion/ej2-grids';
import { GridComponent, SelectionSettingsModel, QueryCellInfoEventArgs } from '@syncfusion/ej2-angular-grids';
import { EmitType, closest } from '@syncfusion/ej2-base';
import { ClickEventArgs } from '@syncfusion/ej2-navigations/src/toolbar';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { Router } from '@angular/router';
import { SecurityService } from 'apps/admin/src/Core/Security/security-service';

@Component({
  selector: 'smart-recruitment-frontend-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit,OnChanges {
  @Input() gridData: any;
  @Input() source: any;
  @Input() allowOperation: any;
  @Input() allowToolbar: any;
  @Input() searchField: any;
  @Input() incomingToolbar: any;
  @Input() incomingCommand: any;
  @Input() columens: any[] = [];
  @Input() showCheckBox: any;
  @Input() childGrid: GridModel;
  @Input() grouped: Boolean;
  @Input() groupOptions: any;
  @Input() height: any;
  @Input() showColumnChooser: Boolean;
  @Input() allowExporttoExcel: Boolean;
  @Input() addPrivilage: string;
  @Input() editPrivilage: string;
  @Input() deletePrivilage: string;
  @Input() showAdd = false;
  @Input() showExcelExport = false;
  @Input() showPrint = false;
  @Input() showSearch = false;
  @Input() showFilter = false;
  @Input() showCollapseAndExpand = false;
  @Input() allowCheckbox = false;


  @Output()
  public addRecord: EventEmitter<any> = new EventEmitter();
  @Output()
  public editRecord: EventEmitter<any> = new EventEmitter();
  @Output()
  public deleteRecord: EventEmitter<any> = new EventEmitter();
  @Output()
  public detailRecord: EventEmitter<any> = new EventEmitter();
  @Output()
  public selectRecord: EventEmitter<any> = new EventEmitter();

  public currentDeletingItem: any;
  public selection: any[] = [];

  public pageSettings: PageSettingsModel;
  public toolbar: Array<any>;
  public searchOptions: SearchSettingsModel;

  public wrapSettings: TextWrapSettingsModel;
  public editSettings: EditSettingsModel;
  public commands: CommandModel[] = [];
  public onfilter = false;

  @ViewChild('grid', { static: false }) public grid: GridComponent;
  @ViewChild('ejDialog', { static: true }) ejDialog: DialogComponent;
  @ViewChild('container', { static: true }) container: ElementRef;
  public targetElement: HTMLElement;
  public animationSettings: any = { effect: 'Zoom', duration: 400, delay: 0 };
  public pageSizes = ['10', '20', '50', '100', '200', '500', '1000', 'All'];
  public selectionOptions: SelectionSettingsModel = {checkboxOnly: true };

  public img_src_female = "../../../../../apps/admin/src/assets/no_photo_female.png";
  public img_src_male = "../../../../../apps/admin/src/assets/no_photo_male.png";

  constructor(private router?: Router,private securityService?: SecurityService) {}

  public initilaizeTarget: EmitType<object> = () => {
    this.targetElement = this.container.nativeElement.parentElement;
    this.ejDialog.content = 'Are you Sure Want to Delete ? ';
  };

  ngOnInit() {
    this.pageSettings = { pageSize: 10, pageSizes: this.pageSizes };
    this.wrapSettings = { wrapMode: 'Content' };
    this.toolbar = this.incomingToolbar;
    this.searchOptions = { fields: [this.searchField],operator: 'contains',key: '',ignoreCase: true};
    this.editSettings = { allowAdding: true };

    if(this.incomingCommand !== undefined){
			this.incomingCommand.forEach(element => {
				switch(element.toString()){
					case 'detail':
							this.commands.push(
							{
                type: 'Edit', 
                buttonOption: { cssClass: 'e-flat',iconCss: 'e-MT_Preview e-icons',click: this.onDetail.bind(this) }
							});			
						
					break;

					case 'edit' :
							this.commands.push(
							{
                type: 'Edit',
                buttonOption: { cssClass: 'e-flat',iconCss: 'e-edit e-icons',click: this.onEdit.bind(this) }
							});
						
					break;

					case 'delete' :
            this.commands.push(
            {
              type: 'Delete',
              buttonOption: { cssClass: 'e-flat',iconCss: 'e-delete e-icons',click: this.onDelete.bind(this) }
            });	
					break;

				}
	
			});
		}

    this.initilaizeTarget();
    this.initializeToolBar();

  }

  ngOnChanges(){
    this.grid ? this.grid.refresh(): '';
  }

  initializeToolBar(): void {
    this.toolbar = [];
    if (this.showAdd && this.securityService.hasClaim(this.addPrivilage)) {
      this.toolbar.push({
        text: 'Add',
        prefixIcon: 'e-create'
      });
    }
    if (this.showFilter) {
      this.toolbar.push({
        text: 'Filter',
        prefixIcon: 'e-BT_Excelfilter'
      });
    }
    if (this.showExcelExport) {
      this.toolbar.push({
        text: 'ExcelExport',
        prefixIcon: 'e-Excel_Export'
      });
    }
    if (this.showPrint) {
      this.toolbar.push({
        text: 'Print',
        prefixIcon: 'e-print'
      });
    }

    if (this.showSearch) {
      this.toolbar.push({
        text: 'Search',
        tooltipText: 'Search Items'
      });
    }
    if (this.showCollapseAndExpand) {
      this.toolbar.push(
        {
          text: 'Expand',
          prefixIcon: 'e-expand',
          tooltipText: 'Expand all'
        },
        {
          text: 'Collapse',
          prefixIcon: 'e-collapse',
          tooltipText: 'Collapse all'
        }
      );
    }
  }

  ngAfterViewInit(): void {
    document.onclick = (args: any): void => {
      if (args.target.tagName === 'body') {
        this.ejDialog.hide();
      }
    };
  }

  public hideDialog: EmitType<object> = () => {
    this.ejDialog.hide();
  };

  public deleteItem: EmitType<object> = () => {
    this.deleteRecord.emit(this.currentDeletingItem);
    this.ejDialog.hide();
  };

  public buttons: any = [
    {
      click: this.deleteItem.bind(this),
      buttonModel: { content: 'Yes', isPrimary: true }
    },
    {
      click: this.hideDialog.bind(this),
      buttonModel: { content: 'No' }
    }
  ];

  onEdit(args: Event): void {
    const rowObj: IRow<Column> = this.grid.getRowObjectFromUID(
      closest(args.target as Element, '.e-row').getAttribute('data-uid')
    );
    const data1: any = rowObj.data;
    this.editRecord.emit(data1);
  }

  onDelete(args: Event): void {
    const rowObj: IRow<Column> = this.grid.getRowObjectFromUID(
      closest(args.target as Element, '.e-row').getAttribute('data-uid')
    );
    const data1: any = rowObj.data;
    this.currentDeletingItem = data1;
    this.ejDialog.show();
  }

  onDetail(args: Event): void {
    const rowObj: IRow<Column> = this.grid.getRowObjectFromUID(
      closest(args.target as Element, '.e-row').getAttribute('data-uid')
    );
    this.detailRecord.emit(rowObj.data);
  }

  toolbarClicked(args: ClickEventArgs): void {
    switch (args.item.text) {
      case 'Add':
        this.addRecord.emit(args.item.id);
        break;
      case 'Filter':
        if (this.onfilter) {
          this.grid.allowFiltering = false;
          this.onfilter = !this.onfilter;
        } else {
          this.grid.allowFiltering = true;
          this.onfilter = !this.onfilter;
        }
        break;
      case 'Collapse':
        this.grid.groupModule.collapseAll();
        break;
      case 'Expand':
        this.grid.groupModule.expandAll();
        break;
      case 'ExportExcel':
        this.grid.excelExport();
        break;
      case 'Print':
        this.grid.print();
        break;
    }
  }

  onRowSelected(event){
    this.selectRecord.emit(event);
  }

  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === 'First' || args.column.field === 'Second') {
      if (args.data[args.column.field] === 'UnFit') {
          args.cell.classList.add('unfit');

      } else if(args.data[args.column.field] === 'Fit') {
          args.cell.classList.add('fit');
      }
    }
  } 

}
