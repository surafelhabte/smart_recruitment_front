module.exports = {
  name: 'data-grid',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/data-grid',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
