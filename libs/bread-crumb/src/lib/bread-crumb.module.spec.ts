import { async, TestBed } from '@angular/core/testing';
import { BreadCrumbModule } from './bread-crumb.module';

describe('BreadCrumbModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BreadCrumbModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(BreadCrumbModule).toBeDefined();
  });
});
