import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PageIdentityComponent } from './page-identity.component';
import { Router, RouterModule } from '@angular/router';
import { PageTitleComponent } from '../page-title/page-title.component';
import { BreadCrumbComponent } from '../bread-crumb/bread-crumb.component';

describe('PageIdentityComponent', () => {
  let component: PageIdentityComponent;
  let fixture: ComponentFixture<PageIdentityComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [
        PageIdentityComponent,
        PageTitleComponent,
        BreadCrumbComponent
      ],
      providers: [RouterModule]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(PageIdentityComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
