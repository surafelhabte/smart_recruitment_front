import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadCrumbComponent } from './bread-crumb/bread-crumb.component';
import { PageTitleComponent } from './page-title/page-title.component';
import { PageIdentityComponent } from './page-identity/page-identity.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [BreadCrumbComponent, PageTitleComponent, PageIdentityComponent],
  exports: [BreadCrumbComponent, PageTitleComponent, PageIdentityComponent]
})
export class BreadCrumbModule {}
