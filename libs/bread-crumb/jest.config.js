module.exports = {
  name: 'bread-crumb',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/bread-crumb',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
