import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'smart-recruitment-frontend-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css']
})
export class ToastComponent {
  @ViewChild('element', {static : true}) element;
  public position = { X: 'Center', Y: 'Top' };
  
  Show(data: any){
    this.element.show(data);
    history.state.data = undefined;
  }

  onClick(e) {
    e.clickToClose = true;
  }
}
