import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastComponent } from './toast/toast.component';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';

@NgModule({
  imports: [
    CommonModule,
    ToastModule, 
  ],
  declarations: [ToastComponent],
  exports: [ToastComponent]
})
export class Toast_Notification_Module {}
 