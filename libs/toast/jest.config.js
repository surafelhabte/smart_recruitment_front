module.exports = {
  name: 'toast',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/toast',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
