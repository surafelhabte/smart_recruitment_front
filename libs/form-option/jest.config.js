module.exports = {
  name: 'form-option',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/form-option',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
