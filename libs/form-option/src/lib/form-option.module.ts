import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormOptionComponent } from './form-option/form-option.component';
import { RouterModule } from '@angular/router';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';

@NgModule({
  imports: [CommonModule, RouterModule, ButtonModule],
  declarations: [FormOptionComponent],
  exports: [FormOptionComponent]
})
export class FormOptionModule {}
