import { async, TestBed } from '@angular/core/testing';
import { FormOptionModule } from './form-option.module';

describe('FormOptionModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormOptionModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(FormOptionModule).toBeDefined();
  });
});
