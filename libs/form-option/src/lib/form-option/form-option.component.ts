import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'smart-recruitment-frontend-form-option',
  templateUrl: './form-option.component.html',
  styleUrls: ['./form-option.component.css']
})
export class FormOptionComponent implements OnInit {
  @Input()
  public isSelfContained: Boolean;
  @Input()
  public submitDisabled: Boolean;
  @Input()
  public cancelDisabled: Boolean;
  @Input()
  public isUpdate: Boolean;
  @Input()
  public buttonText: string;

  constructor(private location: Location) {
    this.cancelDisabled = false;
    this.submitDisabled = false;
  }

  ngOnInit() {
    if (this.isUpdate) {
      this.buttonText = 'Update';
    } else this.buttonText ? null : (this.buttonText = 'Save');
  }

  cancel() {
    this.location.back();
  }
}
