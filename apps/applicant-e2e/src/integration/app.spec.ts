import { getGreeting } from '../support/app.po';

describe('applicant', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to applicant!');
  });
});
