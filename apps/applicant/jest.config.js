module.exports = {
    name: 'applicant',
    preset: '../../jest.config.js',
    coverageDirectory: '../../coverage/apps/applicant',
    snapshotSerializers: [
        // 'jest-preset-angular/AngularSnapshotSerializer.js',
        'jest-preset-angular/HTMLCommentSerializer.js'
    ]
};