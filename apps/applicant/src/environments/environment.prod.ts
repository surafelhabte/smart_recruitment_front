export const environment = {
  production: true,
  api_url: 'http://localhost/smartrecuritement/applicant/api',
  password_reset_url: 'http://localhost:4200/account/password/new'
};