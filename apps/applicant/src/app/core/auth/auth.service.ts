import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiService } from '../api/api.service';
import { HttpClient } from '@angular/common/http';
import { AccountModel } from '../../features/account-managment/account';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  constructor(private apiService: ApiService, private router: Router) {}

  login(email: string, password: string): Observable<any> {
    return this.apiService.post('/user/login', { email, password }).pipe(
      map(user => {
        return user;
      })
    );
  }

  signUp(userInfo): Observable<any> {
    return this.apiService.post('/user/signup', userInfo);
  }

  getUserById(userId: number): Observable<AccountModel> {
    return this.apiService.get('/user/getProfile/' + userId);
  }

  updateProfile(updatedUser: any): Observable<any> {
    return this.apiService.post(
      '/user/updateProfile/' + updatedUser.get('id'),
      updatedUser
    );
  }

  resetPassword(email) {
    return this.apiService.post('/user/findEmail', email);
  }

  changePassword(passwordInfo) {
    return this.apiService.post('/user/changePassword', passwordInfo);
  }

  set_password(data) {
    return this.apiService.post('/user/setNewPassword', data);
  }

  logout(): void {
    localStorage.removeItem('id');
    localStorage.removeItem('key');
    localStorage.removeItem('email');
    localStorage.removeItem('firstName');
    localStorage.removeItem('surname');
    this.router.navigate(['/']);
  }
}
