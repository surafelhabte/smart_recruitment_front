/*
 * @CreateTime: Nov 2, 2019 4:36 PM
 * @Author: Naol
 * @Contact: nnale8899@gmail.com
 * @Last Modified By: Naol
 * @Last Modified Time: Nov 2, 2019 4:36 PM
 * @Description: Modify Here, Please
 */
import { TestBed } from '@angular/core/testing';
import { TokenInterceptorService } from './token-interceptor.service';
import { LocalStorageService } from './local-storage.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { CoreModule } from '../core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from '../api/api.service';
import { AuthService } from './auth.service';

describe('SystemCacheInterceptorService', () => {
  let tokenInterceptService: TokenInterceptorService;
  let storage: LocalStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule],
      providers: [ApiService]
    });

    tokenInterceptService = TestBed.get(TokenInterceptorService);
  });

  it('should be created', () => {
    const service: TokenInterceptorService = new TokenInterceptorService(
      storage
    );
    expect(service).toBeTruthy();
  });
});
