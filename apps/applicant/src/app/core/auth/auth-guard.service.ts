import { Injectable } from '@angular/core';

import { CanActivate, Router } from '@angular/router';

import { Observable } from 'rxjs';

import { map, take } from 'rxjs/operators';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private storage: LocalStorageService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.storage.getItem('email').pipe(
      map(t => {
        if (!t) {
          this.router.navigate(['/']);
          return false;
        }
        return true;
      }),
      take(1)
    );
  }
}
