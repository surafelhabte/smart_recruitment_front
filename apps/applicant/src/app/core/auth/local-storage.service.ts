import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {
  getItem(t: any) {
    const data = localStorage.getItem(t);
    if (data) {
      return of(data);
    }
    return of(null);
  }

  setItem(data: any): Observable<any> {
    localStorage.setItem('userData', data);
    return of(data);
  }

  removeItem(): Observable<boolean> {
    localStorage.removeItem('userData');
    return of(true);
  }
}
