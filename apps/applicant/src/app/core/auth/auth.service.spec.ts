/*
 * @CreateTime: Nov 2, 2019 4:36 PM
 * @Author: Naol
 * @Contact: nnale8899@gmail.com
 * @Last Modified By: Naol
 * @Last Modified Time: Nov 5, 2019 12:27 PMM
 * @Description: Modify Here, Please
 */
import { AuthService } from './auth.service';

import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';

import { TestBed } from '@angular/core/testing';

import { CoreModule } from '../core.module';

import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from '../api/api.service';
import { AccountModel } from '../../features/account-managment/account';

describe('Auth service', () => {
  const blob: any = null;
  let authService: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule],
      providers: [ApiService]
    });

    authService = TestBed.get(AuthService);
    httpMock = TestBed.get(HttpTestingController);
  });
  it('Should get account successfull', () => {
    const profile: AccountModel = {
      id: 1,
      FirstName: 'Appdiv',
      Surname: 'Systems',
      Phone: '12345',
      Email: 'email@email.com',
      Password: 'abcd',
      Gender: 'Male',
      Dob: new Date(),
      Disability: 'False',
      photo: new File([blob], 'filename')
    };

    authService.getUserById(1).subscribe((data: any) => {
      expect(data).toBe(profile);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(profile);

    httpMock.verify();
  });

  it('Should signup user', () => {
    const newAccount: AccountModel = {
      id: 2,
      FirstName: 'Appdiv',
      Surname: 'Systems',
      Phone: '12345',
      Email: 'email@email.com',
      Password: 'abcd',
      Gender: 'Male',
      Dob: new Date(),
      Disability: 'False',
      photo: new File([blob], 'filename')
    };
    authService.signUp(newAccount).subscribe((data: any) => {
      expect(data.Id).toBe(2);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(newAccount);
    httpMock.verify();
  });
  it('Should update account', () => {
    const updatedAccount = new FormData();
    updatedAccount.append('id', '1'),
      updatedAccount.append('FirstName', 'Appdiv'),
      updatedAccount.append('Surname', 'Appdiv'),
      updatedAccount.append('Phone', 'Appdiv'),
      updatedAccount.append('Email', 'Appdiv'),
      updatedAccount.append('Password', 'Appdiv'),
      updatedAccount.append('Gender', 'Appdiv'),
      updatedAccount.append('Dob', '1111-11-11'),
      updatedAccount.append('Disability', 'False'),
      updatedAccount.append('photo', blob);
    authService.updateProfile(updatedAccount).subscribe((data: any) => {
      expect(data.FirstName).toBe('Appdiv');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(updatedAccount);

    httpMock.verify();
  });

  it('Should resetPassword', () => {
    const email = {
      Email: 'email@email.com'
    };
    authService.resetPassword(email).subscribe((data: any) => {
      expect(data.Email).toBe('email@email.com');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(email);
    httpMock.verify();
  });

  it('Should change password', () => {
    const passInfo = {
      id: 1,
      OldPassword: 'sadasdsad234',
      Password: 'sadhjasbd32423'
    };
    authService.changePassword(passInfo).subscribe((data: any) => {
      console.log(data);
      expect(data.OldPassword).toBe('sadasdsad234');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(passInfo);
    httpMock.verify();
  });
});
