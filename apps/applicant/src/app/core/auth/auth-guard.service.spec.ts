/*
 * @CreateTime: Nov 2, 2019 4:36 PM
 * @Author: Naol
 * @Contact: nnale8899@gmail.com
 * @Last Modified By: Naol
 * @Last Modified Time: Nov 2, 2019 4:36 PM
 * @Description: Modify Here, Please
 */
import { TestBed } from '@angular/core/testing';
import { AuthGuardService } from './auth-guard.service';
import { AuthService } from './auth.service';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { CoreModule } from '../core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from '../api/api.service';

describe('SampleService', () => {
  let authService: AuthService;
  let httpMock: HttpTestingController;
  //   beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule]
      //   providers: [AuthService]
    });

    authService = TestBed.get(AuthService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });
});
