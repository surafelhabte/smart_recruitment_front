import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiModule } from './api/api.module';
import { AuthService } from './auth/auth.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuardService } from './auth/auth-guard.service';
import { LocalStorageService } from './auth/local-storage.service';
import { TokenInterceptorService } from './auth/token-interceptor.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, ApiModule, HttpClientModule],
  providers: [
    AuthGuardService,
    AuthService,
    LocalStorageService,
    TokenInterceptorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ]
})
export class CoreModule {}
