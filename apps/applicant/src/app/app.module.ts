import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from './core/core.module';
import { ResetPasswordComponent } from './features/account-managment/reset-password/reset-password.component';
import { AccountManagmentModule } from './features/account-managment/account-managment.module';
import { IndexModule } from './features/index/index.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { TabModule } from '@syncfusion/ej2-angular-navigations';
import { ApplyComponent } from './features/index/vacancy/apply/apply.component';
import { FormOptionModule } from '@smart-recruitment-frontend/form-option';
const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./features/features.module').then(m => m.FeaturesModule)
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    NgbModule,
    MDBBootstrapModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    CoreModule,
    AccountManagmentModule,
    IndexModule,
    CKEditorModule,
    TextBoxModule,
    TabModule,
    FormOptionModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ResetPasswordComponent, ApplyComponent]
})
export class AppModule {}
