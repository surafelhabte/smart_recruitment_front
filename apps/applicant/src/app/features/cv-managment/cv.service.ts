import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ApiService } from '../../core/api/api.service';
import { CvListModel } from './cv.interface';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CVService {
  id = localStorage.getItem('id');
  adminUrl = 'http://smartjob.net/SmartRecruitmentAPI/api';
  constructor(private apiService: ApiService, private httpClient: HttpClient) {}

  getAllMyCvs(): Observable<CvListModel> {
    return this.apiService.post('/Cv/GetCvs', {
      applicant_id: this.id
    });
  }

  deleteCV(cvInfo) {
    // const cvInfo = { applicant_id: this.id, id: cvId, DocumentName: docName, Title };
    return this.apiService.post('/Cv/Delete', cvInfo);
  }

  createCv(cvInfo): Observable<any> {
    return this.apiService.post('/Cv/Create', cvInfo);
  }

  getCv(cvId: number) {
    return this.apiService.get('/cv/GetCv/' + cvId);
  }

  loadLookupData(data) {
    return this.apiService.post('/Lookup/load_lookups', data);
  }

  updateCV(updatedCv: any): Observable<any> {
    return this.apiService.post(
      '/cv/Update',
      updatedCv
    );
  }
}
