import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCvComponent } from './list-cv/list-cv.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthGuardService } from '../../core/auth/auth-guard.service';
import { CoreModule } from '../../core/core.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { CreateCvComponent } from './CV creation/create-cv/create-cv.component';
import { QualificationsComponent } from './CV creation/qualifications/qualifications.component';
import { TabModule } from '@syncfusion/ej2-angular-navigations';
import { ReferencesComponent } from './CV creation/references/references.component';
import { TrainingsComponent } from './CV creation/trainings/trainings.component';
import { ExperienceComponent } from './CV creation/experience/experience.component';
import { FormOptionModule } from '@smart-recruitment-frontend/form-option';

const routes: Routes = [
  {
    path: 'list',
    component: ListCvComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: ':cvId/update',
    component: CreateCvComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'create',
    component: CreateCvComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  declarations: [
    ListCvComponent,
    CreateCvComponent,
    QualificationsComponent,
    ReferencesComponent,
    TrainingsComponent,
    ExperienceComponent
  ],
  imports: [
    CKEditorModule,
    NgbModule,
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    CoreModule,
    TextBoxModule,
    TabModule,
    FormOptionModule
  ],
  exports: [CreateCvComponent]
})
export class CvManagmentModule {}
