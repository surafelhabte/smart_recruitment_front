import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CVService } from '../cv.service';
import { Router } from '@angular/router';

@Component({
  selector: 'smart-recruitment-frontend-list-cv',
  templateUrl: './list-cv.component.html',
  styleUrls: ['./list-cv.component.css']
})
export class ListCvComponent implements OnInit {
  cvs: any = [];
  public cvId = 29;
  applicantId = localStorage.getItem('id');
  public documentName: any;
  public cvInfo: object;
  constructor(private cvService: CVService, private router: Router) {}

  ngOnInit() {
    this.cvService.getAllMyCvs().subscribe(data => {
      this.cvs = data;
    });
  }

  deleteCv(cvId) {
    this.cvInfo = this.cvs.filter(elem => elem.id === cvId);
    const appId = { applicant_id: this.applicantId };
    this.cvInfo = Object.assign(appId, this.cvInfo[0]);
    const confirmation = confirm('Are you sure you want to delete this Cv');
    if (confirmation === true) {
      this.cvService.deleteCV(this.cvInfo).subscribe(data => {
        alert(data.message);
        this.ngOnInit();
      });
    }
  }

  getCV(cvId) {
    // this.router.navigate(['/cv/' + cvId + '/update']);
    this.cvId = cvId;
  }

  selected(i) {
    const className = document.getElementById('aa' + i).className;
    if (className == 'collapse') {
      document.getElementById('headingOne' + i).style.background = '#ffc10788';
     /*  document.getElementById('aa' + i).style.background = '#ffc10733'; */
    } else {
      document.getElementById('headingOne' + i).style.background = '#28a74536';
    }
  }
}
