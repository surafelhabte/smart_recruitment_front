/*
 * @CreateTime: Nov 7, 2019 10:02 AM
 * @Author: Naol
 * @Contact: nnale8899@gmail.com
 * @Last Modified By: Naol
 * @Last Modified Time: Nov 7, 2019 10:06 AM
 * @Description: Modify Here, Please
 */

import { ListCvComponent } from './list-cv.component';
import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { CoreModule } from '../../../core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { CVService } from '../cv.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TabModule } from '@syncfusion/ej2-angular-navigations';
import { CreateCvComponent } from '../CV creation/create-cv/create-cv.component';
import { QualificationsComponent } from '../CV creation/qualifications/qualifications.component';
import { ExperienceComponent } from '../CV creation/experience/experience.component';
import { TrainingsComponent } from '../CV creation/trainings/trainings.component';
import { ReferencesComponent } from '../CV creation/references/references.component';

describe('ListCvComponent', () => {
  let component: ListCvComponent;
  let fixture: ComponentFixture<ListCvComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        CoreModule,
        RouterTestingModule,
        ReactiveFormsModule,
        CKEditorModule,
        TabModule
      ],
      declarations: [
        ListCvComponent,
        CreateCvComponent,
        QualificationsComponent,
        ExperienceComponent,
        TrainingsComponent,
        ReferencesComponent
      ],
      providers: [CVService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ListCvComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
