import { Injectable } from '@angular/core';
import { ApiService } from 'apps/applicant/src/app/core/api/api.service';

@Injectable({
  providedIn: 'root'
})
export class ExperiencesService {
  id = localStorage.getItem('id');
  constructor(private apiService: ApiService) {}

  deleteExperiences(qId) {
      const experiences = {
          table: "Experiences",
          id: qId
      }
    return this.apiService.post('/Cv/DeleteItem', experiences);
  }
}
