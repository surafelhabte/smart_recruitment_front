import { ExperiencesService } from "./experiences.service";
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from 'apps/applicant/src/app/core/api/api.service';


describe('experience service', () => {
  let eService: ExperiencesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule],
      providers: [ApiService]
    });

    eService = TestBed.get(ExperiencesService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should delete cv', () => {
      const exp = {table: "Experiences", id:1}
    eService.deleteExperiences(exp).subscribe((data: any) => {
      expect(data).toBe(exp);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(12);

    httpMock.verify();
  });
});
