import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ExperienceComponent } from './experience.component';

describe('ExperienceComponent', () => {
  let component: ExperienceComponent;
  let fixture: ComponentFixture<ExperienceComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [ReactiveFormsModule, CoreModule, RouterTestingModule],
      declarations: [ExperienceComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ExperienceComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on prepareExperience', () => {
    it('method should be called', () => {
      const submitFun = spyOn(component, 'prepareExperiences');
      component.prepareExperiences();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Reference form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be valid when not empty', () => {
      expect(component.experienceForm.valid).toBeTruthy();
    });
    it('Qualifications array validity', () => {
      const r = component.experiences;
      expect(r.valid).toBeTruthy();
      r.controls.forEach(element => {
        ({
          EmployerName: 'employerName',
          EmployerAddress: 'employerAddress',
          From: new Date(),
          To: new Date(),
          JobTitle: 'jobTitle',
          Achievments: 'achievements',
          Salary: 12213,
          ReasonForLeaving: 'reasonLeaving',
          Notice: 'notice',
          CarrerLevel: 'carrerLevel'
        });
      });
      expect(r.length).toEqual(r.length);
    });
  });
});
