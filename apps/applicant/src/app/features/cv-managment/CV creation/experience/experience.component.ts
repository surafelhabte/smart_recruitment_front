import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ExperiencesService } from './experiences.service';

@Component({
  selector: 'smart-recruitment-frontend-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent implements OnInit {
  @Input()
  public EData = [];
  experienceForm: FormGroup;
  experienceForms = [];
  submitted = false;

  @Input()
  public expForm = this.experienceForm;

  constructor(private formBuilder: FormBuilder, private eService: ExperiencesService) {
    this.createForm();
  }

  ngOnInit() {
    if (this.EData.length > 0) {
      this.EData.forEach(exp => {
        this.experiences.controls.push(this.initializeExperiences(exp));
      });
    }
  }

  initializeExperiences(exp) {
    return this.formBuilder.group({
      id: [exp.id],
      employerName: [exp.EmployerName, Validators.required],
      employerAddress: [exp.EmployerAddress, Validators.required],
      startDate: [exp.EmployedFrom, Validators.required],
      endDate: [exp.EmployedTo, Validators.required],
      jobTitle: [exp.JobTitle, Validators.required],
      achievements: [exp.Achievements],
      salary: [exp.Salary, Validators.required],
      reasonLeaving: [exp.ReasonForLeaving, Validators.required],
      notice: [exp.Notice],
      carrerLevel: [exp.CareerLevel, Validators.required]
    });
  }

  createForm(): void {
    this.experienceForm = this.formBuilder.group({
      experienceArray: this.formBuilder.array([])
    });
  }

  createExperience(): FormGroup {
    return this.formBuilder.group({
      employerName: ['', Validators.required],
      employerAddress: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      jobTitle: ['', Validators.required],
      achievements: [''],
      salary: ['', Validators.required],
      reasonLeaving: ['', Validators.required],
      notice: [''],
      carrerLevel: ['', Validators.required]
    });
  }

  addExperience(): void {
    this.experiences.controls.push(this.createExperience());
  }

  deleteExperience(index) {
    const deletedControlId = this.experiences.controls[index].get('id');
    if (deletedControlId) {
      const conf = confirm('Are you sure you want to delete');
      if (conf) {
        this.eService.deleteExperiences(deletedControlId.value).subscribe(data => {
         alert(data.message);
       }) 
        this.experiences.removeAt(index);
      }
    } else {
      this.experiences.removeAt(index);
    }
  }

  get experiences(): FormArray {
    return this.experienceForm.get('experienceArray') as FormArray;
  }

  prepareExperiences() {
    this.experienceForms = [];
    if (this.experienceForm.valid) {
      this.experiences.controls.forEach(element => {
        if (this.EData.length > 0) {
          this.experienceForms.push({
            id: element.value.id,
            EmployerName: element.value.employerName,
            EmployerAddress: element.value.employerAddress,
            EmployedFrom: element.value.startDate,
            EmployedTo: element.value.endDate,
            JobTitle: element.value.jobTitle,
            Achievements: element.value.achievements,
            Salary: element.value.salary,
            ReasonForLeaving: element.value.reasonLeaving,
            Notice: element.value.notice,
            CareerLevel: element.value.carrerLevel
          });
        } else {
          this.experienceForms.push({
            EmployerName: element.value.employerName,
            EmployerAddress: element.value.employerAddress,
            EmployedFrom: element.value.startDate,
            EmployedTo: element.value.endDate,
            JobTitle: element.value.jobTitle,
            Achievements: element.value.achievements,
            Salary: element.value.salary,
            ReasonForLeaving: element.value.reasonLeaving,
            Notice: element.value.notice,
            CareerLevel: element.value.carrerLevel
          });
        }
      });
       
    } else alert("Invalid experience form detected!");
    return this.experienceForms;
  }
}
