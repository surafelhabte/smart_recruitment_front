import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  ChangeDetectionStrategy,
  Input
} from '@angular/core';
import {
  FormGroup,
  Validators,
  FormBuilder,
  FormControl,
  FormArray
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CVService } from '../../cv.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { QualificationsComponent } from '../qualifications/qualifications.component';
import { TrainingsComponent } from '../trainings/trainings.component';
import { ReferencesComponent } from '../references/references.component';
import { ExperienceComponent } from '../experience/experience.component';
import { Location } from '@angular/common';

@Component({
  selector: 'smart-recruitment-frontend-create-cv',
  templateUrl: './create-cv.component.html',
  styleUrls: ['./create-cv.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateCvComponent implements OnInit {
  @ViewChild('qualificationComponent', { static: false })
  QComponent: QualificationsComponent;
  @ViewChild('trainingComponent', { static: false })
  TComponent: TrainingsComponent;
  @ViewChild('referenceComponent', { static: false })
  RComponent: ReferencesComponent;
  @ViewChild('experienceComponent', { static: false })
  EComponent: ExperienceComponent;
  @Input()
  public cvId: number;

  public experianceArray = [];
  public qualificationArray = [];
  public referenceArray = [];
  public trainingArray = [];
  public loadLookup = ['Qualification', 'FieldOfStudy'];
  public qualificationList = [];
  public fieldOfStudyList = [];
  documentName: string;
  public expForm: FormGroup;
  public documentFile: any;

  applicantId = localStorage.getItem('id');
  cvForm: FormGroup;
  submitted = false;
  isUpdate = false;

  public Editor = ClassicEditor;
  constructor(
    private formBuilder: FormBuilder,
    private cvService: CVService,
    private location: Location,
    private router: Router
  ) {
    this.createForm();
  }

  public input = new FormData();

  ngOnInit() {
    this.QComponent;
    this.getLookupData(this.loadLookup);
    // this.cvId = +this.activatedRoute.snapshot.paramMap.get('cvId');
    if (this.cvId) {
      this.isUpdate = true;
      this.cvService.getCv(this.cvId).subscribe(CV => {
        this.cvForm.patchValue({
          title: CV.cv.Title,
          coverLetter: CV.cv.CoverLetter
        });
        this.initializeForm(CV.cv);
        this.qualificationArray = CV.Qualifications;
        this.trainingArray = CV.Trainings;
        this.referenceArray = CV.References;
        this.experianceArray = CV.Experiences;
      });
    }
  }

  createForm(): void {
    this.cvForm = this.formBuilder.group({
      title: ['', Validators.required],
      document: ['', Validators.required],
      coverLetter: ['', Validators.required]
    });
  }
  initializeForm(CV) {
    this.cvForm = this.formBuilder.group({
      title: [CV.Title, Validators.required],
      coverLetter: [CV.CoverLetter, Validators.required],
      document: ['']
    });
  }
  get title(): FormControl {
    return this.cvForm.get('title') as FormControl;
  }
  get document(): FormControl {
    return this.cvForm.get('document') as FormControl;
  }

  get coverLetter(): FormControl {
    return this.cvForm.get('coverLetter') as FormControl;
  }
  prepareFormData(): any | null {
    let qualificationForms = [];
    let trainingForms = [];
    let refernceForms = [];
    let experienceForms = [];
    qualificationForms = this.QComponent.prepareQualification();
    trainingForms = this.TComponent.prepareTraining();
    refernceForms = this.RComponent.prepareReferences();
    experienceForms = this.EComponent.prepareExperiences();
    if (
      this.cvForm.valid &&
      this.EComponent.experiences.valid &&
      this.QComponent.qualifications.valid &&
      this.RComponent.references.valid &&
      this.TComponent.trainings.valid
    ) {
      let cv: Object;
      this.cvId
        ? (cv = {
          id: this.cvId,
          Applicant_Id: this.applicantId,
          Title: this.title.value,
          CoverLetter: this.coverLetter.value
        })
        : (cv = {
          Applicant_Id: this.applicantId,
          Title: this.title.value,
          CoverLetter: this.coverLetter.value
        });
      this.input.append('cv', JSON.stringify(cv));
      this.input.append('Document', this.documentFile);
      this.input.append('Qualifications', JSON.stringify(qualificationForms));
      this.input.append('Trainings', JSON.stringify(trainingForms));
      this.input.append('References', JSON.stringify(refernceForms));
      this.input.append('Experiences', JSON.stringify(experienceForms));

      return this.input;
    } else {
      return null;
    }
  }

  submit() {
    this.submitted = true;
    const payload = this.prepareFormData();
    if (payload) {
      if (this.cvId) {
        this.cvService.updateCV(payload).subscribe(data => {
          alert(data.message);
        });
      } else {
        this.cvService.createCv(this.input).subscribe(data => {
          alert(data.message);
          this.location.back();
        });
      }
    }
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      this.documentFile = event.target.files[0];
    }
  }

  getLookupData(data) {
    this.cvService.loadLookupData(data).subscribe((data: any) => {
      this.qualificationList = data.Qualification;
      this.fieldOfStudyList = data.FieldOfStudy;
    });
  }

  test() {
    alert('message');
  }
}
