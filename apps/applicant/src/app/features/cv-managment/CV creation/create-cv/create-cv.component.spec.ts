import {
  TestModuleMetadata,
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CreateCvComponent } from './create-cv.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TabModule } from '@syncfusion/ej2-angular-navigations';
import { ExperienceComponent } from '../experience/experience.component';
import { QualificationsComponent } from '../qualifications/qualifications.component';
import { TrainingsComponent } from '../trainings/trainings.component';
import { ReferencesComponent } from '../references/references.component';

import { CoreModule } from 'apps/applicant/src/app/core/core.module';

const crypto = require('crypto');
/* (window as any).crypto = window;
(window as any).getRandomValues = window; */
/* const buffer = crypto.getRandomValues(new Uint8Array(31)) as Uint8Array;
const value = buffer[0];

Object.defineProperty(global, 'crypto', {
  value
}); */
Object.defineProperty(global, 'crypto', {
  value: {
    getRandomValues: arr => crypto.randomBytes(arr.length)
  }
});

describe('CreateCvComponent', () => {
  let component: CreateCvComponent;
  let fixture: ComponentFixture<CreateCvComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        CKEditorModule,
        CoreModule,
        TabModule
      ],
      declarations: [
        CreateCvComponent,
        ExperienceComponent,
        QualificationsComponent,
        TrainingsComponent,
        ReferencesComponent
      ]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(CreateCvComponent);
    component = fixture.componentInstance;
    component.QComponent = TestBed.createComponent(
      QualificationsComponent
    ).componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onCreate', () => {
    it('Submit should be called', () => {
      const qualBtn = jest.spyOn(component.QComponent, 'prepareQualification');
      const obj1 = {
        qualBtn: () => null
      };
      const submitFun = jest.spyOn(component, 'submit');
      component.submit();
      component.QComponent.prepareQualification();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('cv form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.cvForm.valid).toBeFalsy();
    });
    it('CV fields validity sample for title', () => {
      const cvField = component.title;
      expect(cvField.valid).toBeFalsy();
      cvField.setValue('email@email.com');
      expect(cvField.valid).toBeTruthy();
    });
  });
});
