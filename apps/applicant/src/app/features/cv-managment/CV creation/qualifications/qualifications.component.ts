import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { CVService } from '../../cv.service';
import { QualificationsService } from './qualifications.service';

@Component({
  selector: 'smart-recruitment-frontend-qualifications',
  templateUrl: './qualifications.component.html',
  styleUrls: ['./qualifications.component.css']
})
export class QualificationsComponent implements OnInit {
  @Input()
  public QData = [];
  @Input()
  public qualificationList = [];
  @Input()
  public fieldOfStudyList = [];
  qualificationForm: FormGroup;
  qualificationForms = [];
  submitted = false;
  isUpdate = false;

  constructor(private formBuilder: FormBuilder, private qService: QualificationsService) {
    this.createForm();
  }

  ngOnInit() {
    if (this.QData.length > 0) {
      this.QData.forEach(q => {
        this.qualifications.controls.push(this.initializeQualification(q));
      });
    }
  }

  initializeQualification(Q) {
    return this.formBuilder.group({
      id: [Q.id],
      qualification: [Q.Qualification, Validators.required],
      institution: [Q.Institution, Validators.required],
      fieldOfStudy: [Q.Field_Of_Study, Validators.required],
      startDate: [Q.From],
      endDate: [Q.To]
    });
  }

  createForm(): void {
    this.qualificationForm = this.formBuilder.group({
      qualificationArray: this.formBuilder.array([])
    });
  }

  createQualification(): FormGroup {
    return this.formBuilder.group({
      qualification: ['', Validators.required],
      institution: ['', Validators.required],
      fieldOfStudy: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required]
    });
  }

  addQualification(): void {
    this.qualifications.controls.push(this.createQualification());
  }

  deleteQualification(index) {
    const deletedControlId = this.qualifications.controls[index].get('id');
    if (deletedControlId) {
      const conf = confirm('Are you sure you want to delete');
      if (conf) {
       this.qService.deleteQualification(deletedControlId.value).subscribe(data => {
         alert(data.message);
       }) 
        this.qualifications.removeAt(index);
      }
    } else {
      this.qualifications.removeAt(index);
    }
  }

  get qualifications(): FormArray {
    return this.qualificationForm.get('qualificationArray') as FormArray;
  }

  prepareQualification() {
    this.qualificationForms = [];
    if (this.qualificationForm.valid) {
      this.qualifications.controls.forEach(element => {
        if (this.QData.length > 0) {
          this.qualificationForms.push({
            id: element.value.id,
            Qualification: element.value.qualification,
            Institution: element.value.institution,
            Field_Of_Study: element.value.fieldOfStudy,
            From: element.value.startDate,
            To: element.value.endDate
          });
        } else {
          this.qualificationForms.push({
            Qualification: element.value.qualification,
            Institution: element.value.institution,
            Field_Of_Study: element.value.fieldOfStudy,
            From: element.value.startDate,
            To: element.value.endDate
          });
        }
      });
    } else alert("Invalid qualification form detected!");;

    return this.qualificationForms;
  }
}
