import { Injectable } from '@angular/core';
import { ApiService } from 'apps/applicant/src/app/core/api/api.service';

@Injectable({
  providedIn: 'root'
})
export class QualificationsService {
  id = localStorage.getItem('id');
  constructor(private apiService: ApiService) {}

  deleteQualification(qId) {
      const qualification = {
          table: "Qualifications",
          id: qId
      }
    return this.apiService.post('/Cv/DeleteItem', qualification);
  }
}
