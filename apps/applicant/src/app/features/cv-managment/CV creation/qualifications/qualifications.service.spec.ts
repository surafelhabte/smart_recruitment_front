import { QualificationsService } from "./qualifications.service";
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from 'apps/applicant/src/app/core/api/api.service';

describe('qualification service', () => {
  let qService: QualificationsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule],
      providers: [ApiService]
    });

    qService = TestBed.get(QualificationsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should delete cv', () => {
      const qual = {table: "Qualifications", id:1}
    qService.deleteQualification(qual).subscribe((data: any) => {
      expect(data).toBe(qual);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(12);

    httpMock.verify();
  });
});
