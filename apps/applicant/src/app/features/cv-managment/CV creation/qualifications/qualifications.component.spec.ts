import { QualificationsComponent } from './qualifications.component';
import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('QualificationsComponent', () => {
  let component: QualificationsComponent;
  let fixture: ComponentFixture<QualificationsComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [ReactiveFormsModule, CoreModule, RouterTestingModule],
      declarations: [QualificationsComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(QualificationsComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on prepareQualification', () => {
    it('method should be called', () => {
      const submitFun = jest.spyOn(component, 'prepareQualification');
      component.prepareQualification();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Qualification form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be valid when not empty', () => {
      expect(component.qualificationForm.valid).toBeTruthy();
    });
    it('Qualifications array validity', () => {
      const q = component.qualifications;
      expect(q.valid).toBeTruthy();
      q.controls.forEach(element => {
        [
          {
            Qualification: 'degree',
            Institution: 'aau',
            From: new Date(),
            To: new Date()
          }
        ];
      });
      expect(q.length).toEqual(q.length);
    });
  });
});
