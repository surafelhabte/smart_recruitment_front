import { Injectable } from '@angular/core';
import { ApiService } from 'apps/applicant/src/app/core/api/api.service';

@Injectable({
  providedIn: 'root'
})
export class TrainingsService {
  constructor(private apiService: ApiService) {}

  deleteTraining(tId) {
      const train = {
          table: "Trainings",
          id: tId
      }
    return this.apiService.post('/Cv/DeleteItem', train);
  }
}
