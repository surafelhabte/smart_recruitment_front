import { TrainingsService } from "./trainings.service";
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from 'apps/applicant/src/app/core/api/api.service';



describe('trainings service', () => {
  let tService: TrainingsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule],
      providers: [ApiService]
    });

    tService = TestBed.get(TrainingsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should delete cv', () => {
      const train = {table: "Trainings", id:1}
    tService.deleteTraining(train).subscribe((data: any) => {
      expect(data).toBe(train);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(12);

    httpMock.verify();
  });
});
