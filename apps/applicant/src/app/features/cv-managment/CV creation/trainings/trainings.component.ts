import { Component, OnInit, Input } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TrainingsService } from './trainings.service';

@Component({
  selector: 'smart-recruitment-frontend-trainings',
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.css']
})
export class TrainingsComponent implements OnInit {
  @Input()
  public TData = [];
  trainingForm: FormGroup;
  trainingForms = [];
  submitted = false;
  constructor(private formBuilder: FormBuilder, private tService: TrainingsService) {
    this.createForm();
  }

  ngOnInit() {
    if (this.TData.length > 0) {
      this.TData.forEach(train => {
        this.trainings.controls.push(this.initializeTraining(train));
      });
    }
  }

  initializeTraining(T) {
    return this.formBuilder.group({
      id: [T.id],
      course: [T.Course, Validators.required],
      startDate: [T.From, Validators.required],
      endDate: [T.To, Validators.required],
      details: [T.Details]
    });
  }

  createForm(): void {
    this.trainingForm = this.formBuilder.group({
      trainingArray: this.formBuilder.array([])
    });
  }

  createTraining(): FormGroup {
    return this.formBuilder.group({
      course: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      details: ['']
    });
  }

  addTraining(): void {
    this.trainings.controls.push(this.createTraining());
  }

  deleteTraining(index) {
    const deletedControlId = this.trainings.controls[index].get('id');
    if (deletedControlId) {
      const conf = confirm('Are you sure you want to delete');
      if (conf) {
        this.tService.deleteTraining(deletedControlId.value).subscribe(data => {
         alert(data.message);
        })
        this.trainings.removeAt(index);
      }
    } else {
      this.trainings.removeAt(index);
    }
  }

  get trainings(): FormArray {
    return this.trainingForm.get('trainingArray') as FormArray;
  }

  prepareTraining() {
    this.trainingForms = [];
    if (this.trainingForm.valid) {
      this.trainings.controls.forEach(element => {
        if (this.TData.length > 0) {
          this.trainingForms.push({
            id: element.value.id,
            Course: element.value.course,
            From: element.value.startDate,
            To: element.value.endDate,
            Details: element.value.details
          });
        } else {
          this.trainingForms.push({
            Course: element.value.course,
            From: element.value.startDate,
            To: element.value.endDate,
            Details: element.value.details
          });
        }
      });
    } else alert("Invalid trainings form detected!");;

    return this.trainingForms;
  }
}
