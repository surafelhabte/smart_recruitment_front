import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule, FormArray } from '@angular/forms';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TrainingsComponent } from './trainings.component';

describe('TrainingsComponent', () => {
  let component: TrainingsComponent;
  let fixture: ComponentFixture<TrainingsComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [ReactiveFormsModule, CoreModule, RouterTestingModule],
      declarations: [TrainingsComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(TrainingsComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  describe('When training form is on create state', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    describe('on prepareTraining', () => {
      it('method should be called', () => {
        const submitFun = jest.spyOn(component, 'prepareTraining');
        component.prepareTraining();
        expect(submitFun).toHaveBeenCalled();
      });
    });

    describe('Training form', () => {
      it('Should be created', () => {
        const obj1 = {
          createForm: () => null
        };
        spyOn(obj1, 'createForm');
        obj1.createForm();
        expect(obj1.createForm).toBeTruthy();
      });

      it('Should be valid when not empty', () => {
        expect(component.trainingForm.valid).toBeTruthy();
      });
      it('Training array validity', () => {
        const t = component.trainings;
        expect(t.valid).toBeTruthy();
        t.controls.forEach(element => {
          ({
            Course: 'course',
            From: new Date(),
            To: new Date(),
            Details: 'details'
          });
        });
        expect(t.length).toEqual(t.length);
      });
    });
  });
});
