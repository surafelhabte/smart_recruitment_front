import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ReferencesService } from './references.service';

@Component({
  selector: 'smart-recruitment-frontend-references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.css']
})
export class ReferencesComponent implements OnInit {
  @Input()
  public RData = [];

  referenceForm: FormGroup;
  referenceForms = [];
  submitted = false;
  constructor(private formBuilder: FormBuilder, private rService: ReferencesService) {
    this.createForm();
  }

  ngOnInit() {
    if (this.RData.length > 0) {
      this.RData.forEach(refer => {
        this.references.controls.push(this.initializeTraining(refer));
      });
    }
  }

  createForm(): void {
    this.referenceForm = this.formBuilder.group({
      referenceArray: this.formBuilder.array([])
    });
  }

  initializeTraining(R) {
    return this.formBuilder.group({
      id: [R.id],
      fullName: [R.FullName, Validators.required],
      jobTitle: [R.JobTitle, Validators.required],
      relationship: [R.Relationship, Validators.required],
      address: [R.Address, Validators.required],
      email: [R.Email, [Validators.required, Validators.email]],
      phone: [R.Phone, Validators.required],
      CanWeContact: [R.CanWeContact]
    });
  }

  createReference(): FormGroup {
    return this.formBuilder.group({
      fullName: ['', Validators.required],
      jobTitle: ['', Validators.required],
      relationship: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      CanWeContact: ['']
    });
  }

  addReference(): void {
    this.references.controls.push(this.createReference());
  }

  deleteReference(index) {
    const deletedControlId = this.references.controls[index].get('id');
    if (deletedControlId) {
      const conf = confirm('Are you sure you want to delete');
      if (conf) {
         this.rService.deleteReferences(deletedControlId.value).subscribe(data => {
         alert(data.message);
       })
        this.references.removeAt(index);
      }
    } else {
      this.references.removeAt(index);
    }
  }

  get references(): FormArray {
    return this.referenceForm.get('referenceArray') as FormArray;
  }

  prepareReferences() {
    this.referenceForms = [];
    if (this.referenceForm.valid) {
      this.references.controls.forEach(element => {
        if (this.RData.length > 0) {
          this.referenceForms.push({
            id: element.value.id,
            FullName: element.value.fullName,
            JobTitle: element.value.jobTitle,
            Relationship: element.value.relationship,
            Address: element.value.address,
            Email: element.value.email,
            Phone: element.value.phone,
            CanWeContact: element.value.CanWeContact
          });
        } else {
          this.referenceForms.push({
            FullName: element.value.fullName,
            JobTitle: element.value.jobTitle,
            Relationship: element.value.relationship,
            Address: element.value.address,
            Email: element.value.email,
            Phone: element.value.phone,
            CanWeContact: element.value.CanWeContact
          });
        }
      });
    } else alert("Invalid references form detected!");;

    return this.referenceForms;
  }
}
