import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ReferencesComponent } from './references.component';

describe('ReferencesComponent', () => {
  let component: ReferencesComponent;
  let fixture: ComponentFixture<ReferencesComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [ReactiveFormsModule, CoreModule, RouterTestingModule],
      declarations: [ReferencesComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ReferencesComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on prepareRefernce', () => {
    it('method should be called', () => {
      const submitFun = spyOn(component, 'prepareReferences');
      component.prepareReferences();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Reference form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be valid when not empty', () => {
      expect(component.referenceForm.valid).toBeTruthy();
    });
    it('Qualifications array validity', () => {
      const r = component.references;
      expect(r.valid).toBeTruthy();
      r.controls.forEach(element => {
        ({
          fullName: 'full name',
          jobTitle: 'job',
          relationship: 'r/n',
          address: 'address',
          email: 'email@as',
          phone: 'phone'
        });
      });
      expect(r.length).toEqual(r.length);
    });
  });
});
