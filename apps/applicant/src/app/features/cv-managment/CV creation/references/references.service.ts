import { Injectable } from '@angular/core';
import { ApiService } from 'apps/applicant/src/app/core/api/api.service';

@Injectable({
  providedIn: 'root'
})
export class ReferencesService {
  id = localStorage.getItem('id');
  constructor(private apiService: ApiService) {}

  deleteReferences(rId) {
      const refer = {
          table: "References",
          id: rId
      }
    return this.apiService.post('/Cv/DeleteItem', refer);
  }
}
