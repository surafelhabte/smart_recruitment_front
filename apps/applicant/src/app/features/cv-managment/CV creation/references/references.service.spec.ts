import { ReferencesService } from "./references.service";
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from 'apps/applicant/src/app/core/api/api.service';



describe('reference service', () => {
  let rService: ReferencesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule],
      providers: [ApiService]
    });

    rService = TestBed.get(ReferencesService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should delete cv', () => {
      const refer = {table: "References", id:1}
    rService.deleteReferences(refer).subscribe((data: any) => {
      expect(data).toBe(refer);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(12);

    httpMock.verify();
  });
});
