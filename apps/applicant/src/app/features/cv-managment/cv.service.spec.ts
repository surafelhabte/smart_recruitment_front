import { CVService } from './cv.service';
import { CvListModel } from './cv.interface';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { CoreModule } from '../../core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from '../../core/api/api.service';

describe('Cv service', () => {
  const blob: any = null;
  let cvService: CVService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule],
      providers: [ApiService]
    });

    cvService = TestBed.get(CVService);
    httpMock = TestBed.get(HttpTestingController);
  });
  it('Should get all cvs successfull', () => {
    const cvs: CvListModel[] = [
      {
        id: 1,
        Title: 'Software Developer',
        DocumentName: File
      },
      { id: 2, Title: 'Software Enginner', DocumentName: File }
    ];

    cvService.getAllMyCvs().subscribe((data: any) => {
      expect(data).toBe(cvs);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    // fire the request with its data we really expect

    req.flush(cvs);

    httpMock.verify();
  });

  it('Should delete cv', () => {
    cvService.deleteCV(12).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(12);

    httpMock.verify();
  });

  it('Should create cv', () => {
    const input = new FormData();
    const cv = {
      Applicant_Id: '122',
      Title: 'Title',
      CoverLetter: 'Cover letter'
    };
    const experienceForms = [
      {
        EmployerName: 'employerName',
        EmployerAddress: 'employerAddress',
        From: new Date(),
        To: new Date(),
        JobTitle: 'jobTitle',
        Achievments: 'achievements',
        Salary: 'salary',
        ReasonForLeaving: 'reasonLeaving',
        Notice: 'notice',
        CarrerLevel: 'carrerLevel'
      }
    ];
    const qualificationForms = [
      {
        Qualification: 'degree',
        Institution: 'aau',
        From: new Date(),
        To: new Date()
      }
    ];
    const refernceForms = [
      {
        fullName: 'full name',
        jobTitle: 'job',
        relationship: 'r/n',
        address: 'address',
        email: 'email@as',
        phone: 'phone'
      }
    ];
    const trainingForms = [
      {
        Course: 'course',
        From: new Date(),
        To: new Date(),
        Details: 'details'
      }
    ];
    input.append('cv', JSON.stringify(cv));
    input.append('Document', new File([blob], 'filename'));
    input.append('Qualifications', JSON.stringify(qualificationForms));
    input.append('Trainings', JSON.stringify(trainingForms));
    input.append('References', JSON.stringify(refernceForms));
    input.append('Experiences', JSON.stringify(experienceForms));
    cvService.createCv(input).subscribe((data: any) => {
      expect(data.Id).toBe(2);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(input);
    httpMock.verify();
  });

  it('Should get single CV successfull', () => {
    const input = new FormData();
    const CvId = 12;
    const cv = {
      Applicant_Id: '122',
      Title: 'Title',
      CoverLetter: 'Cover letter'
    };
    const experienceForms = [
      {
        EmployerName: 'employerName',
        EmployerAddress: 'employerAddress',
        From: new Date(),
        To: new Date(),
        JobTitle: 'jobTitle',
        Achievments: 'achievements',
        Salary: 'salary',
        ReasonForLeaving: 'reasonLeaving',
        Notice: 'notice',
        CarrerLevel: 'carrerLevel'
      }
    ];
    const qualificationForms = [
      {
        Qualification: 'degree',
        Institution: 'aau',
        From: new Date(),
        To: new Date()
      }
    ];
    const refernceForms = [
      {
        fullName: 'full name',
        jobTitle: 'job',
        relationship: 'r/n',
        address: 'address',
        email: 'email@as',
        phone: 'phone'
      }
    ];
    const trainingForms = [
      {
        Course: 'course',
        From: new Date(),
        To: new Date(),
        Details: 'details'
      }
    ];
    input.append('cvId', CvId.toString());
    input.append('cv', JSON.stringify(cv));
    input.append('Document', new File([blob], 'filename'));
    input.append('Qualifications', JSON.stringify(qualificationForms));
    input.append('Trainings', JSON.stringify(trainingForms));
    input.append('References', JSON.stringify(refernceForms));
    input.append('Experiences', JSON.stringify(experienceForms));

    cvService.getCv(12).subscribe((data: any) => {
      expect(data).toBe(input);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(input);

    httpMock.verify();
  });
});
