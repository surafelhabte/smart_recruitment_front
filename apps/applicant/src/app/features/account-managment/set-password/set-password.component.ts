import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.css']
})
export class SetPasswordComponent {
  public formgroup: FormGroup;
  public formSubmitted = false;
  public invalidClass = false;
  public id: any;

  constructor(private service?: AuthService, public actRoute?: ActivatedRoute,private router?: Router) {
    this.id = this.actRoute.snapshot.params.id;
    localStorage.setItem('key', this.id);
    this.formgroup = new FormGroup
    ({
      password : new FormControl('', [Validators.required, Validators.minLength(4)]),
      repassword : new FormControl('', Validators.required)
    });
  }

  get getpassword(): FormControl {
    return this.formgroup.get('password') as FormControl;
  }

  get getrepassword(): FormControl {
    return this.formgroup.get('repassword') as FormControl;
  }

  Submit(): any {
    if (this.getpassword.errors || this.getrepassword.errors) {
        this.invalidClass = true;
        this.formSubmitted = true;
        return;
      } else {
          return this.service.set_password(
            {
              id: this.id,
              Password : this.formgroup.controls.password.value
            })
              .subscribe((response: any) => {
                if (response.status) {
                  window.alert(response.message);                
                  this.router.navigate(['account/login']);
                  localStorage.clear();
                } else {
                  window.alert(response.message);
                  localStorage.clear();
                }
            });
      }
  }

}
