import { TestBed  } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SetPasswordComponent } from './set-password.component';
import { AuthService } from '../../../core/auth/auth.service';
import { ApiService } from '../../../core/api/api.service';


describe('Set New Password Component', () => {
    let service: AuthService;
    let actRoute: ActivatedRoute;
    let page: SetPasswordComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule, FormsModule, ReactiveFormsModule],
        providers: [AuthService, ApiService]
      });
      service = TestBed.get(AuthService);
      actRoute = TestBed.get(ActivatedRoute);

      page = new SetPasswordComponent(service, actRoute);
    });

    it(' form should have a control', () => {
      expect(page.formgroup.contains('password')).toBeTruthy();
      expect(page.formgroup.contains('repassword')).toBeTruthy();
    });

    it(': form control should have a validation required', () => {
      const password = page.formgroup.get('password');
      const repassword = page.formgroup.get('repassword');

      password.setValue('');
      expect(password.valid).toBeFalsy();

      password.setValue('a1');
      expect(password.valid).toBeFalsy();

      repassword.setValue('');
      expect(repassword.valid).toBeFalsy();
    });

    it('Should change password successfull, alert Message and redirect the user', () => {
        const router = TestBed.get(Router);
        const spy_router = spyOn(router, 'navigate');
        const spy_Window = spyOn(window, 'alert');

        service.set_password({id: 1, password: 'abcd1234'})
        .subscribe((response: any) => {
            if (response.status) {
                expect(spy_Window).toHaveBeenCalledWith([response.message]);
                expect(spy_router).toHaveBeenCalledWith(['account/login']);
            }
        });
    });
});
