/*
 * @CreateTime: Nov 5, 2019 12:22 PM
 * @Author: Naol
 * @Contact: nnale8899@gmail.com
 * @Last Modified By: Naol
 * @Last Modified Time: Nov 7, 2019 9:47 AM
 * @Description: Modify Here, Please
 */
import {
  async,
  ComponentFixture,
  TestBed,
  TestModuleMetadata
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../../core/auth/auth.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountManagmentModule } from '../account-managment.module';
import { CoreModule } from '../../../core/core.module';
import { ChangePasswordComponent } from './change-password.component';

describe('ChangePasswordComponent', () => {
  let component: ChangePasswordComponent;
  let fixture: ComponentFixture<ChangePasswordComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [ReactiveFormsModule, CoreModule, RouterTestingModule],
      declarations: [ChangePasswordComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ChangePasswordComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onLogin', () => {
    it('submit be called', () => {
      const submitFun = spyOn(component, 'submit');
      component.submit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Change password form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.passForm.valid).toBeFalsy();
    });
    it('Login fields validity', () => {
      const passwordField = component.password;
      expect(passwordField.valid).toBeFalsy();
      passwordField.setValue('sajdkasdnjasndsak324234');
      expect(passwordField.valid).toBeTruthy();
    });
  });
});
