import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { AuthService } from '../../../core/auth/auth.service';
import { Router } from '@angular/router';
import { PasswordChangeModel } from '../account';

@Component({
  selector: 'smart-recruitment-frontend-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  passForm: FormGroup;
  userId: number;
  submitted = false;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.userId = +localStorage.getItem('id');
  }

  createForm(): void {
    this.passForm = this.formBuilder.group({
      oldPassword: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      confPassword: [
        '',
        [Validators.required, this.PasswordValidator('password')]
      ]
    });
  }

  get password(): FormControl {
    return this.passForm.get('password') as FormControl;
  }

  get confPassword(): FormControl {
    return this.passForm.get('confPassword') as FormControl;
  }

  get oldPassword(): FormControl {
    return this.passForm.get('oldPassword') as FormControl;
  }

  PasswordValidator(confirmPasswordInput: string) {
    let confirmPasswordControl: FormControl;
    let passwordControl: FormControl;

    return (control: FormControl) => {
      if (!control.parent) {
        return null;
      }

      if (!confirmPasswordControl) {
        confirmPasswordControl = control;
        passwordControl = control.parent.get(
          confirmPasswordInput
        ) as FormControl;
        passwordControl.valueChanges.subscribe(() => {
          confirmPasswordControl.updateValueAndValidity();
        });
      }

      if (passwordControl.value !== confirmPasswordControl.value) {
        return {
          notMatch: true
        };
      }
      return null;
    };
  }

  prepareFormData(): PasswordChangeModel | null {
    if (this.passForm.valid) {
      return {
        id: this.userId,
        OldPassword: this.oldPassword.value,
        Password: this.password.value
      };
    } else {
      return null;
    }
  }

  submit() {
    this.submitted = true;
    const payload = this.prepareFormData();

    if (payload && this.userId) {
      this.authService.changePassword(payload).subscribe(data => {
        alert(data.message);
        this.router.navigate(['/']);
      });
    }
  }
}
