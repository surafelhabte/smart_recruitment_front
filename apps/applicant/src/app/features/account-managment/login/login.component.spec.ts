/*
 * @CreateTime: Nov 2, 2019 4:36 PM
 * @Author: Naol
 * @Contact: nnale8899@gmail.com
 * @Last Modified By: Naol
 * @Last Modified Time: Nov 2, 2019 4:36 PM
 * @Description: Modify Here, Please
 */
import {
  async,
  ComponentFixture,
  TestBed,
  TestModuleMetadata
} from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../../core/auth/auth.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountManagmentModule } from '../account-managment.module';
import { CoreModule } from '../../../core/core.module';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [ReactiveFormsModule, CoreModule, RouterTestingModule],
      declarations: [LoginComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onLogin', () => {
    it('submit be called', () => {
      const submitFun = jest.spyOn(component, 'submit');
      component.submit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Login form', () => {
    it('Should be created', () => {
      const obj1 = {
        createLoginForm: () => null
      };
      spyOn(obj1, 'createLoginForm');
      obj1.createLoginForm();
      expect(obj1.createLoginForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.loginForm.valid).toBeFalsy();
    });
    it('Login fields validity', () => {
      const loginField = component.email;
      expect(loginField.valid).toBeFalsy();
      loginField.setValue('email@email.com');
      expect(loginField.valid).toBeTruthy();
    });
  });
});
