/*
 * @CreateTime: Oct 28, 2019 6:14 PM
 * @Author: Naol
 * @Contact: nnale8899@gmail.com
 * @Last Modified By: Naol
 * @Last Modified Time: Nov 5, 2019 4:27 PM
 * @Description: Modify Here, Please
 */
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl
} from '@angular/forms';
import { AuthService } from '../../../core/auth/auth.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';
import { LoginModel } from '../account';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'smart-recruitment-frontend-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitClicked = false;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.createForm();
  }

  ngOnInit() {}

  createForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  get email(): FormControl {
    return this.loginForm.get('email') as FormControl;
  }

  get password(): FormControl {
    return this.loginForm.get('password') as FormControl;
  }

  prepareFormData(): LoginModel | null {
    if (this.loginForm.valid) {
      return {
        email: this.email.value,
        password: this.password.value
      };
    } else {
      return null;
    }
  }

  submit() {
    this.submitClicked = true;
    const payload = this.prepareFormData();
    if (payload) {
      this.authService
        .login(this.email.value, this.password.value)
        .pipe(first())
        .subscribe(data => {
          if (data.length > 0) {
            localStorage.setItem('email', data[0].Email);
            localStorage.setItem('key', data[0].key);
            localStorage.setItem('id', data[0].id);
            localStorage.setItem('firstName', data[0].FirstName);
            localStorage.setItem('surname', data[0].Surname);
            window.location.replace('/');
          } else {
            alert('Invalid email/password');
            this.loginForm.reset();
          }
        });
    }
  }

  openModal() {
    const modalRef = this.modalService.open(ResetPasswordComponent);
  }
}
