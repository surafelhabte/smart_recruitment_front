import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../core/auth/auth.service';
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { environment } from 'apps/applicant/src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'smart-recruitment-frontend-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  currentEmail = localStorage.getItem('email');
  public disable: any = false;
  public formSubmitted: any = false;
  toResetForm: FormGroup;
  constructor(
    public activeModal: NgbActiveModal,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    this.createForm();
  }

  ngOnInit() {}

  createForm(): void {
    this.toResetForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  get email(): FormControl {
    return this.toResetForm.get('email') as FormControl;
  }

  prepareFormData() {
    return { Email: this.email.value , Link: environment.password_reset_url + '/'};
  }

  sendEmail() {
    this.formSubmitted = true;
    if(!this.toResetForm.valid){
      return;
    } else {
      this.disable = true;
      this.authService.resetPassword(this.prepareFormData()).subscribe((response: any) => {
        if(response.status){
          this.disable = false;
          alert(response.message);
          this.activeModal.dismiss('Cross click');
        } else {
          this.disable = false;
          alert(response.message);
          this.activeModal.dismiss('Cross click');
        }
      });
    }
  }
}
