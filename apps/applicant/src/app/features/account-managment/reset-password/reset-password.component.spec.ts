/*
 * @CreateTime: Nov 5, 2019 10:35 AM
 * @Author: Naol
 * @Contact: nnale8899@gmail.com
 * @Last Modified By: Naol
 * @Last Modified Time: Nov 5, 2019 10:41 AM
 * @Description: Modify Here, Please
 */
import {
  async,
  ComponentFixture,
  TestBed,
  TestModuleMetadata
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../../core/auth/auth.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountManagmentModule } from '../account-managment.module';
import { CoreModule } from '../../../core/core.module';
import { ResetPasswordComponent } from './reset-password.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

describe('ResetFormComponent', () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [ReactiveFormsModule, CoreModule, RouterTestingModule],
      declarations: [ResetPasswordComponent],
      providers: [NgbActiveModal]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ResetPasswordComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onLogin', () => {
    it('submit be called', () => {
      const sendEm = jest.spyOn(component, 'sendEmail');
      component.sendEmail();
      expect(sendEm).toHaveBeenCalled();
    });
  });

  it('Should be invalid when empty', () => {
    expect(component.toResetForm.valid).toBeFalsy();
  });
  it('Email field validity', () => {
    const emailField = component.email;
    expect(emailField.valid).toBeFalsy();
    emailField.setValue('email@email.com');
    expect(emailField.valid).toBeTruthy();
  });
});
