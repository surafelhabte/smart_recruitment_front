import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { Routes, RouterModule } from '@angular/router';
import {
  NgbDatepickerModule,
  NgbActiveModal
} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthGuardService } from '../../core/auth/auth-guard.service';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SetPasswordComponent } from './set-password/set-password.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: ':userId/update',
    component: SignupComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'reset_password',
    component: ResetPasswordComponent
  },
  {
    path: 'change_password',
    component: ChangePasswordComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'password/new/:id',
    component: SetPasswordComponent,
  }
];
@NgModule({
  declarations: [
    LoginComponent,
    SignupComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    SetPasswordComponent
  ],
  imports: [
    NgbDatepickerModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [NgbActiveModal],
  exports: [ResetPasswordComponent]
})
export class AccountManagmentModule {}
