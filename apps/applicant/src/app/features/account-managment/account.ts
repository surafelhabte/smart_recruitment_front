export interface AccountModel {
  id?: number;
  FirstName: string;
  Surname: string;
  Phone: string;
  Email: string;
  OldPassword?: string;
  Password: string;
  Gender: string;
  Dob: Date;
  Disability: string;
  photo: File;
  PhotoName?: string;
}

export interface LoginModel {
  email: string;
  password: string;
}

export interface PasswordChangeModel {
  id?: number;
  OldPassword: string;
  Password: string;
}
