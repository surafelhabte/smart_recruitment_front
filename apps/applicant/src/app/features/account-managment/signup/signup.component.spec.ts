import { SignupComponent } from './signup.component';
import {
  TestModuleMetadata,
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { CoreModule } from '../../../core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbDatepicker, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        ReactiveFormsModule,
        CoreModule,
        RouterTestingModule,
        NgbDatepickerModule
      ],
      declarations: [SignupComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onSignup', () => {
    it('Submit should be called', () => {
      const submitFun = spyOn(component, 'submit');
      component.submit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Signup form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.signupForm.valid).toBeFalsy();
    });
    it('Signup fields validity sample for email', () => {
      const signupField = component.email;
      expect(signupField.valid).toBeFalsy();
      signupField.setValue('email@email.com');
      expect(signupField.valid).toBeTruthy();
    });
    it('Signup fields validity sample for gender', () => {
      const signupField = component.gender;
      expect(signupField.valid).toBeFalsy();
      signupField.setValue('Male');
      expect(signupField.valid).toBeTruthy();
    });
  });
});
