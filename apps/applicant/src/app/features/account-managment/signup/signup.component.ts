/*
 * @CreateTime: Oct 31, 2019 4:38 PM
 * @Author: Naol
 * @Contact: nnale8899@gmail.com
 * @Last Modified By: Naol
 * @Last Modified Time: Nov 5, 2019 10:47 AMM
 * @Description: Modify Here, Please
 */
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth/auth.service';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountModel } from '../account';

@Component({
  selector: 'smart-recruitment-frontend-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  public date: any;
  isUpdate = false;
  userId: number;
  submitted = false;
  public photoName: any;
  public photoFile: any;

  public input = new FormData();

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.createForm();
  }
  ngOnInit() {
    this.userId = +this.activatedRoute.snapshot.paramMap.get('userId');
    if (this.userId) {
      this.isUpdate = true;
      this.authService
        .getUserById(this.userId)
        .subscribe((user: AccountModel) => {
          this.initializeForm(user);
          this.signupForm.patchValue({
            firstName: user.FirstName,
            surname: user.Surname,
            dob: user.Dob,
            gender: user.Gender,
            email: user.Email,
            phone: user.Phone,
            disability: user.Disability
          });
          this.photoName = user.PhotoName;
        });
    }
  }

  createForm(): void {
    this.signupForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      surname: ['', Validators.required],
      dob: ['', Validators.required],
      gender: ['', Validators.required],
      phone: ['', Validators.required],
      oldPassword: [],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      confPassword: [
        '',
        [Validators.required, this.PasswordValidator('password')]
      ],
      photo: [''],
      disability: ['', Validators.required]
    });
  }

  initializeForm(user: AccountModel) {
    this.signupForm = this.formBuilder.group({
      firstName: [user.FirstName, Validators.required],
      surname: [user.Surname, Validators.required],
      dob: [user.Dob, Validators.required],
      gender: [user.Gender],
      email: [user.Email, Validators.required],
      phone: [user.Phone, Validators.required],
      photo: [user.photo],
      disability: [user.Disability]
    });
    this.dob.validator = null;
  }

  get firstName(): FormControl {
    return this.signupForm.get('firstName') as FormControl;
  }

  get surname(): FormControl {
    return this.signupForm.get('surname') as FormControl;
  }

  get dob(): FormControl {
    return this.signupForm.get('dob') as FormControl;
  }

  get gender(): FormControl {
    return this.signupForm.get('gender') as FormControl;
  }

  get phone(): FormControl {
    return this.signupForm.get('phone') as FormControl;
  }

  get email(): FormControl {
    return this.signupForm.get('email') as FormControl;
  }

  get password(): FormControl {
    return this.signupForm.get('password') as FormControl;
  }

  get confPassword(): FormControl {
    return this.signupForm.get('confPassword') as FormControl;
  }

  get photo(): FormControl {
    return this.signupForm.get('photo') as FormControl;
  }

  get disability(): FormControl {
    return this.signupForm.get('disability') as FormControl;
  }
  
  get oldPassword(): FormControl {
    return this.signupForm.get('oldPassword') as FormControl;
  }

  prepareFormData(): any | null {
    if (this.signupForm.valid) {
      this.input.append('FirstName', this.firstName.value);
      this.input.append('SurName', this.surname.value);
      this.input.append('Phone', this.phone.value);
      this.input.append('Email', this.email.value);
      this.input.append('Gender', this.gender.value);
      this.input.append('Dob', this.dob.value);
      if(this.photoFile !== undefined){
        this.input.append('photo', this.photoFile);
      }
      this.input.append('Disability', this.disability.value);
      this.input.append('Activated', 'False');
      return this.input;
    } else {
      return null;
    }
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      this.photoFile = event.target.files[0];
    }
  }

  PasswordValidator(confirmPasswordInput: string) {
    let confirmPasswordControl: FormControl;
    let passwordControl: FormControl;

    return (control: FormControl) => {
      if (!control.parent) {
        return null;
      }

      if (!confirmPasswordControl) {
        confirmPasswordControl = control;
        passwordControl = control.parent.get(
          confirmPasswordInput
        ) as FormControl;
        passwordControl.valueChanges.subscribe(() => {
          confirmPasswordControl.updateValueAndValidity();
        });
      }

      if (passwordControl.value !== confirmPasswordControl.value) {
        return {
          notMatch: true
        };
      }
      return null;
    };
  }

  submit() {
    this.submitted = true;
    const payload = this.prepareFormData();

    if (payload) {
      if (this.userId) {
        this.input.append('id', this.userId.toString());
        this.input.append('PhotoName', this.photoName);
        this.authService.updateProfile(payload).subscribe(data => {
          alert(data.message);
        });
      } else {
        this.input.append('Password', this.password.value);
        this.authService.signUp(this.input).subscribe(data => {
          if (data.status === true) {
            alert(data.message);
            this.router.navigate(['/login']);
          }
          else {
            alert(data.message);
          }

        });
      }
    }
  }
}
