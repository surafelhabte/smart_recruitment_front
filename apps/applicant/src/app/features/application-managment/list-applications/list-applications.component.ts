import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../application.service';

@Component({
  selector: 'smart-recruitment-frontend-list-applications',
  templateUrl: './list-applications.component.html',
  styleUrls: ['./list-applications.component.css']
})
export class ListApplicationsComponent implements OnInit {
  applications;

  constructor(private appService: ApplicationService) {}

  ngOnInit() {
    this.appService.listApplication().subscribe(data => {
      this.applications = data;
    });

    const next = document.getElementsByClassName('next');
    /* if (this.applications.Status === 'Failed') {
      next.style.color = 'red'; 
    }
    if (this.applications.Status === 'Pass') {
      next.style.color = 'Green';
    } */

    // next.style.background = 'green';
  }

  withdraw(id) {
    const vacancyid = { id: id };
    const confirmation = confirm(
      'Are you sure you want to withraw this application'
    );
    if (confirmation === true) {
      this.appService.withdraw(vacancyid).subscribe(data => {
        alert(data.message);
        this.ngOnInit();
      });
    }
  }
}
