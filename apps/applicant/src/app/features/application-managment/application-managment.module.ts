import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListApplicationsComponent } from './list-applications/list-applications.component';
import { ApplicationService } from './application.service';
import { AuthGuardService } from '../../core/auth/auth-guard.service';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'list',
    component: ListApplicationsComponent,
    canActivate: [AuthGuardService]
  },
];


@NgModule({
  declarations: [ListApplicationsComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ],
  providers: [ApplicationService]
})
export class ApplicationManagmentModule { }
