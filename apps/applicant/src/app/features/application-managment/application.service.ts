import { Injectable } from '@angular/core';
import { ApiService } from '../../core/api/api.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {
  id = localStorage.getItem('id');

  constructor(private apiService: ApiService) { }

  listApplication() {
    return this.apiService.post('/application/GetApplications', {
         applicant_id: this.id
    });
  }

  withdraw(vacancyId) {
    return this.apiService.post('/application/Withdraw', vacancyId);
  }
}
