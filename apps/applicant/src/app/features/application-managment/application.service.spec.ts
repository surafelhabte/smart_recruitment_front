import { ApplicationService } from "./application.service";
import { HttpTestingController, HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { CoreModule } from "../../core/core.module";
import { RouterTestingModule } from "@angular/router/testing";
import { ApiService } from "../../core/api/api.service";


describe('Application service', () => {
  const blob: any = null;
  let appService: ApplicationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule],
      providers: [ApiService]
    });

    appService = TestBed.get(ApplicationService);
    httpMock = TestBed.get(HttpTestingController);
  });
  it('Should get all applications successfull', () => {
    const applicant = [
      { applicant : 1 }
    ];

    appService.listApplication().subscribe((data: any) => {
      expect(data).toBe(applicant);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    // fire the request with its data we really expect

    req.flush(applicant);

    httpMock.verify();
  });

  it('Should withdraw cv', () => {
    appService.withdraw({id: 12}).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(12);

    httpMock.verify();
  });
});
