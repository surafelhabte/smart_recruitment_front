import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CoreModule } from '../core/core.module';
import { IndexModule } from './index/index.module';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./index/index.module').then(m => m.IndexModule)
  },
  {
    path: 'account',
    loadChildren: () =>
      import('./account-managment/account-managment.module').then(
        m => m.AccountManagmentModule
      )
  },
  {
    path: 'cv',
    loadChildren: () =>
      import('./cv-managment/cv-managment.module').then(
        m => m.CvManagmentModule
      )
  },
   {
    path: 'application',
    loadChildren: () =>
      import('./application-managment/application-managment.module').then(
        m => m.ApplicationManagmentModule
      )
  }
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreModule,
    IndexModule
  ]
})
export class FeaturesModule {}
