import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { ApiService } from '../../core/api/api.service';
import { CoreModule } from '../../core/core.module';
import { VacancyModule } from './vacancy/vacancy.module';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./vacancy/vacancy.module').then(m => m.VacancyModule)
  }
];
@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    CoreModule,
    VacancyModule,
    RouterModule.forChild(routes)
  ],
  exports: [HomeComponent],
  providers: [ApiService]
})
export class IndexModule {}
