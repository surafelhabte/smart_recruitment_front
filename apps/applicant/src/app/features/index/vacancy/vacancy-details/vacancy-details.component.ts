import { Component, OnInit, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { VacancyService } from '../vacancy.service';
import { ActivatedRoute } from '@angular/router';
import { ApplyComponent } from '../apply/apply.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'smart-recruitment-frontend-vacancy-details',
  templateUrl: './vacancy-details.component.html',
  styleUrls: ['./vacancy-details.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VacancyDetailsComponent implements OnInit {
  vacancyId: number;
  vacancyData: any[] = [];
  public vacancyDetails: {
    Availability: string
    Details: string
    Field_Of_Study: Array<any>
    JobId: string
    Location: string
    Remark: string
    Sector: []
    Special_Work_Experience: string
    Special_Year_Of_Experience: string
    Term_Of_Employement: string
    Title: string
    Work_Experience: Array<any>;
    created_date: Date
    deadline: Date
    id: string
  } = {
      Availability: null,
      Details: null,
      Field_Of_Study: [],
      JobId: null,
      Location: null,
      Remark: null,
      Sector: [],
      Special_Work_Experience: null,
      Special_Year_Of_Experience: null,
      Term_Of_Employement: null,
      Title: null,
      Work_Experience: [],
      created_date: null,
      deadline: null,
      id: null
    }
  qualification: any[] = [];

  constructor(
    private vacancyService: VacancyService,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private ref: ChangeDetectorRef
  ) { 
  }

  ngOnInit() {
    this.vacancyId = +this.activatedRoute.snapshot.paramMap.get('vacancyId');
    if (this.vacancyId) {
      this.vacancyService
        .getVacancyDetail(this.vacancyId)
        .subscribe((data: any) => {
          data ? (this.vacancyDetails = data.vacancy) && (this.qualification = data.qualifications) : null;
          this.ref.detectChanges();
        });
    }
  }

  apply(id) {
    const modalRef = this.modalService.open(ApplyComponent);
    modalRef.componentInstance.VacancyId = id;
    modalRef.componentInstance.vacancyTitle = this.vacancyDetails.Title;
  }

}
