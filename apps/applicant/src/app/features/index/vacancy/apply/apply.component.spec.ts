import { ApplyComponent } from './apply.component';
import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'apps/applicant/src/app/core/api/api.service';

describe('ApplyComponent', () => {
  let component: ApplyComponent;
  let fixture: ComponentFixture<ApplyComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [ReactiveFormsModule, CoreModule, RouterTestingModule],
      declarations: [ApplyComponent],
      providers: [NgbActiveModal, ApiService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ApplyComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on', () => {
    it('submit be called', () => {
      const apply = jest.spyOn(component, 'apply');
      component.apply();
      expect(apply).toHaveBeenCalled();
    });
  });

  it('Should be invalid when empty', () => {
    expect(component.applyForm.valid).toBeFalsy();
  });
  it('Email field validity', () => {
    const cvField = component.cv;
    expect(cvField.valid).toBeFalsy();
    cvField.setValue('Appdiv');
    expect(cvField.valid).toBeTruthy();
  });
});
