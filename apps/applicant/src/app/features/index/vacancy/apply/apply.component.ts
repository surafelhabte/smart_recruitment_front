import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VacancyService } from '../vacancy.service';
import { CVService } from '../../../cv-managment/cv.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'smart-recruitment-frontend-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.css']
})
export class ApplyComponent implements OnInit {
  applyForm: FormGroup;
  myCvs: any = [];
  applicantId = localStorage.getItem('id');
  isLoggedIn = false;
  public VacancyId: any;
  public vacancyTitle: any;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private vacancyService: VacancyService,
    private cvService: CVService,
    private router: Router,
    private location: Location
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.applicantId ? (this.isLoggedIn = true) : null;
    this.cvService.getAllMyCvs().subscribe(data => {
      this.myCvs = data;
    });
  }

  createForm(): void {
    this.applyForm = this.formBuilder.group({
      cv: ['', Validators.required]
    });
  }

  get cv(): FormControl {
    return this.applyForm.get('cv') as FormControl;
  }

  prepareFormData(): any | null {
    if (this.applyForm.valid) {
      return {
        CVId: this.cv.value,
        applicant_id: this.applicantId,
        VacancyId: this.VacancyId,
        Title: this.vacancyTitle,
        Status: 'Inprocess'
      };
    } else {
      return null;
    }
  }

  apply() {
    this.vacancyService.apply(this.prepareFormData()).subscribe(data => {
      this.vacancyService.apply_error = data.message;
      this.activeModal.dismiss('Cross click');
      this.router.navigateByUrl('/');
    });
  }

  linkToLogin() {
    this.router.navigate(['/account/login']);
    this.activeModal.dismiss('Cross click');
  }
}
