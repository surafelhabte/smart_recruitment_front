import {
  async,
  ComponentFixture,
  TestBed,
  TestModuleMetadata
} from '@angular/core/testing';

import { ListVacancyComponent } from './list-vacancy.component';
import { DebugElement } from '@angular/core';
import { CoreModule } from 'apps/applicant/src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { VacancyService } from '../vacancy.service';
import { CvManagmentModule } from '../../../cv-managment/cv-managment.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbPaginationModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HtmlSanitaizerModule } from '../../htmlSanitaizer/html-sanitaizer.module';

describe('ListVacancyComponent', () => {
  let component: ListVacancyComponent;
  let fixture: ComponentFixture<ListVacancyComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        CoreModule,
        RouterTestingModule,
        ReactiveFormsModule,
        CvManagmentModule,
        NgbPaginationModule,
        HtmlSanitaizerModule,
        NgbModule
      ],
      declarations: [ListVacancyComponent],
      providers: [VacancyService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ListVacancyComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
