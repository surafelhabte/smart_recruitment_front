import { Component, OnInit } from '@angular/core';
import { VacancyService } from '../vacancy.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'smart-recruitment-frontend-list-vacancy',
  templateUrl: './list-vacancy.component.html',
  styleUrls: ['./list-vacancy.component.css']
})
export class ListVacancyComponent implements OnInit {
  vacancyList: any = [];
  searchForm: FormGroup;
  searchAdvanceForm: FormGroup;
  searchMessage: string;
  locations: any = [];
  public loc = ['Location'];
  btnText = 'Advance search';
  page = 1;
  public System_Message: any = { show: false, content: '', cssStyle: '' };


  constructor(
    private vacancyService: VacancyService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.createForm();
    this.createAdvanceForm();
    if (this.vacancyService.apply_error !== undefined) {
      this.System_Message = { show: true, content: this.vacancyService.apply_error, cssStyle: 'danger' };
    }
  }

  ngOnInit() {
    this.vacancyService.listVacancy().subscribe(data => {
      this.vacancyList = data;
    });

    this.vacancyService.getLocation(this.loc).subscribe((data: any) => {
      this.locations = data.Location;
    });
  }

  createForm(): void {
    this.searchForm = this.formBuilder.group({
      searchItem: ['', Validators.required]
    });
  }

  createAdvanceForm(): void {
    this.searchAdvanceForm = this.formBuilder.group({
      experience: [null],
      location: [null],
      job: [null]
    });
  }

  get searchItem(): FormControl {
    return this.searchForm.get('searchItem') as FormControl;
  }

  get experience(): FormControl {
    return this.searchAdvanceForm.get('experience') as FormControl;
  }

  get job(): FormControl {
    return this.searchAdvanceForm.get('job') as FormControl;
  }

  get location(): FormControl {
    return this.searchAdvanceForm.get('location') as FormControl;
  }

  prepareFormData(): any | null {
    if (this.searchForm.valid) {
      return { Search: this.searchItem.value };
    } else {
      return null;
    }
  }

  prepareAdvanceFormData(): any | null {
    if ((this.location.value || this.job.value || this.experience.value) !== null) {
      return {
        keyword: this.job.value,
        Experience: this.experience.value,
        Location: this.location.value
      };
    } else {
      return null;
    }
  }

  search() {
    const payload = this.prepareFormData();
    this.vacancyService.getBySearch(payload.Search).subscribe(data => {
      data.status ? (this.vacancyList = data.message) : alert(data.message);
    });
  }

  searchAdvance() {
    const payload = this.prepareAdvanceFormData();
    if (payload) {
      this.vacancyService.getByFilter(payload).subscribe((data: any) => {
        data.status ? (this.vacancyList = data.message) : alert(data.message);
      });
    }
  }

  selected(i) {
    const className = document.getElementById('aa' + i).className;
    if (className == 'collapse') {
      document.getElementById('headingOne' + i).style.background = '#ffc10788';
      document.getElementById('aa' + i).style.background = '#ffc10733';
    } else {
      document.getElementById('headingOne' + i).style.background = '#28a74536';
    }
  }

  advanceClicked() {
    const className = document.getElementById('multiCollapseExample2')
      .className;
    if (className !== 'multi-collapse collapse show') {
      this.btnText = 'Collapse';
    } else {
      this.btnText = 'Advance search';
    }
  }

  viewDetail(id) {
    this.router.navigate(['vacancy/' + id + '/detail']);
  }

  close_alert() {
    this.System_Message.show = false;
    this.vacancyService.apply_error = undefined;
  }

}
