import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/api/api.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VacancyService {

  public apply_error: any;
  id = localStorage.getItem('id');

  constructor(private apiService: ApiService, private httpClient: HttpClient) { }

  listVacancy() {
    return this.apiService.get('/vacancy/GetVacancies');
  }

  getBySearch(searchValue) {
    return this.apiService.get('/vacancy/Search/' + searchValue);
  }

  getByFilter(filterValue) {
    return this.apiService.post('/vacancy/Filter', filterValue);
  }

  apply(cvId) {
    return this.apiService.post('/application/Apply', cvId
    );
  }

  getLocation(location) {
    return this.apiService.post('/Lookup/load_lookups', location
    );
  }

  getVacancyDetail(id: any) {
    return this.apiService.get('/vacancy/GetVacancy/' + id)
  }
}
