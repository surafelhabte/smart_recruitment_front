import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListVacancyComponent } from './list-vacancy/list-vacancy.component';
import { VacancyService } from './vacancy.service';
import { CoreModule } from '../../../core/core.module';
import { Routes, RouterModule } from '@angular/router';
import { ApplyComponent } from './apply/apply.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbPaginationModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HtmlSanitaizerModule } from '../htmlSanitaizer/html-sanitaizer.module';
import { VacancyDetailsComponent } from './vacancy-details/vacancy-details.component';

const routes: Routes = [
  {
    path: '',
    component: ListVacancyComponent
  },
  {
    path: 'vacancy/:vacancyId/detail',
    component: VacancyDetailsComponent
  }
];

@NgModule({
  declarations: [ListVacancyComponent, ApplyComponent, VacancyDetailsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CoreModule,
    NgbModule,
    RouterModule.forChild(routes),
    NgbPaginationModule,
    HtmlSanitaizerModule
  ],
  providers: [VacancyService],
  
  exports: [ApplyComponent]
})
export class VacancyModule {}
