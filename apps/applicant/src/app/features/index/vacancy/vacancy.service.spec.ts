import { VacancyService } from './vacancy.service';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { CoreModule } from '../../../core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from '../../../core/api/api.service';
import { CvListModel } from '../../cv-managment/cv.interface';

describe('Cv service', () => {
  const blob: any = null;
  let vacancyService: VacancyService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, RouterTestingModule],
      providers: [ApiService]
    });

    vacancyService = TestBed.get(VacancyService);
    httpMock = TestBed.get(HttpTestingController);
  });
  it('Should get all cvs successfull', () => {
    const vacancy = [
      {
        Availability: 'As Soon as Possible',
        Dead_line: '2019-09-12',
        Hired: 'False',
        JobId: '1',
        Location: 'Sengatera',
        Term_Of_Employement: 'Temporary',
        Title: 'Junior Programmer',
        Type_Of_Employement: 'New',
        created_date: '2019-11-11 16:08:14',
        id: '1'
      }
    ];

    vacancyService.listVacancy().subscribe((data: any) => {
      expect(data).toBe(vacancy);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(vacancy);

    httpMock.verify();
  });

  it('Should get all locations', () => {
    const location = [
      { text: 'Piassa', value: 'Piassa' },
      { text: 'Bole', value: 'Bole' }
    ];

    vacancyService.getLocation(location).subscribe((data: any) => {
      expect(data).toBe(location);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    // fire the request with its data we really expect

    req.flush(location);

    httpMock.verify();
  });

  it('shold get by search', () => {
    const vacancy = [
      {
        Availability: 'As Soon as Possible',
        Dead_line: '2019-09-12',
        Hired: 'False',
        JobId: '1',
        Location: 'Sengatera',
        Term_Of_Employement: 'Temporary',
        Title: 'Junior Programmer',
        Type_Of_Employement: 'New',
        created_date: '2019-11-11 16:08:14',
        id: '1'
      }
    ];
    vacancyService.getLocation(vacancy).subscribe((data: any) => {
      expect(data).toBe(vacancy);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    // fire the request with its data we really expect

    req.flush(vacancy);

    httpMock.verify();
  });

  it('shold send apply', () => {
    const apply = [
      {
        Cvid: '1'
      }
    ];
    vacancyService.getLocation(apply).subscribe((data: any) => {
      expect(data).toBe(apply);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    // fire the request with its data we really expect

    req.flush(apply);

    httpMock.verify();
  });
});
