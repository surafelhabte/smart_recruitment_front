import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'smart-recruitment-frontend-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public currentUser: string;
  dropdownMenuText: string;
  isLoggedIn: boolean;
  fullName =
    localStorage.getItem('firstName') + ' ' + localStorage.getItem('surname');
  email = localStorage.getItem('email');
  userId = localStorage.getItem('id');
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.currentUser = localStorage.getItem('userData');
    this.email != null ? (this.isLoggedIn = true) : (this.isLoggedIn = false);
    this.isLoggedIn ? this.dropdownMenuText = this.fullName : this.dropdownMenuText = "Register/Login"
  }

  toChangeProfile() {
    this.router.navigate(['/account/' + this.userId+ '/update'])
  }

  logOut() { 
    this.authService.logout();
    window.location.reload();
  }
}
