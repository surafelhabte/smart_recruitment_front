import { Component } from '@angular/core';

@Component({
  selector: 'smart-recruitment-frontend-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'applicant';
}
