import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";

export class AppUserAuth {
    userName = "";
    accessToken = "";
    isAuthenticated = false;
    roles: RoleModel[] = [];
}

export interface RoleModel {
    role: string;
    status: string;
}

export class AppUser {
    userName = "";
    password = "";
}
@Injectable()
export class SecurityService {
    securityObject: AppUserAuth;
    roleInfo = {
        accessToken: "xxxxsampleaccesstokenxxxx",
        isAuthenticated: true,
        userName: 'Admin',
        roles: [
            { role: 'manageJobs', status: 'true' },
            { role: 'viewJobRequirments', status: 'true' },
            { role: 'createJobRequirment', status: 'true' },
            { role: 'editJobRequirment', status: 'true' },
            { role: 'deleteJobRequirment', status: 'true' },
            { role: 'viewJobRequest', status: 'true' },
            { role: 'viewJobRequestList', status: 'true' },
            { role: 'createJobRequest', status: 'true' },
            { role: 'deleteRequest', status: 'true' },

            { role: 'manageAssessment', status: 'true' },
            { role: 'viewAssessmentProcedure', status: 'true' },
            { role: 'createAssessmentProcedure', status: 'true' },
            { role: 'editAssessmentProcedure', status: 'true' },
            { role: 'deleteAssessmentProcedure', status: 'true' },
            
            { role: 'viewSchedules', status: 'true' },
            { role: 'editSchedules', status: 'true' },
            { role: 'createSchedules', status: 'true' },
            { role: 'deleteSchedules', status: 'true' },

            { role: 'viewVacancies', status: 'true' },
            { role: 'createVacancy', status: 'true' },
            { role: 'editVacancy', status: 'true' },
            { role: 'deleteVacancy', status: 'true' },

            { role: 'manageApplications', status: 'true' },
            { role: 'managePriliminaryListedApplications', status: 'true' },
            { role: 'manageEvaluation', status: 'true' },
            { role: 'manageOffering', status: 'true' },
            { role: 'manageRecruitment', status: 'true' },

            { role: 'managePanelMembers', status: 'true' },
            { role: 'viewPanelMembers', status: 'true' },
            { role: 'editPanelMembers', status: 'true' },
            { role: 'createPanelMembers', status: 'true' },
            { role: 'deletePanelMembers', status: 'true' },

            { role: 'manageSettings', status: 'true' },
            { role: 'viewLookups', status: 'true' },
            { role: 'createLookup', status: 'true' },
            { role: 'editLookup', status: 'true' }, 
            { role: 'deleteLookup', status: 'true' },

            { role: 'manageReport', status: 'true' },
            { role: 'reportApplication', status: 'true' },
            { role: 'reportFailedApplication', status: 'true' },
            { role: 'reportSuccessfullApplications', status: 'true' },
            { role: 'reportOfferedApplicants', status: 'true' },
            { role: 'reportHiredApplicants', status: 'true' },
            { role: 'reportReservedApplicants', status: 'true' },
            { role: 'reportRecruitmentRequest', status: 'true' },
            { role: 'reportShortlistedApplicants', status: 'true' },
            { role: 'reportVacancies', status: 'true' },
            { role: 'reportExpiredVacancies', status: 'true' },
            { role: 'saveReport', status: 'true' },
            { role: 'editViewReport', status: 'true' },
            { role: 'deleteViewReport', status: 'true' },
        ]
    }
    logIn(): Observable<AppUserAuth> {
        if (!this.securityObject.isAuthenticated && true) {
            this.httpClient.get<AppUserAuth>(`http://${window.location.hostname}/smarthrm/authenticate/my_role/recruitment`)
                .subscribe(
                    (data: AppUserAuth) => (this.securityObject = data),
                    (error: HttpErrorResponse) => (window.location.href = `http://${window.location.hostname}/smarthrm/authenticate/logout`)
                );
        }
        localStorage.setItem("recruitmentAccessToken", JSON.stringify(this.roleInfo));

        return of<AppUserAuth>(this.roleInfo);
    }
    constructor(private httpClient: HttpClient) {
        this.securityObject = new AppUserAuth();
        localStorage.setItem("recruitmentAccessToken", JSON.stringify(this.roleInfo));
        if (localStorage.getItem("recruitmentAccessToken")) {
            this.securityObject = JSON.parse(
                localStorage.getItem("recruitmentAccessToken")
            );
        }
    }

    resetSecurityObject(): void {
        this.securityObject.userName = "";
        this.securityObject.accessToken = "";
        this.securityObject.roles = [];
        this.securityObject.isAuthenticated = false;
        localStorage.removeItem("recruitmentAccessToken");
    }

    logOut() {
        this.resetSecurityObject();
        window.location.href = `http://${window.location.hostname}/smarthrm/authenticate/logout`;
    }

    hasClaim(role: any, status?: any): boolean {
        let ret = false;
        if (typeof role === "string") {
            ret = this.isClaimValid(role, status);
        } else {
            const claims: string[] = role;

            if (claims) {
                for (let index = 0; index < claims.length; index++) {
                    ret = this.isClaimValid(claims[index]);
                    if (ret) {
                        break;
                    }
                }
            }
        }
        return ret;
    }

    private isClaimValid(role: string, status?: string): boolean {
        let ret = false;
        let auth: AppUserAuth = null;

        auth = this.securityObject;

        if (auth) {
            if (role.indexOf(":") >= 0) {
                const words: string[] = role.split(":");
                role = words[0].toLocaleLowerCase();
                status = words[1];
            } else {
                role = role.toLocaleLowerCase();
                status = status ? status : "true";
            }

            const s = auth.roles.find(
                c =>
                    c.role.toLocaleLowerCase() === role &&
                    c.status === status
            );

            ret =
                auth.roles.find(
                    c =>
                        c.role.toLocaleLowerCase() === role &&
                        c.status === status
                ) != null;
        }

        return ret;
    }
}
