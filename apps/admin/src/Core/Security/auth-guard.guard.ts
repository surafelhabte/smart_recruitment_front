import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { SecurityService } from './security-service';
import { Observable } from 'rxjs';


@Injectable()
export class AuthGuardGuard implements CanActivate {
  constructor(
    private securityService: SecurityService,
    private router: Router
  ) {
    this.securityService.logIn().subscribe();
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const role: string = next.data["role"];
    if (!this.securityService.securityObject.isAuthenticated) {
      window.location.href = `http://${window.location.hostname}/smarthrm/authenticate/logout`;
    } else {
      if (this.securityService.hasClaim(role)) {
        return true;
      } else {
        this.router.navigate(["unauthorized"]);
      }
      return true;
    }
    return true;
  }
  canLoad(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.securityService.securityObject.isAuthenticated) {
      window.location.href = `http://${window.location.hostname}/smarthrm/authenticate/logout`;
      return false;
    } else {
      return true;
    }
  }
}
