import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NodeSelectEventArgs } from '@syncfusion/ej2-navigations';
import {
  TreeViewComponent,
  SidebarComponent
} from '@syncfusion/ej2-angular-navigations';
import { NAVIGATION_LINKS } from './navigation-data.model';
import { SecurityService } from '../../Security/security-service';
import { ButtonComponent } from '@syncfusion/ej2-angular-buttons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @ViewChild('tree', { static: false })
  public tree: TreeViewComponent;
  @ViewChild('togglebtn', { static: false })
  public togglebtn: ButtonComponent;
  @ViewChild('sidebar', { static: false })
  public sidebar: SidebarComponent;
  public type = 'Push';
  public target = '.content';
  @Input() roles: any;
  allowedMenus: any[] = [];
  allowedSubMenus: any[] = [];
  recruitmentLinks = NAVIGATION_LINKS;
  field: object;
  sidebarOpened: boolean;
  public onCreated(args: any) {
    this.sidebar.show();
    this.sidebarOpened = true;
  }
  username: string;
  public Recruitment_Request: any = 0;

  constructor(
    private router: Router,
    private securityService: SecurityService
  ) {
    this.field = {
      dataSource: this.recruitmentLinks,
      id: 'id',
      text: 'name',
      child: 'subChild',
      expanded: 'expanded',
      selected: 'selected',
      enabled: 'enabled'
    };

    for (let index = 1; index < this.recruitmentLinks.length; index++) {
      const subChild = this.recruitmentLinks[index].subChild;
      if (subChild) {
        for (let i = subChild.length - 1; i > -1; i--) {
          if (!this.securityService.hasClaim(subChild[i].roleName)) {
            subChild.splice(i, 1);
          } else {
          }
        }
        if (this.recruitmentLinks[index].subChild.length === 0) {
          this.recruitmentLinks.splice(index, 1);
          --index;
        }
      } else {
        if (
          !this.securityService.hasClaim(this.recruitmentLinks[index].roleName)
        ) {
          this.recruitmentLinks.splice(index, 1);
        } else {
        }
        /*  this.recruitmentLinks.splice(index, 1);
         --index; */
      }
    }
  }

  ngOnInit() {
    const u = JSON.parse(localStorage.getItem('recruitmentAccessToken'));
    this.username = u.userName;
    // this.roleMatch(this.roles);
    this.Recruitment_Request = localStorage.getItem('Recruitment_Request');
    setInterval(function() {
      this.Recruitment_Request = localStorage.getItem('Recruitment_Request');
    }, 3000);
  }

  public loadRoutingContent(args: NodeSelectEventArgs): void {
    const data: any = this.tree.getTreeData(args.node);
    const routerLink: string = data[0].url;

    if (routerLink !== 'parent') {
      this.router.navigate([routerLink]);
    }
    this.securityService.logIn().subscribe();
  }

  btnClick() {
    if (this.sidebarOpened) {
      this.sidebar.hide();
      this.sidebarOpened = !this.sidebarOpened;
    } else {
      this.sidebar.show();
      this.sidebarOpened = !this.sidebarOpened;
    }
  }

  logout() {
    this.securityService.logOut();
  }
}
