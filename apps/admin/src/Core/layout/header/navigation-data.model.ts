export const NAVIGATION_LINKS = [
    {
        id: "00",
        name: "Dashboard",
        enabled: true,
        expanded: false,
        url: "",
        selected: true,
        icon: "fas fa-columns",
        roleName: "viewDashboard"
    },
    {
        id: "01",
        name: "Jobs",
        expanded: true,
        enabled: true,
        url: "parent",
        icon: "fas fa-user-graduate",
        selected: false,
        roleName: "manageJobs",
        subChild: [
            {
                id: "01-01",
                name: "Requirment",
                url: "/job-requirment",
                expanded: false,
                selected: false,
                enabled: true,
                icon: "fas fa-question-circle",
                roleName: "viewJobRequirments"
            },
            {
                id: "01-02",
                name: "Request",
                url: "job/request",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-reply",
                roleName: "viewJobRequest"
            }
        ]
    },
    {
        id: "02",
        name: "Assessment",
        expanded: true,
        enabled: true,
        url: "parent",
        selected: false,
        icon: "fas fa-file-medical-alt",
        roleName: "manageAssessment",
        subChild: [
            {
                id: "02-01",
                name: "Procedures",
                url: "assessment-procedure",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-list-ol",
                roleName: "viewAssessmentProcedure"
            },
            {
                id: "02-02",
                name: "Schedule",
                url: "schedule",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "far fa-calendar-alt",
                roleName: "viewSchedules"
            }
        ]
    },
    {
        id: "03",
        name: "Vacancy",
        enabled: true,
        expanded: false,
        url: "vacancy",
        selected: true,
        icon: "fas fa-clipboard",
        roleName: "viewVacancies"
    },
    {
        id: "04",
        name: "Screening",
        enabled: true,
        expanded: false,
        url: "application",
        selected: true,
        icon: "fas fa-chalkboard-teacher",
        roleName: "manageApplications"
    },
    {
        id: "05",
        name: "Preliminary listed applications",
        enabled: true,
        expanded: false,
        url: "preliminaryListedApplications",
        selected: true,
        icon: "fas fa-clipboard-list",
        roleName: "managePriliminaryListedApplications"
    },
    {
        id: "06",
        name: "Evaluation",
        enabled: true,
        expanded: false,
        url: "evaluation",
        selected: true,
        icon: "fas fa-clipboard-check",
        roleName: "manageEvaluation"
    },
    {
        id: "07",
        name: "Offering",
        enabled: true,
        expanded: false,
        url: "offering",
        selected: true,
        icon: "fas fa-external-link-alt",
        roleName: "manageOffering"
    },
    {
        id: "08",
        name: "Recruit",
        enabled: true,
        expanded: false,
        url: "recruit",
        selected: true,
        icon: "fas fa-user-check",
        roleName: "manageRecruitment"
    },
    {
        id: "09",
        name: "Settings",
        expanded: true,
        enabled: true,
        url: "parent",
        selected: false,
        icon: "fas fa-cogs",
        roleName: "manageSettings",
        subChild: [
            {
                id: "03-01",
                name: "Lookup",
                url: "lookup",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-eye",
                roleName: "viewLookups"
            }
        ]
    },
    {
        id: "10",
        name: "Reports",
        expanded: false,
        enabled: true,
        url: "parent",
        selected: false,
        icon: "fas fa-newspaper",
        roleName: "manageReport",
        subChild: [
            {
                id: "04-01",
                name: "Applications",
                url: "/reports/generate/Applications",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportApplication"
            },
            {
                id: "04-02",
                name: "Failed applications",
                url: "/reports/generate/FailedApplication",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportFailedApplication"
            },
            {
                id: "04-03",
                name: "Successfull applications",
                url: "/reports/generate/SuccessfullApplications",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportSuccessfullApplications"
            },
            {
                id: "04-04",
                name: "Offered applicants",
                url: "/reports/generate/OfferdApplicants",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportOfferedApplicants"
            },
            {
                id: "04-05",
                name: "Hired applicants",
                url: "/reports/generate/HiredApplicants",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportHiredApplicants"
            },
            {
                id: "04-06",
                name: "Reserved applicants",
                url: "/reports/generate/ReservedApplicants",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportReservedApplicants"
            },
            {
                id: "04-07",
                name: "Recruitment request",
                url: "/reports/generate/RecruitmentRequest",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportRecruitmentRequest"
            },
            {
                id: "04-09",
                name: "Shortlisted applications",
                url: "/reports/generate/ShortlistedApplications",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportShortlistedApplicants"
            },
            {
                id: "04-09",
                name: "Vacancies",
                url: "/reports/generate/Vacancies",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportVacancies"
            },
            {
                id: "04-09",
                name: "Expired vacancy",
                url: "/reports/generate/ExpiredVacancy",
                expanded: false,
                enabled: true,
                selected: false,
                icon: "fas fa-pager",
                roleName: "reportExpiredVacancies"
            }
        ]
    },
    {
        id: "11",
        name: "Saved Reports",
        expanded: false,
        enabled: true,
        url: "reports/view/saved",
        selected: false,
        icon: "fas fa-newspaper",
        roleName: "saveReport",
       
    },
    {
        id: "12",
        name: "Panel Members",
        enabled: true,
        expanded: false,
        url: "panel",
        selected: true,
        icon: "fas fa-clipboard-list",
        roleName: "managePanelMembers"
    },
];
