import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicModule } from '../../Shared/Modules/basic.module';
import {
  SidebarModule,
  TreeViewModule
} from '@syncfusion/ej2-angular-navigations';

@NgModule({
  declarations: [],
  imports: [CommonModule, BasicModule, SidebarModule, TreeViewModule],
  exports: [SidebarModule, TreeViewModule]
})
export class LayoutModule {}
