import { ReportService } from './report.service';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('ReportService', () => {
  let repService: ReportService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    repService = TestBed.get(ReportService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get dashboard report', () => {
    const dashboardInfo = {
      vacantPositions: 10,
      applications: 48,
      screenings: 28,
      evaluations: 20,
      passedCandidates: 12,
      offeredCandidates: 5,
      reserved: 7,
      accepted: 4
    };

    repService.loadDashboardReport().subscribe((data: any) => {
      expect(data).toBe(dashboardInfo);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(dashboardInfo);

    httpMock.verify();
  });
});
