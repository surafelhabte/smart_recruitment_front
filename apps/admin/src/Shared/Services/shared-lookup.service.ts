import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SharedLookupService {
  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

  load_data(query:any){
    return this.http.post(this.url + 'lookup/load_lookups', query)
  }
}
