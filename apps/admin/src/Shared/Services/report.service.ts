import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  private url = environment.baseUrl;
  public saved_report: any;

  constructor(private http?: HttpClient) {}

  loadDashboardReport() {
    return this.http.get(this.url + 'Dashboard/GetDashboardData');
  }

  intialize(data) {
    return this.http.post(this.url + 'Report/intialize', data);
  }

  send_request(data) {
    return this.http.post(this.url + 'Report/Process_Request/', data);
  }

  save_report(data) {
    return this.http.post(this.url + 'Report/Save_Report/', data);
  }

  update_report(data) {
    return this.http.post(this.url + 'Report/Update_Saved_Report/', data);
  }

  get_saved_reports() {
    return this.http.get(this.url + 'Report/Get_Saved_Reports');
  }

  delete_saved_report(id:any) {
    return this.http.delete(this.url + 'Report/Delete_Saved_Report/' + id);
  }
  
}
