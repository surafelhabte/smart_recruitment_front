import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import {
  DropDownListModule,
  MultiSelectModule
} from '@syncfusion/ej2-angular-dropdowns';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { NumericTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { TreeGridModule } from '@syncfusion/ej2-angular-treegrid';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { DetailRowService } from '@syncfusion/ej2-angular-grids';
import {
  TabModule,
  SidebarModule,
  TreeViewModule,
  MenuModule
} from '@syncfusion/ej2-angular-navigations';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { BreadCrumbModule } from '@smart-recruitment-frontend/bread-crumb';
import { FormOptionModule } from '@smart-recruitment-frontend/form-option';
import { TokenizeService } from '../../Core/Interceptor/tokenize.service';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';
import { Toast_Notification_Module } from 'libs/toast/src/lib/Toast_Notification.module';
import { AutoCompleteComponent } from './auto-complete/auto-complete.component';

@NgModule({
  declarations: [AutoCompleteComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DropDownListModule,
    TextBoxModule,
    ButtonModule,
    DatePickerModule,
    CheckBoxModule,
    NumericTextBoxModule,
    MultiSelectModule,
    TreeGridModule,
    GridModule,
    TabModule,
    CKEditorModule,
    BreadCrumbModule,
    FormOptionModule,
    ToastModule,
    Toast_Notification_Module,
    SidebarModule,
    TreeViewModule,
    MenuModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DropDownListModule,
    TextBoxModule,
    ButtonModule,
    DatePickerModule,
    CheckBoxModule,
    NumericTextBoxModule,
    MultiSelectModule,
    TreeGridModule,
    GridModule,
    TabModule,
    CKEditorModule,
    BreadCrumbModule,
    FormOptionModule,
    Toast_Notification_Module,
    SidebarModule,
    TreeViewModule,
    AutoCompleteComponent
  ],
  providers: [
    DetailRowService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenizeService, multi: true }
  ]
})
export class BasicModule {}
