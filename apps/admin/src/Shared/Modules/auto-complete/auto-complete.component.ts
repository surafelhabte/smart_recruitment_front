import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FilteringEventArgs } from '@syncfusion/ej2-angular-dropdowns';
import { EmitType } from '@syncfusion/ej2-base';
import { Query } from '@syncfusion/ej2-data';
import { JobRequirmentService } from 'apps/admin/src/featured/job-requirment/job-requirment.service';


@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.css']
})
export class AutoCompleteComponent implements OnInit {
  public positions: any = [];
  public fields: any = { text: 'position', value: 'pos_id' };
  @Output() OnPositionSelect: EventEmitter<any> = new EventEmitter();

  constructor(private service: JobRequirmentService) { }

  ngOnInit() {
  }

  public onFilteringGuarantors: EmitType<any> = (e: FilteringEventArgs) => {
    let query = new Query();
    query = e.text != '' ? query.where('full_name', 'contains', e.text, true) : query;
    this.service.get_position_info({ keyword: e.text }).subscribe((data: any) => {
      e.updateData(data);
    });
  }

   public onSelect: EmitType<any> = (value) => {
    this.OnPositionSelect.emit(value);
  }

}
