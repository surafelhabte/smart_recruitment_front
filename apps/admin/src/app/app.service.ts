import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   get_token() {
    return this.http.get(this.url + 'Tokenizer/Get_token')
   }
   
   get_identity() {
     if(localStorage.getItem('employee_id') !== null){
       return this.http.post(this.url + 'Tokenizer/Get_identity',{ id: localStorage.getItem('employee_id') })
     }
   }

}
 