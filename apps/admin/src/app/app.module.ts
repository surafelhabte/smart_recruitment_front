import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { LayoutModule } from '../Core/layout/layout.module';
import { HeaderComponent } from '../Core/layout/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { BreadCrumbModule } from '@smart-recruitment-frontend/bread-crumb';
import { FormOptionModule } from '@smart-recruitment-frontend/form-option';
import { AppService } from './app.service';
import { Toast_Notification_Module } from 'libs/toast/src/lib/Toast_Notification.module';
import { AuthGuardGuard } from '../Core/Security/auth-guard.guard';
import { SecurityService } from '../Core/Security/security-service';
import { UnAuthorizedPageComponent } from '../Shared/un-authorized-page/un-authorized-page.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent, UnAuthorizedPageComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),
    AppRoutingModule,
    LayoutModule,
    HttpClientModule,
    BreadCrumbModule,
    FormOptionModule,
    Toast_Notification_Module
  ],
  providers: [AppService, AuthGuardGuard, SecurityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
