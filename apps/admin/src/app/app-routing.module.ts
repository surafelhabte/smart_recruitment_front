import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnAuthorizedPageComponent } from '../Shared/un-authorized-page/un-authorized-page.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () =>
      import('../featured/dashboard/dashboard.module').then(
        m => m.DashboardModule
      ),
    data: {
      title: 'Dashboard',
      breadCrum: 'Dashboard'
    }
  },
  {
    path: 'lookup',
    loadChildren: '../featured/lookup/lookup.module#LookupModule',
    data: {
      title: 'Lookup',
      breadCrum: 'Lookup'
    }
  },
  {
    path: 'vacancy',
    loadChildren: '../featured/vacancy/vacancy.module#VacancyModule',
    data: {
      title: 'Vacancy',
      breadCrum: 'Vacancy'
    }
  },
  {
    path: 'schedule',
    loadChildren: '../featured/schedule/schedule.module#ScheduleModule',
    data: {
      title: 'Schedule',
      breadCrum: 'Schedule'
    }
  },
  {
    path: 'job/request',
    loadChildren:
      '../featured/recruitment-request/recruitment-request.module#RecruitmentRequestModule',
    data: {
      title: 'Job request',
      breadCrum: 'Job request'
    }
  },
  {
    path: 'application',
    loadChildren:
      '../featured/application/application.module#NgbdTableCompleteModule',
    data: {
      title: 'Application',
      breadCrum: 'Application'
    }
  },
  {
    path: 'preliminaryListedApplications',
    loadChildren:
      '../featured/secondary-application/secondary-application.module#NgbdTableCompleteModule',
    data: {
      title: 'Priliminary listed',
      breadCrum: 'Priliminary listed'
    }
  },
  {
    path: 'evaluation',
    loadChildren:
      '../featured/evaluation/evaluation.module#NgbdTableCompleteModule',
    data: {
      title: 'Evaluation',
      breadCrum: 'Evaluation'
    }
  },
  {
    path: 'offering',
    loadChildren: '../featured/offer/offer.module#NgbdTableCompleteModule',
    data: {
      title: 'Offering',
      breadCrum: 'Offering'
    }
  },
  {
    path: 'job-requirment',
    loadChildren: () =>
      import('../featured/job-requirment/job-requirment.module').then(
        m => m.JobRequirmentModule
      ),
    data: {
      title: 'Job requirment',
      breadCrum: 'Job requirment'
    }
  },
  {
    path: 'assessment-procedure',
    loadChildren: () =>
      import(
        '../featured/assessment-procedure/assessment-procedure.module'
      ).then(m => m.AssessmentProcedureModule),
    data: {
      title: 'Assessment procedure',
      breadCrum: 'Assessment procedure'
    }
  },
  {
    path: 'recruit',
    loadChildren: '../featured/recruit/Recruit.module#NgbdTableCompleteModule',
    data: {
      title: "Recruitment Approval",
      breadCrum: "Recruitment Approval"
    }
  },
  {
    path: 'panel',
    loadChildren: '../featured/panel-members/panel-members.module#PanelMembersModule',
    data: {
      title: "Assign Panel Members",
      breadCrum: "Assign Panel Members"
    }
  },
  {
    path: 'reports',
    loadChildren: '../featured/reports/reports.module#ReportsModule',
    data: {
      title: 'Reports',
      breadCrum: 'Reports'
    }
  },
  { path: "unauthorized", component: UnAuthorizedPageComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
