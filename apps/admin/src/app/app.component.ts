import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { AppService } from './app.service';
import { Router, NavigationEnd, Event } from '@angular/router';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';
import {
  AppUserAuth,
  SecurityService
} from '../Core/Security/security-service';

@Component({
  selector: 'smart-recruitment-frontend-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent;
  public securityObject: AppUserAuth;
  public roles: any;
  constructor(
    public service: AppService,
    private router: Router,
    public securityService: SecurityService
  ) {}

  ngOnInit(): void {
    this.securityObject = this.securityService.securityObject;
    this.roles = this.securityObject.roles.filter(elem => elem.status === 'true');
    
    this.service.get_token().subscribe((data: any) => {
      localStorage.setItem('token', data.token);
    });

    this.service.get_identity().subscribe((data: any) => {
      localStorage.setItem('identity', JSON.stringify(data));
    });

    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        if (history.state.data !== undefined) {
          this.notif.Show(history.state.data);
        }
      }
    });
  }
}
