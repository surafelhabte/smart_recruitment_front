import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BasicModule } from '../../Shared/Modules/basic.module';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';
import { ViewComponent } from './view/view.component';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';

const routes: Routes = [
  {
    path: '',
    component: ViewComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: "List", role: "manageEvaluation" }
  },
];

@NgModule({
  imports: [
    BasicModule,
    NgbModule,
    DataGridModule,
    DialogModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ViewComponent],
  exports: [RouterModule]
})
export class NgbdTableCompleteModule { }
