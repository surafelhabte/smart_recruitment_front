import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EvaluationService {

  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   load_applications(vacancy_Id: any) {
    return this.http.get(this.url + 'Evaluation/LoadApplications/' + vacancy_Id)
   }

   save_result(evaluation: any) {
    return this.http.post(this.url + 'Evaluation/SaveResult', evaluation)
   }

   get_vacancies() {
    return this.http.get(this.url + 'Evaluation/GetVacancies')
   }

   get_panel_members(data) {
    return this.http.post(this.url + 'Evaluation/GetPanelMembers',data)
   }
}
