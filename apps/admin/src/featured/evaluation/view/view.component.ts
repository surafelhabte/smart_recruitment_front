import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { FormControl, FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EvaluationService } from '../evaluation.service';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';

@Component({
   selector: 'app-evaluation',
   templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
  })
  
export class ViewComponent implements OnInit{
  @ViewChild('ejDialog', { static: false }) ejDialog: DialogComponent;
  @ViewChild('container', { read: ElementRef, static: false }) container: ElementRef;
  public targetElement: HTMLElement;
  public position = { X: 'center', Y: 'center' };

  public applications: any[] = [];
  public vacancies: any[] = [];
  public loading: boolean = false;
  
  public form: FormGroup;
  public formSubmitted = false;

  public vacancy_type : any;
  public vacancy_id : any;
  public procedure : any;

  public columens: any[] = [];

  public message: any = { show: false, content: '', cssStyle: '' };
  public fields: any = { value: 'value', text: 'text' };

  public toolbar = [{ text: 'Search', tooltipText: 'Search Items' }];
  public command = ['detail'];

  vacancy = new FormControl('');

  public profile_detail: any = null;
  public panel_members: any[]= [];

  constructor(public service: EvaluationService,private router?: Router, private fb?: FormBuilder,) {
    this.load_Vacancies();

    this.form = this.fb.group({
      Applications: this.fb.array([]),
      Result: this.fb.control('',Validators.required)
    });
  }

  ngOnInit(): void {
    this.columens.push({field: 'Photo',headerText: 'Photo',textAlign: 'center',width: 40, img: true });
    this.columens.push({field: 'Gender',headerText: 'Gender',textAlign: 'center',width: 35 });
    this.columens.push({field: 'FirstName',headerText: 'First Name',textAlign: 'center',width: 90 });
    this.columens.push({field: 'Surname',headerText: 'Last Name',textAlign: 'center',width: 90 });
  }

  load_Vacancies(){
    this.service.get_vacancies()
    .subscribe((data: any[]) => {
      const incomingData = data;
      if (incomingData.length > 0) {
        incomingData.forEach(element => {
          this.vacancies.push({value: element.value, text: element.text, date: element.date});
         });
      }
    });
  }

  load_Application(vacancy_id){
    this.applications = [];
    this.message.show = false;
    this.loading = true;

    this.service.load_applications(vacancy_id)
    .subscribe((data: any) => {
      if(data.status){
        this.applications = data.message.applications;
        this.procedure = data.message.procedure;

      } else {
        this.message = {show: true, content: data.message, cssStyle: 'danger'};
      }

      this.loading = false;
    });

  }

  get get_Applications() {
    return this.form.get('Applications') as FormArray;
  }

  public initilaizeTarget: EmitType<object> = () => {
    this.targetElement = this.container.nativeElement.parentElement;
  }
  
  public hideDialog: EmitType<object> = () => {
    this.ejDialog.hide();
    this.formSubmitted = false;
  }

  public buttons: object = [
    { click: this.Save_Result.bind(this),buttonModel: { content: 'Save', isPrimary: true} },
    { click: this.hideDialog.bind(this), buttonModel: { content: 'Cancel' } }
  ];

  getFormGroup(index): FormGroup {
    return this.get_Applications.controls[index] as FormGroup;
  }

  get score():FormControl{
    return this.form.get('Result') as FormControl;
  }

  ShowDetail(data){
    this.profile_detail = data;
    this.get_Applications.clear();
    this.score.reset();
    
    if(data.panel === "1"){
      this.service.get_panel_members({ vacancy_id: data.Vacancy_Id, step : data.Step })
       .subscribe((data: any) => {
        this.panel_members = data;
        if(data.length > 0){
          this.panel_members.forEach(element => {
            this.get_Applications.push(this.fb.group({
              panel_member_id : new FormControl(element.value, Validators.required),
              value : new FormControl('', Validators.required),
              application_id : new FormControl(this.applications[0].id)
            }));
          });
        }
      });
  
    }

    this.initilaizeTarget();
    this.ejDialog.show(); 

  }

  Save_Result(){
    if(confirm('Are you sure you want to save ?')){
      let data: any; 
      if(this.get_Applications.length > 0) {
        data = this.get_Applications.value;
        this.score.setValue('Yes');
      } else {
        data = this.score.value;
      }
      if(this.form.invalid){
        this.formSubmitted = true;
        return;

      } else {
        this.service.save_result({ 
          Vacancy_Id: this.profile_detail.Vacancy_Id,Vacancy_Type: this.profile_detail.Vacancy_Type,Data : data, 
          Step: this.profile_detail.Step,Gender: this.profile_detail.Gender, application_id: this.profile_detail.id
        })
        .subscribe((response: any) => {
          if(response.status){
            this.ngOnInit();
            this.ejDialog.hide();
            alert(response.message);
            
          } else {
            this.message = { show: true, content: response.message, cssStyle: 'danger' };
          } 

        });

      }
    }
  }

}
