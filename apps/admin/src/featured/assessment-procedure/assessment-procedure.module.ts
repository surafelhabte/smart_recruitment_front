import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssessmentProcedureListComponent } from './assessment-procedure-list/assessment-procedure-list.component';
import { CreateFormComponent } from './create-form/create-form.component';
import { Routes, RouterModule } from '@angular/router';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';
import { InternalAssessmentProcedureFormComponent } from './Internal/internal-assessment-procedure-form/internal-assessment-procedure-form.component';
import { SettingComponent } from './setting/setting.component';

const routes: Routes = [
  {
    path: 'create',
    component: CreateFormComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Create', role: "createAssessmentProcedure" }
  },
  {
    path: ':id/update',
    component: CreateFormComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Edit', role: "editAssessmentProcedure" }
  },
  {
    path: 'internal/create',
    component: InternalAssessmentProcedureFormComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Create', role: "createAssessmentProcedure" }
  },
  {
    path: 'internal/:id/update',
    component: InternalAssessmentProcedureFormComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Edit', role: "editAssessmentProcedure" }
  },
  {
    path: '',
    component: AssessmentProcedureListComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'List', role: "viewAssessmentProcedure" }
  },
  {
    path: 'setting',
    component: SettingComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Create', role: "createAssessmentProcedure" }
  }
];

@NgModule({
  declarations: [AssessmentProcedureListComponent, CreateFormComponent, 
    InternalAssessmentProcedureFormComponent, SettingComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    DataGridModule,
    BasicModule,
    NgbModule
  ],
  exports: [RouterModule]
})
export class AssessmentProcedureModule { }
