export class AssessmentProcedureModel {
    id?: number
    Job_Grade_Low: string
    Job_Grade_High: string
    Step: number
    Assessment: string
    Internal_Min_Point: number
    External_Min_Point: number
    Details: string
    Affirmative_Min_Point: number
    panel_members_required: boolean
}

export class AssessmentProcedure {
    AssessmentProcedure: AssessmentProcedureModel;
}
