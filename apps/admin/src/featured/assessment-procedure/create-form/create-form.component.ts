import { Component, OnInit } from '@angular/core';
import { AssessmentProcedureService } from '../assessment-procedure.service';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AssessmentProcedureModel} from '../assessment-procedure.interface';
import { Location } from '@angular/common';

@Component({
  selector: 'smart-recruitment-frontend-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.css']
})
export class CreateFormComponent implements OnInit {
  procedureForm: FormGroup;
  isUpdate = false;
  procedureId: number;
  lookupData = ['AssessmentType', 'VacancyType', 'JobGrade'];
  AssessmentTypes = [];
  VacancyTypes = [];
  grades = [];
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private service: AssessmentProcedureService,
    private location: Location,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.procedureId = +this.activatedRoute.snapshot.paramMap.get('id');
    if (this.procedureId) {
      this.isUpdate = true;
      this.service
        .get_assessmentProcedure(this.procedureId)
        .subscribe((data: any) => {
          this.initializeForm(data);
          
        });
    }
    this.service.load_data(this.lookupData).subscribe((data: any) => {
      this.AssessmentTypes = data.AssessmentType;
      this.VacancyTypes = data.VacancyType;
      this.grades = data.JobGrade;
    });
  }

  createForm(): void {
    this.procedureForm = this.formBuilder.group({
      job_Grade_Low: ['', Validators.required],
      job_Grade_High: ['', Validators.required],
      step: ['', Validators.required],
      assessmentType: ['', Validators.required],
      internalMinPoint: ['', Validators.required],
      externalMinPoint: ['', Validators.required],
      description: [''],
      affirmativeMinPoint: [''],
      panel_members_required: [false]
    });
  }

  initializeForm(payload: AssessmentProcedureModel) {
    this.procedureForm = this.formBuilder.group({
      id: [payload.id],
      job_Grade_Low: [payload[0].Job_Grade_Low, Validators.required],
      job_Grade_High: [payload[0].Job_Grade_High, Validators.required],
      step: [payload[0].Step, Validators.required],
      assessmentType: [payload[0].Assessment, Validators.required],
      internalMinPoint: [payload[0].Internal_Min_Point, Validators.required],
      externalMinPoint: [payload[0].External_Min_Point, Validators.required],
      description: [payload[0].Details],
      affirmativeMinPoint: [payload[0].Affirmative_Min_Point],
      panel_members_required: [payload[0].panel_members_required === "1" ? true: false]
    });
  }

  createGenderScore() {
    return this.formBuilder.group({
      gender: ['', Validators.required],
      score: ['', Validators.required]
    });
  }

  get job_Grade_Low(): FormControl {
    return this.procedureForm.get('job_Grade_Low') as FormControl;
  }

  get job_Grade_High(): FormControl {
    return this.procedureForm.get('job_Grade_High') as FormControl;
  }

  get panel_members_required(): FormControl {
    return this.procedureForm.get('panel_members_required') as FormControl;
  }

  get emaploymentType(): FormControl {
    return this.procedureForm.get('employmentType') as FormControl;
  }

  get step(): FormControl {
    return this.procedureForm.get('step') as FormControl;
  }

  get assessmentType(): FormControl {
    return this.procedureForm.get('assessmentType') as FormControl;
  }

  get employmentType(): FormControl {
    return this.procedureForm.get('employmentType') as FormControl;
  }

  get description(): FormControl {
    return this.procedureForm.get('description') as FormControl;
  }

  get internalMinPoint(): FormControl {
    return this.procedureForm.get('internalMinPoint') as FormControl;
  }

  get externalMinPoint(): FormControl {
    return this.procedureForm.get('externalMinPoint') as FormControl;
  }

  get affirmativeMinPoint(): FormControl {
    return this.procedureForm.get('affirmativeMinPoint') as FormControl;
  }

  prepareFormData(): AssessmentProcedureModel | null {
    if (this.procedureForm.valid) {
      if (this.procedureId) {
        return {
          id: this.procedureId,
          Job_Grade_Low: this.job_Grade_Low.value,
          Job_Grade_High: this.job_Grade_High.value,
          Step: this.step.value,
          Assessment: this.assessmentType.value,
          Internal_Min_Point: this.internalMinPoint.value,
          External_Min_Point: this.externalMinPoint.value,
          Details: this.description.value,
          Affirmative_Min_Point: this.affirmativeMinPoint.value,
          panel_members_required: this.panel_members_required.value
        };
      } else {
        return {
          Job_Grade_Low: this.job_Grade_Low.value,
          Job_Grade_High: this.job_Grade_High.value,
          Step: this.step.value,
          Assessment: this.assessmentType.value,
          Internal_Min_Point: this.internalMinPoint.value,
          External_Min_Point: this.externalMinPoint.value,
          Details: this.description.value,
          Affirmative_Min_Point: this.affirmativeMinPoint.value,
          panel_members_required: this.panel_members_required.value
        };
      }
    } else {
      return null;
    }
  }

  onSubmit() {
    const payload = this.prepareFormData();
    this.submitted = true;
    if (payload) {
      if (this.procedureId) {
        this.service.update(payload).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/assessment-procedure'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          } else {
            this.router.navigate(['/assessment-procedure'], {state: {data: {title: 'Error', content: response.message, cssClass:'e-toast-danger'}}});
          }
        });
      } else {
        this.service.create(payload).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/assessment-procedure'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          } else {
            this.router.navigate(['/assessment-procedure'], {state: {data: {title: 'Error', content: response.message, cssClass:'e-toast-danger'}}});
          }
        });
      }
    }
  }

  
}
