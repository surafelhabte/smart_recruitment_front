import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AssessmentProcedureService } from '../assessment-procedure.service';
import { GroupSettingsModel } from '@syncfusion/ej2-grids';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';

@Component({
  selector: 'smart-recruitment-frontend-assessment-procedure-list',
  templateUrl: './assessment-procedure-list.component.html',
  styleUrls: ['./assessment-procedure-list.component.css']
})
export class AssessmentProcedureListComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent

  public procedures: any[] = [];
  public procedures_internal: any[] = [];
  public columensToDisplay: any[] = [];
  public columens_internal: any[] = [];
  public searchField = 'Position';
  public groupOptions: GroupSettingsModel;
  public groupOption_internal: GroupSettingsModel;
  public groupBy = ['JobGrade'];
  public incomingToolbar = [
    { text: 'Add', tooltipText: 'Add Items' },
    { text: 'Filter', tooltipText: 'Filter data' },
    { text: 'Collapse', prefixIcon: 'e-collapse', tooltipText: 'Collapse all' },
    { text: 'Expand', prefixIcon: 'e-expand', tooltipText: 'Expand all' }
  ];
  public incomingCommand = ['edit', 'delete'];


  constructor(
    private router: Router,
    private assService: AssessmentProcedureService
  ) {
    this.columensToDisplay.push({
      field: 'JobGrade',
      headerText: 'Job grade',
      textAlign: 'left',
      width: 50
    });
    this.columensToDisplay.push({
      field: 'Step',
      headerText: 'Step',
      textAlign: 'left',
      width: 50
    });
    this.columensToDisplay.push({
      field: 'Assessment',
      headerText: 'Assessment Type',
      textAlign: 'left',
      width: 90
    });
    this.columensToDisplay.push({
      field: 'Internal_Min_Point',
      headerText: 'Internal min point',
      textAlign: 'left',
      width: 90
    });
    this.columensToDisplay.push({
      field: 'External_Min_Point',
      headerText: 'External min point',
      textAlign: 'left',
      width: 90
    });
    this.columensToDisplay.push({
      field: 'Affirmative_Min_Point',
      headerText: 'Affirmative min point',
      textAlign: 'left',
      width: 90
    });
    this.columensToDisplay.push({
      field: 'Details',
      headerText: 'Description',
      textAlign: 'left',
      width: 130
    });

    //for internal
    this.columens_internal.push({field: 'criteria',headerText: 'Procedure Criteria',textAlign: 'center',width: 130});
    this.columens_internal.push({field: 'text',headerText: 'Procedure Type',textAlign: 'center',width: 90});
    this.columens_internal.push({field: 'min_service_year',headerText: 'Minimim Service Year',textAlign: 'center',width: 90});
    this.columens_internal.push({field: 'max_service_year',headerText: 'Maximum Service Year',textAlign: 'center',width: 90});
    this.columens_internal.push({field: 'value',headerText: 'Minimum Score',textAlign: 'center',width: 90});


  }
  
  ngOnInit() {
    this.assService.get_assessmentProcedures().subscribe((data: any[]) => {
      this.procedures = data;
    });

    this.groupOptions = { disablePageWiseAggregates: false, showDropArea: true,showGroupedColumn: false,columns: ['JobGrade'] };
    this.groupOption_internal = { disablePageWiseAggregates: false, showDropArea: true,showGroupedColumn: false,columns: ['criteria'] };

    this.assService.get_internal_procedures().subscribe((data: any[]) => {
      this.procedures_internal = data;
    });
    
  }

  add() {
    this.router.navigate(['assessment-procedure/create']);
  }

  edit($event) {
    this.router.navigate(['assessment-procedure/' + $event.id + '/update']);
  }

  delete(item: any) {
    this.assService.delete(item.id).subscribe((response: any) => {
      if (response.status === true) {
        this.notif.Show({ title: 'Success', content: response.message, cssClass: 'e-toast-success' })
        this.ngOnInit();
      } else {
        this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' })
      }
    });
  }

  add_internal() {
    this.router.navigate(['assessment-procedure/internal/create']);
  }

  edit_internal($event) {
    this.router.navigate(['assessment-procedure/internal/' + $event.id + '/update']);
  }

  delete_internal(item: any) {
    this.assService.delete_internal(item.id).subscribe((response: any) => {
      if (response.status === true) {
        this.notif.Show({ title: 'Success', content: response.message, cssClass: 'e-toast-success' })
        this.ngOnInit();
      } else {
        this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' })
      }
    });
  }
}
