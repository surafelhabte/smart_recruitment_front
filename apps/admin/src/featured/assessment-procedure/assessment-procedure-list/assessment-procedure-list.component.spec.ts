import { AssessmentProcedureListComponent } from "./assessment-procedure-list.component";
import { AssessmentProcedureService } from '../assessment-procedure.service';
import { ComponentFixture, TestModuleMetadata, TestBed, async } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { SecurityService } from 'apps/admin/src/Core/Security/security-service';

const crypto = require('crypto');
Object.defineProperty(global, 'crypto', {
  value: {
    getRandomValues: arr => crypto.randomBytes(arr.length)
  }
});
describe('AssessmentProcedureListComponent', () => {
  let component: AssessmentProcedureListComponent;
  let fixture: ComponentFixture<AssessmentProcedureListComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        RouterTestingModule, DataGridModule, HttpClientTestingModule,BasicModule
      ],
      declarations: [AssessmentProcedureListComponent],
      providers: [AssessmentProcedureService, SecurityService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(AssessmentProcedureListComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

