import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AssessmentProcedureService } from '../../assessment-procedure.service';


@Component({
  selector: 'smart-recruitment-frontend-internal-assessment-procedure-form',
  templateUrl: './internal-assessment-procedure-form.component.html',
  styleUrls: ['./internal-assessment-procedure-form.component.css']
})
export class InternalAssessmentProcedureFormComponent implements OnInit {
  public form: FormGroup;
  public formSubmitted = false;
  public isUpdate = false;
  public id: any;
  public fields: any = {text: 'text', value: 'value'};
  public criteria: any [];
  public type: any [];
  
  constructor(private fb: FormBuilder,private act: ActivatedRoute,private service: AssessmentProcedureService,private router: Router) {
    this.form = this.fb.group({
      id: this.fb.control(''),
      criteria: this.fb.control('',Validators.required),
      type: this.fb.control(''),
      text: this.fb.control(''),
      min_service_year: this.fb.control(''),
      max_service_year: this.fb.control(''),
      value: this.fb.control('',Validators.required),
    });
  }

  ngOnInit() {
    this.id = this.act.snapshot.paramMap.get('id');
    if (this.id) {
      this.isUpdate = true;
      this.service.get_internal_procedure(this.id)
      .subscribe((data: any) => {
        this.form.setValue(data);
      });
    }

    this.criteria = [ 
      { text: 'Service Year', value: 'Year'},
      { text: 'File Clearance', value: 'File'},
      { text: 'Performance Appraisal Result', value: 'APA'}
    ];

  }

  public get_controls(name: string): FormControl {
    return this.form.get(name) as FormControl;
  }

  public set_validation(name: string) {
    if(name === 'type'){
      this.form.get('min_service_year').clearValidators();
      this.form.get('min_service_year').updateValueAndValidity();
      this.form.get('max_service_year').clearValidators();
      this.form.get('max_service_year').updateValueAndValidity();

    } else {
      this.form.get('type').clearValidators();
      this.form.get('type').updateValueAndValidity();

    } 

    this.form.get(name).setValidators(Validators.required);
    this.form.get(name).updateValueAndValidity();

  }

  onSubmit() {
    if (this.form.invalid) {
      this.formSubmitted = true;
      return;

    } else {
      if (this.id) {
        this.service.update_internal(this.form.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/assessment-procedure'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.router.navigate(['/assessment-procedure'], {state: {data: {title: 'Error', content: response.message, cssClass:'e-toast-danger'}}});
          }
        });

      } else {
        this.service.create_internal(this.form.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/assessment-procedure'], { state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.router.navigate(['/assessment-procedure'], {state: {data: {title: 'Error', content: response.message, cssClass:'e-toast-danger'}}});
          }
        });
      }
    }  

  }

  onCriteriaChange(value: string){
    switch(value){
      case 'Year':
        this.set_validation('min_service_year');
        this.set_validation('max_service_year');
        this.type = [];
      break;

      case 'File':
        this.set_validation('type');
        this.type = [
          { text: 'No Active Discipline', value: 'NAD'},
          { text: 'Active Verbal Warning', value: 'AVW'},
          { text: 'Active Written Warning', value: 'AWW'},
          { text: 'Demotion', value: 'DEMO'}
        ];
      break;

      case 'APA':
        this.set_validation('type');
        this.type = [
          { text: 'Exceptional', value: 'Exe/CE'},
          { text: 'FE', value: 'FE'},
          { text: 'FM/FS', value: 'FM/FS'},
          { text: 'PS/PM', value: 'PS/PM'},
          { text: 'DM/UNP', value: 'DM/UNP'}
        ];
      break;
      
    }
  }

}
