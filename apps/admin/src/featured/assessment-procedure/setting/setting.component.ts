import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AssessmentProcedureService } from '../assessment-procedure.service';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent

  public form: FormGroup;
  public formSubmitted = false;
  public isUpdate = false;
  public id: any;
  
  constructor(private fb: FormBuilder,private act: ActivatedRoute,
    private service: AssessmentProcedureService,private router: Router) {
    this.form = this.fb.group({
      id: this.fb.control(null),
      min_year_in_ecx: this.fb.control('',Validators.required),
      min_year_on_position: this.fb.control('',Validators.required),
      min_year_promotion: this.fb.control('',Validators.required),
      min_pass_point: this.fb.control('',Validators.required),
    });
  }

  ngOnInit() {
    this.service.get_setting()
    .subscribe((data: any) => { 
      if(data != null){
        this.isUpdate = true;
        this.form.setValue(data); 
      }
    });
  }

  public get_controls(name: string): FormControl {
    return this.form.get(name) as FormControl;
  }

  onSubmit() {
    if (this.form.invalid) {
      this.formSubmitted = true;
      return;

    } else {
      this.service.save_setting(this.form.value).subscribe((response: any) => {
        if(response.status){
          this.notif.Show({ title: 'Success', content: response.message, cssClass:'e-toast-success' });
        
        } else {
          this.notif.Show({ title: 'Error', content: response.message, cssClass:'e-toast-danger' });
        }
      });
    }  

  }

}
