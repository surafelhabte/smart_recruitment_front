import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AssessmentProcedureService {
  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   create(payload: any): any {
    return this.http.post(this.url + 'AssessmentProcedure/create', payload)
   }

   get_assessmentProcedures() {
    return this.http.get(this.url + 'AssessmentProcedure/GetProcedures')
   }

   get_assessmentProcedure(id: any) {
    return this.http.get(this.url + 'AssessmentProcedure/GetProcedure/' + id)
   }

   update(job: any) {
    return this.http.post(this.url + 'AssessmentProcedure/Update', job)
   }

  delete(id: any) {
    return this.http.delete(this.url + 'AssessmentProcedure/delete/' + id)
  }

   create_internal(data: any): any {
    return this.http.post(this.url + 'AssessmentProcedure/createInternal', data)
   }

   get_internal_procedures() {
    return this.http.get(this.url + 'AssessmentProcedure/GetsInternal')
   }

   get_internal_procedure(id: any) {
    return this.http.get(this.url + 'AssessmentProcedure/GetInternal/' + id)
   }

   update_internal(job: any) {
    return this.http.post(this.url + 'AssessmentProcedure/UpdateInternal', job)
   }

  delete_internal(id: any) {
    return this.http.delete(this.url + 'AssessmentProcedure/deleteInternal/' + id)
  }

  load_data(query:any){
    return this.http.post(this.url + 'lookup/load_lookups', query)
  }

  get_setting() {
    return this.http.get(this.url + 'AssessmentProcedure/Setting')
  }

  save_setting(data: any) {
    return this.http.post(this.url + 'AssessmentProcedure/Setting',data)
  }

  





}
