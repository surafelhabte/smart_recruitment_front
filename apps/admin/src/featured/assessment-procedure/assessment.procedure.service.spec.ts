import { AssessmentProcedureService } from "./assessment-procedure.service";
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AssessmentProcedureModel } from './assessment-procedure.interface';

describe('AssessmentProcedureService', () => {
  let reqService: AssessmentProcedureService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
    });

    reqService = TestBed.get(AssessmentProcedureService);
    httpMock = TestBed.get(HttpTestingController);
  });

    it('Should create job requirment', () => {
    const requirment: AssessmentProcedureModel = {
    id: 2,
    JobGrade: "dept",
    Step: 1,
    Assessment: "string",
    Internal_Min_Point: 40,
    External_Min_Point: 60,
    Details: "sdasdas",
    Affirmative_Min_Point: 10,
    };
    reqService.create(requirment).subscribe((data: any) => {
      expect(data.JobGrade).toBe("job grade");
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(requirment);
    httpMock.verify();
  });

    it('Should get and list assessment procedures', () => {
     const requirment: AssessmentProcedureModel = {
    id: 2,
    JobGrade: "dept",
    Step: 1,
    Assessment: "string",
    Internal_Min_Point: 40,
    External_Min_Point: 60,
    Details: "sdasdas",
    Affirmative_Min_Point: 10,
    };

    reqService.get_assessmentProcedures().subscribe((data: any) => {
      expect(data).toBe(requirment);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(requirment);

    httpMock.verify();
  });

    it('Should update assessment procedure', () => {
       const requirment: AssessmentProcedureModel = {
    id: 2,
    JobGrade: "dept",
    Step: 1,
    Assessment: "string",
    Internal_Min_Point: 40,
    External_Min_Point: 60,
    Details: "details",
    Affirmative_Min_Point: 10,
    };

    reqService.update(requirment.id).subscribe((data: any) => {
      expect(data.Details).toBe("details");
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(requirment);

    httpMock.verify();
  });

    it('Should delete job requirment', () => {
    reqService.delete(12).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('DELETE');

    req.flush(12);

    httpMock.verify();
  });
});
