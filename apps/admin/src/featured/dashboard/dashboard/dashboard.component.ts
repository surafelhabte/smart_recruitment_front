import { Component, OnInit } from '@angular/core';
import { ReportService } from 'apps/admin/src/Shared/Services/report.service';

@Component({
  selector: 'smart-recruitment-frontend-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(private apiService: ReportService) {}

  dashboardItems: {
    name: string;
    class: string;
    icon: string;
    value: number;
  }[] = [
    {
      name: 'Vacant position',
      class: 'vacant',
      icon: '',
      value: 0
    },
    {
      name: 'Applications',
      class: 'applications',
      icon: '',
      value: 0
    },
    {
      name: 'Screening',
      class: 'screening',
      icon: '',
      value: 0
    },
    {
      name: 'Evaluation',
      class: 'evaluation',
      icon: '',
      value: 0
    },
    {
      name: 'Passed candidates',
      class: 'passed',
      icon: '',
      value: 0
    },
    {
      name: 'Offered candidates',
      class: 'offered',
      icon: '',
      value: 0
    },
    {
      name: 'Reserved candidates',
      class: 'reserved',
      icon: '',
      value: 0
    },
    {
      name: 'Accepted offers',
      class: 'accepted',
      icon: '',
      value: 0
    }
  ];

  ngOnInit() {
    this.apiService.loadDashboardReport().subscribe((data: any) => {
      this.dashboardItems[0].value = data.vacantPositions;
      this.dashboardItems[1].value = data.applications;
      this.dashboardItems[2].value = data.screenings;
      this.dashboardItems[3].value = data.evaluations;
      this.dashboardItems[4].value = data.passedCandidates;
      this.dashboardItems[5].value = data.offeredCandidates;
      this.dashboardItems[6].value = data.reserved;
      this.dashboardItems[7].value = data.accepted;
      localStorage.setItem('Recruitment_Request', data.Recruitment_Request);
    });
  }
}
