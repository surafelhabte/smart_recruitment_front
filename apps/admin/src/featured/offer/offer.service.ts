import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   load_applications(vacancy_Id: any) {
    return this.http.get(this.url + 'Offering/ShowResult/' + vacancy_Id)
   }

   show_detail(Application_Id: any) {
    return this.http.get(this.url + 'Offering/ShowDetail/' + Application_Id)
   }

   send_letter(data: any) {
    return this.http.post(this.url + 'Offering/SendLetter/', data)
   }

   get_vacancies() {
    return this.http.get(this.url + 'Offering/GetVacancies')
   }
}
