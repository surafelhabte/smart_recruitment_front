import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ViewComponent } from './view/view.component';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';

const routes: Routes = [
  {
    path: '',
    component: ViewComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: "View", role: "manageOffering" }
  },
];

@NgModule({
  imports: [
    BasicModule,
    NgbModule,
    DataGridModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ViewComponent, DetailsComponent],
  exports: [RouterModule],
  entryComponents: [DetailsComponent]
})
export class NgbdTableCompleteModule { }
