import { Component, OnInit, Input } from '@angular/core';
import { Router} from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { OfferService } from '../offer.service';

@Component({
  selector: 'smart-recruitment-frontend-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  @Input() id;
  public detail: any;
  
  constructor(public service: OfferService, private router?: Router, public activeModal?: NgbActiveModal) {}

  ngOnInit(): void {
    if (this.id !== undefined) {
    this.service.show_detail(this.id)
    .subscribe((data: any) => {
      if(data){
        this.detail = data;
      }
    });
   }
  }
}
