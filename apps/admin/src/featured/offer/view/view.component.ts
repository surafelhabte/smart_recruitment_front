import {Component} from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OfferService } from '../offer.service';
import { DetailsComponent } from '../details/details.component';
import { Router } from '@angular/router';

@Component({
   selector: 'app-offer',
   templateUrl: './view.component.html',
   styleUrls: ['./view.component.css'],
  })
  
export class ViewComponent {
  public applications: any[] = [];
  public vacancies: any[] = [];
  
  public offer_Spinner: any = false;
  public loading: boolean = false;

  public selected_applications: any[] = [];
  public columens: any[] = [];

  public message: any = { show: false, content: '', cssStyle: '' };
  public fields: any = { value: 'value', text: 'text' };
  
  public toolbar = [{ text: 'Search', tooltipText: 'Search Items' }];
  public command = ['detail'];

  vacancy = new FormControl(''); 
 
  constructor(public service: OfferService,private router: Router, private modalService?: NgbModal) {
    this.load_Vacancies();

    this.columens.push({field: 'Photo',headerText: 'Photo',textAlign: 'center',width: 40, img: true });
    this.columens.push({field: 'FirstName',headerText: 'First Name',textAlign: 'center',width: 70 });
    this.columens.push({field: 'Surname',headerText: 'Last Name',textAlign: 'center',width: 70 });
    this.columens.push({field: 'Gender',headerText: 'Gender',textAlign: 'center',width: 35 });
    this.columens.push({field: 'Result',headerText: 'Accumulative',textAlign: 'center',width: 50 });
    this.columens.push({field: 'Hiring_Status',headerText: 'Hiring Status',textAlign: 'center',width: 50 });
  }

  load_Vacancies(){
    this.service.get_vacancies()
    .subscribe((data: any[]) => {
      if (data.length > 0) {
        data.forEach(element => {
          this.vacancies.push({value: element.value, text: element.text, date: element.date});
        });
      }
    });
  }

  load_Application(vacancy_id){
    this.selected_applications = [];
    this.applications = [];
    this.loading = true;

    this.service.load_applications(vacancy_id)
    .subscribe((data: any) => {
      if(data.status){
        this.applications = data.message.applications;
        this.message = { show: false, content: '', cssStyle: '' };

      } else {
        this.applications = [];
        this.message = { show: true, content: data.message, cssStyle: 'danger' };

      }
      this.loading = false;
    });
  }

  select_application(data) {
    data instanceof Array ? data : data = [data];
    data.forEach(element => {
      const z: number = this.selected_applications.findIndex(obj=> obj.id === element.Application_Id && obj.Email === element.Email);
      if(z > -1){
        this.selected_applications.splice(z, 1);
  
      } else {
        this.selected_applications.push({id : element.Application_Id, Email:element.Email});
      }
      
    });
  }

  Send_Letter(type,status,message, hiring_status){
    if(confirm('are you sure you want to Send ' + message + ' Letter For the following application ?')){
      const applicants:any [] = [];
      this.selected_applications.forEach((element) => {
        applicants.push({id: element.id, Email: element.Email, Status:status, Hiring_Status: hiring_status});
      });
    
      this.service.send_letter({ type: type,vacancy_Id: this.applications[0].Vacancy_Id,applicant: applicants })
      .subscribe((response: any) => {
        if (response.status) {
          this.router.navigate([''], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});

        } else {
          this.message = { show: true, content: response.message, cssStyle: 'danger' };

        }
      });
    }
  }

  ShowDetail(data){
    const modalRef = this.modalService.open(DetailsComponent, { size: 'sm' });
    modalRef.componentInstance.id = data.Application_Id;
  }

}
 