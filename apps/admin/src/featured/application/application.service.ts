import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   get_applications_first(vacancyId : any) {
    return this.http.get(this.url + 'Application/GetApplicationsFirst/' + vacancyId)
   }

   get_applications_second(data : any) {
    return this.http.post(this.url + 'Application/GetApplicationsSecond/', data)
   }

   get_application(id: any) {
    return this.http.get(this.url + 'Application/GetApplication/' + id)
   }

   update(application: any) {
    return this.http.post(this.url + 'Application/Update', application)
   }

   filter_applications(data:any) {
    return this.http.post(this.url + 'Application/Get_Shortlisted_Applicant/', data)
   }

   get_vacancies() {
    return this.http.get(this.url + 'Application/GetVacancies')
   }

   get_comments(id) {
    return this.http.get(this.url + 'Application/GetComments/' + id)
   }

   post_comment(data) {
    return this.http.post(this.url + 'Application/PostComment/', data)
   }

}
 