import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';
import { ApplicationService } from '../application.service';
import { EmitType } from '@syncfusion/ej2-base';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';

@Component({
  selector: 'smart-recruitment-frontend-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent;
  @ViewChild('ejDialog', { static: true }) ejDialog: DialogComponent;
  @ViewChild('container', { read: ElementRef, static: true }) container: ElementRef;
  public targetElement: HTMLElement;

  public position = { X: 'center', Y: 'Top' };
  public animationSettings: Object = { effect: 'FadeZoom' , duration : 400};

  public BioData: any;
  public Qualifications: any;
  public Experiences: any;
  public References: any;
  public Trainings: any;

  @Input() id;
  @Input() is_dep_head;
  @Input() current_Step;
  @Input() vacancy;

  public form: FormGroup;
  public formSubmitted: boolean = false;
  public comments: any[];
  public columen: any = 'First';
  public is_internal: boolean = false;

  constructor(public service: ApplicationService, private router?: Router, private fb?: FormBuilder) {
    this.form = this.fb.group({ Comment: this.fb.control('',Validators.required) });
   
  }

  ngOnInit(): void {
    this.is_dep_head === "1" ? this.columen = "Second" : '';

      this.service.get_application(this.id)
        .subscribe((data: any) => {
          if (data) {
            this.BioData = data.bio_data;
            this.Qualifications = data.Qualifications;
            this.Experiences = data.Experiences;

            if(data.vacancy !== 'Internal'){
              this.References = data.References;
              this.Trainings = data.Trainings;

            } else {
              this.is_internal = true;

            }
          }
        });
        
        this.ejDialog.show(); 
        this.load_comments();

  }

  public initilaizeTarget: EmitType<object> = () => {
    this.targetElement = this.container.nativeElement.parentElement;
  }
  
  public hideDialog: EmitType<object> = () => {
    this.ejDialog.hide();
  }

  public buttons: object = [
    { click: this.takeAction.bind(this,'Set as Un-fit For','UnFit'),buttonModel: { content: 'Un-Fit',cssClass: 'e-danger' }},
    { click: this.takeAction.bind(this,'Set as Fit For','Fit'),buttonModel: { content: 'Fit',cssClass: 'e-success', isPrimary: true }}
  ];

  get comment():FormControl{
    return this.form.get('Comment') as FormControl;
  }

  takeAction(warning, action):any {
    if (confirm('are you sure you want to ' + warning + ' the following application ?')) {
      this.service.update([{ id: this.id, [this.columen]: action }])
      .subscribe((response: any) => {
        if (response.status) {
          this.router.navigate(['','application'], { state: { data: { title: 'Success', content: response.message, cssClass: 'e-toast-success' } } });
        
        } else {
          this.router.navigate(['','application'], { state: { data: { title: 'Success', content: response.message, cssClass: 'e-toast-danger' } } });
        }
      });           
    }
  }

  printApplication() {
  
  }

  post_comment(){
    if(this.form.invalid){
      this.formSubmitted = true;
      return;

    } else {
      this.formSubmitted = false;
      this.service.post_comment({ application_id: this.id, body: this.comment.value, comment_by: localStorage.getItem('employee_id') })
      .subscribe((response: any) => {
        if(response.status){
          alert(response.message);
        }
      });

    }


  }

  load_comments(){
    this.service.get_comments(this.id)
    .subscribe((data: any) => { this.comments = data; });
  }


}
