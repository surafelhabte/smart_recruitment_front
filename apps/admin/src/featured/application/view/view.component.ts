import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DetailsComponent } from '../details/details.component';
import { ApplicationService } from '../application.service';

@Component({
  selector: 'app-application',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
})

export class ViewComponent implements OnInit{
  public applications: any[] = [];
  public vacancies: any[] = [];
  public current_Step: any;

  public selected_applicants: any[] = [];
  public columens: any[] = [];

  public message: any = { show: false, content: '', cssStyle: '' };
  public fields: any = { value: 'value', text: 'text' };
  public col: any = 'First';
  public action: any[];

  public toolbar = [
    { text: 'Search', tooltipText: 'Search Items' }
  ];
  public command = ['detail'];
  public loading:boolean = false;

  public identity = JSON.parse(localStorage.getItem('identity'));

  public selected_action: any;
  public Submitted: boolean = false;

  public form: FormGroup;

  constructor(public service: ApplicationService, private modalService?: NgbModal) {
    this.load_Vacancies();

    this.form = new FormGroup({
      'vacancy': new FormControl('')
    });

    this.columens.push({field: 'Photo',headerText: 'Photo',textAlign: 'center',width: 25,img: true});
    this.columens.push({field: 'Gender',headerText: 'Gender',textAlign: 'center',width: 25 });
    this.columens.push({field: 'FirstName',headerText: 'First Name',textAlign: 'center',width: 60 });
    this.columens.push({field: 'Surname',headerText: 'Last Name',textAlign: 'center',width: 60 });
    this.columens.push({field: 'Date',headerText: 'Date',textAlign: 'center',width: 30 });
    this.columens.push({field: 'First',headerText: 'First',textAlign: 'center',width: 25 });
    this.columens.push({field: 'Second',headerText: 'Second',textAlign: 'center',width: 27 });
    this.columens.push({field: 'Shortlist',headerText: 'Short-listed',textAlign: 'center',width: 40 });
  }
  
  ngOnInit() {
    this.identity.is_department_head === "1" ? this.col = "Second" : '';
    this.action = [
      { value: JSON.stringify({'value':'Fit','warning':'Set Fit For this Vacancy', 'columen': this.col }), text: 'Fit' },
      { value: JSON.stringify({'value':'UnFit','warning':'Set UnFit For this Vacancy', 'columen': this.col}), text: 'Un-Fit'},
      { value: JSON.stringify({'value':'Passed','warning':'Pass this application to Next Assessment','status':null}), text: 'Pass Application' },
      { value: JSON.stringify({'value':'Failed','warning':'Drop this application from Next Assessment','status':'Failed'}), text: 'Drop Application' }
    ];
  }

  ShowDetail(data) {
    const modalRef = this.modalService.open(DetailsComponent, { size: 'xl' });
    modalRef.componentInstance.id = data.id;
    modalRef.componentInstance.is_dep_head = this.identity.is_department_head;
    modalRef.componentInstance.current_Step = this.current_Step;
    modalRef.componentInstance.vacancy = this.form.get('vacancy').value;
  }

  load_Vacancies() {
    this.service.get_vacancies()
    .subscribe((data: any[]) => {
      if (data.length > 0) {
        data.forEach(element => {
          this.vacancies.push({ value: element.value, text: element.text, date: element.date });
        });
      }
    });
  }

  load_Application(value) {
    this.selected_applicants = [];
    this.applications = [];
    this.loading = true;
    if(this.identity.is_department_head === "1"){
      this.service.get_applications_second({ vac_id:value, dep_id:this.identity.dep_id })
      .subscribe((data: any[]) => {
        if (data.length) {
          this.applications = data;
          this.current_Step = data[0].Shortlist;
        }
        this.loading = false;
      });
    } else {
      this.service.get_applications_first(value)
      .subscribe((data: any[]) => {
        if (data.length) {
          this.applications = data;
          this.current_Step = data[0].Shortlist;
        }
        this.loading = false;
      });
    }
  }

  select_Applicant(data) {
    data instanceof Array ? data : data = [data];
    data.forEach(element => {
      const z: number = this.selected_applicants.indexOf(element.id);
      if(z > -1){
        this.selected_applicants.splice(z, 1);
  
      } else {
        this.selected_applicants.push(element.id);
      }
      
    });
  }

  takeAction() {
    if (confirm('are you sure you want to ' + this.selected_action.warning + ' the following application ?')) {
      this.Submitted = true;
      let applications = [];
      this.selected_applicants.forEach((element) => {
        switch(this.selected_action.value){
          case 'Fit':
          case 'UnFit':
            applications.push({ id: element, [this.col]: this.selected_action.value });
          break;

          case 'Passed':
          case 'Failed':
          applications.push({ id: element, 'Shortlist': 'Completed','Status': this.selected_action.status });
          break;
          
        }
      });

      this.service.update(applications)
        .subscribe((response: any) => {
          if (response.status) {
            this.message = { show: true, content: response.message, cssStyle: 'success' };
            this.selected_applicants = [];
            this.load_Application(this.form.get('vacancy').value);
          } else {
            this.message = { show: true, content: response.message, cssStyle: 'danger' };
          }

        this.Submitted = false;
      });
    }
  }

  select_action(value){
    this.selected_action = JSON.parse(value);
  }

}
