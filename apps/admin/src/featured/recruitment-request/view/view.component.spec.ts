import { TestBed  } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ViewComponent } from './view.component';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { RecruitmentRequestService } from '../recruitment-request.service';


describe('View Recruitment Request component', () => {
    let Service: RecruitmentRequestService;
    let page: ViewComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule, BasicModule],
        providers: [RecruitmentRequestService]
      });
      Service = TestBed.get(RecruitmentRequestService);
      page = new ViewComponent(Service);
  });

    it(' : Should load all Requests', () => {
      Service.get_requests().subscribe((data: any) => {
        expect(page.Requests.length).toBeGreaterThanOrEqual(0);
});

  });

});
