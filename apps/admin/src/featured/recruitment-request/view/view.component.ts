import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { RecruitmentRequestService } from '../recruitment-request.service';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';

@Component({
  selector: 'smart-recruitment-frontend-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent
  public Requests: any[] = [];
  public columensToDisplay: any[] = [];
  public searchField = 'Title';
  public incomingToolbar = [
    { text: 'Add', tooltipText: 'Add Items' },
    { text: 'Filter', tooltipText: 'Filter data' },
    { text: 'Search', tooltipText: 'Search Items' }
  ];
  public incomingCommand = ['edit', 'delete'];

  public System_Message: any = { show: false, content: '', cssStyle: '' };
  constructor(
    private Service: RecruitmentRequestService,
    private router?: Router
  ) {
    this.columensToDisplay.push({
      field: 'Division',
      headerText: 'Division',
      textAlign: 'left',
      width: 130
    });
    this.columensToDisplay.push({
      field: 'Position',
      headerText: 'Position',
      textAlign: 'left',
      width: 130
    });
    this.columensToDisplay.push({
      field: 'TypeOfEmployment',
      headerText: 'Type Of Employment',
      textAlign: 'left',
      width: 100
    });
    this.columensToDisplay.push({
      field: 'RequestedBy',
      headerText: 'Request by',
      textAlign: 'left',
      width: 80
    });
    this.columensToDisplay.push({
      field: 'Status',
      headerText: 'Status',
      textAlign: 'left',
      width: 80
    });
    this.columensToDisplay.push({
      field: 'created_date',
      headerText: 'Requested on',
      textAlign: 'left',
      width: 60
    });
  }

  ngOnInit() {
    this.Service.get_requests().subscribe((data: any[]) => {
      this.Requests = data;
    });
  }

  edit($event) {
    this.router.navigate(['job/request/details/' + $event.id]);
  }

  delete(item: any) {
    this.Service.delete(item.id).subscribe((response: any) => {
      if (response.status) {
        this.notif.Show({ title: 'Success', content: response.message, cssClass: 'e-toast-success' })
        this.ngOnInit();
      } else {
        this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' })
      }
    });
  }

  addRequest() {
    this.router.navigate(['job/request/create']);
  }
}
