import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecruitmentRequestService {

  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   create(request: any) {
    return this.http.post(this.url + 'Recruitment/Create', request)
   }

   get_requests() {
    return this.http.get(this.url + 'Recruitment/GetRequests')
   }

   get_request(id: any) {
    return this.http.get(this.url + 'Recruitment/GetRequest/' + id)
   }

   update(request: any) {
    return this.http.post(this.url + 'Recruitment/Update', request)
   }

  delete(id: any) {
    return this.http.delete(this.url + 'Recruitment/DeleteRequest/' + id)
  }

  loadLookupData(data) {
    return this.http.post(
      this.url + '/Lookup/load_lookups', data
    );
  }

}
