import { TestBed  } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { RecruitmentRequestService } from '../recruitment-request.service';
import { DetailsComponent } from './details.component';
import { Router } from '@angular/router';


describe('Detail View Recruitment Request component', () => {
    let Service: RecruitmentRequestService;
    let page: DetailsComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule, BasicModule],
        providers: [RecruitmentRequestService]
      });
      Service = TestBed.get(RecruitmentRequestService);
      page = new DetailsComponent(Service);
  });

  it(' : Should load Details of Request', () => {
      Service.get_request(1).subscribe((data: any) => {
        expect(page.request.length).toBeGreaterThanOrEqual(0);
});

  });


it('Should update request successfull and redirect to request list', () => {
  const request = { id: '1', Status : 'Approved'};
  const router = TestBed.get(Router);
  const router_spy = spyOn(router, 'navigateByUrl');

  Service.update(request).subscribe((response: any) => {
    if(response.status == true){
        expect(router_spy).toHaveBeenCalledWith(['job/request/view']);
    } 
  });

});

});
