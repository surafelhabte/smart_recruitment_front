import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecruitmentRequestService } from '../recruitment-request.service';

@Component({
  selector: 'smart-recruitment-frontend-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  public currentEditingID: any;
  public request: any;
  public Status: any;
  public disableButtons: any = false;

  constructor(private Service: RecruitmentRequestService, private router?: Router, private actRoute?: ActivatedRoute) {}

  onSubmit(): any {
    return this.Service.update(
    {
      id: this.currentEditingID,
      status: this.Status
    })
      .subscribe((response: any) => {
        this.currentEditingID = '';
        this.router.navigate(['/job/request/view'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
      });
  }

  ngOnInit() {
    const id = this.actRoute.snapshot.params.id;
    if (id !== undefined) {
      this.Service.get_request(id).subscribe((data: any) => {
        if (data) {
          this.request = data;
          this.currentEditingID = data.id;
          this.Status = data.status;
          data.TypeOfEmployment === 'Replacement' ? this.disableButtons = true : this.disableButtons = false; 
        }
      });
    }
  }

  approve() {
    const conf = confirm('Are you sure you want to Approve This Request  ?');
    if (conf) {
      this.Status = 'Approved';
      this.onSubmit();
    }
  }

  reject() {
    const conf = confirm('Are you sure you want to Reject This Request ?');
    if (conf) {
      this.Status = 'Rejected';
      this.onSubmit();
    }
  }

}
