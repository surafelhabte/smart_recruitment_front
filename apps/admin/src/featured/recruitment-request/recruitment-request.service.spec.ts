import { RecruitmentRequestService } from "./recruitment-request.service";
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Requestmodel } from './request.interface';

fdescribe('RecruitmentRequestService', () => {
  let reqService: RecruitmentRequestService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
    });

    reqService = TestBed.get(RecruitmentRequestService);
    httpMock = TestBed.get(HttpTestingController);
  });
  it('Should get request successfull', () => {
    const reqData = {
      Division: "Main",
        JobGrade: "A",
        JobId: "1",
        NumberOfRequired: "2",
        Position: "Programmer",
        ReportTo: "Girma Wake",
        RequestedBy: "Surafel Habte",
        StartingDate: "2012-10-07",
        Status: "Approved",
        TypeOfEmployment: "New",
        created_date: "2019-11-14 16:54:51",
        id: "1"
        };

    reqService.get_request(1).subscribe((data: any) => {
      expect(data).toBe(reqData);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(reqData);

    httpMock.verify();
  });

  it('Should send request', () => {
    const newRequest: Requestmodel = {
     JobGrade: "A",
     Division: "division",
        NumberOfRequired: 2,
        Position: "Programmer",
        ReportTo: "Girma Wake",
        RequestedBy: "Surafel Habte",
        StartingDate: new Date(),
        Status: "Approved",
        TypeOfEmployment: "New",
    };
    reqService.create(newRequest).subscribe((data: any) => {
      expect(data.JobGrade).toBe("A");
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(newRequest);
    httpMock.verify();
  });
  it('Should update request', () => {
    const updatedRequest = [
        { JobGrade: "A",
        JobId: "1",
        NumberOfRequired: "2",
        Position: "Programmer",
        ReportTo: "Girma Wake",
        RequestedBy: "Surafel Habte",
        StartingDate: "2012-10-07",
        Status: "Approved",
        TypeOfEmployment: "New",
        created_date: "2019-11-14 16:54:51",
        id: "1"}
    ]
    reqService.update(updatedRequest).subscribe((data: any) => {
      expect(data.JobId).toBe('1');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(updatedRequest);

    httpMock.verify();
  });

   it('Should delete/cancel request', () => {
    reqService.delete(12).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('DELETE');

    req.flush(12);

    httpMock.verify();
  });
});
