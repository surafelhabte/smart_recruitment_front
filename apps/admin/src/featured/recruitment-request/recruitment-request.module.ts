import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { RecruitmentRequestService } from './recruitment-request.service';
import { DetailsComponent } from './details/details.component';
import { ViewComponent } from './view/view.component';
import { CreateRequestComponent } from './create-request/create-request.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';

const routes: Routes = [
  {
    path: 'details/:id',
    component: DetailsComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'View', role: 'viewJobRequest' }
  },
  {
    path: '',
    component: ViewComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'List', role: 'viewJobRequestList' }
  },
  {
    path: 'create',
    component: CreateRequestComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Create', role: 'createJobRequest' }
  }
];

@NgModule({
  declarations: [DetailsComponent, ViewComponent, CreateRequestComponent],
  imports: [BasicModule, RouterModule.forChild(routes), DataGridModule, NgbModule],
  providers: [RecruitmentRequestService],
  exports: [RouterModule]
})
export class RecruitmentRequestModule { }
