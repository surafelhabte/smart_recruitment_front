export interface Requestmodel {
JobId? : number; 
Division : string;
Position : string;
NumberOfRequired : number;
JobGrade : string
ReportTo : string
TypeOfEmployment : string
StartingDate : Date,
RequestedBy : string
Status : string
}
