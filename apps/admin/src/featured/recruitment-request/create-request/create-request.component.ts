import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { Requestmodel } from '../request.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { RecruitmentRequestService } from '../recruitment-request.service';
import { JobRequirmentService } from '../../job-requirment/job-requirment.service';
import { Location } from '@angular/common';

@Component({
  selector: 'smart-recruitment-frontend-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.css']
})
export class CreateRequestComponent implements OnInit {
  requestForm: FormGroup;
  isUpdate = false;
  requestId: number;
  public lookupData = ['Division', 'TypeOfEmployment', 'ReportTo', 'JobGrade'];
  divisions: any = [];
  employmentTypes: any = [];
  positions: any = [];
  reportsTo: any = [];
  dateValue = new Date();
  submitted = false;
  public System_Message: any = { show: false, content: '', cssStyle: '' };
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private reqService: RecruitmentRequestService,
    private jobReqService: JobRequirmentService,
    private location: Location,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.requestId = +this.activatedRoute.snapshot.paramMap.get('id');
    this.reqService.loadLookupData(this.lookupData).subscribe((data: any) => {
      this.divisions = data.Division;
      this.employmentTypes = data.TypeOfEmployment;
      this.reportsTo = data.ReportTo;
    });
    this.jobReqService.get_jobs().subscribe((data: any) => {
      if (data.length > 0) {
        data.forEach(elem => {
          this.positions.push({ value: elem.Position, text: elem.Position });
        });
      }
    });
  }

  createForm(): void {
    this.requestForm = this.formBuilder.group({
      division: ['', Validators.required],
      position: ['', Validators.required],
      numberOfRequired: ['', Validators.required],
      jobGrade: ['', Validators.required],
      reportTo: ['', Validators.required],
      typeOfEmployment: ['', Validators.required],
      startingDate: ['', Validators.required]
    });
  }

  get division(): FormControl {
    return this.requestForm.get('division') as FormControl;
  }

  get position(): FormControl {
    return this.requestForm.get('position') as FormControl;
  }

  get numberOfRequired(): FormControl {
    return this.requestForm.get('numberOfRequired') as FormControl;
  }

  get jobGrade(): FormControl {
    return this.requestForm.get('jobGrade') as FormControl;
  }

  get reportTo(): FormControl {
    return this.requestForm.get('reportTo') as FormControl;
  }

  get typeOfEmployment(): FormControl {
    return this.requestForm.get('typeOfEmployment') as FormControl;
  }

  get startingDate(): FormControl {
    return this.requestForm.get('startingDate') as FormControl;
  }

  prepareFormData(): Requestmodel | null {
    if (this.requestForm.valid) {
      const user = JSON.parse(localStorage.getItem('recruitmentAccessToken'));
      return {
        Division: this.division.value,
        Position: this.position.value,
        NumberOfRequired: this.numberOfRequired.value,
        JobGrade: this.jobGrade.value,
        ReportTo: JSON.stringify(this.reportTo.value),
        TypeOfEmployment: this.typeOfEmployment.value,
        StartingDate: new Date(this.startingDate.value + 'UTC'),
        RequestedBy: user.userName,
        Status: 'Inprocess'
      };
    } else {
      return null;
    }
  }

  onSubmit() {
    const payload = this.prepareFormData();
    this.submitted = true;
    if (payload) {
      const conf = confirm('Are you sure you want to send this request');
      if (conf) {
        this.reqService.create(payload).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/job/request'], {
              state: {
                data: {
                  title: 'Success',
                  content: response.message,
                  cssClass: 'e-toast-success'
                }
              }
            });
          } else {
            this.System_Message = {
              show: true,
              content: response.message,
              cssStyle: 'danger'
            };
          }
        });
      }
    }
  }

  onPositionSelect(event){
    this.position.setValue(event.position);
    this.jobGrade.setValue(event.job_grade);
  }

}
