import { CreateRequestComponent } from "./create-request.component";
import { ComponentFixture, TestModuleMetadata, TestBed, async } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const crypto = require('crypto');
Object.defineProperty(global, 'crypto', {
  value: {
    getRandomValues: arr => crypto.randomBytes(arr.length)
  }
});
describe('CreateRequestComponent', () => {
  let component: CreateRequestComponent;
  let fixture: ComponentFixture<CreateRequestComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        BasicModule,
        NgbModule
      ],
      declarations: [CreateRequestComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(CreateRequestComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on send', () => {
    it('Submit should be called', () => {
      const submitFun = jest.spyOn(component, 'onSubmit');
      component.onSubmit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Request form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.requestForm.valid).toBeFalsy();
    });
    it('Signup fields validity sample for division', () => {
      const reqField = component.division;
      expect(reqField.valid).toBeFalsy();
      reqField.setValue('division');
      expect(reqField.valid).toBeTruthy();
    });
    it('Signup fields validity sample for starting date', () => {
      const reqField = component.startingDate;
      expect(reqField.valid).toBeFalsy();
      reqField.setValue(new Date());
      expect(reqField.valid).toBeTruthy();
    });
  });
});
