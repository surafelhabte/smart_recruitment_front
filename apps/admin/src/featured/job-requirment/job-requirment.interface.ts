export class JobRequirment {
    id?: number;
    Position: string;
    Department: string;
    Division: string;
    JobGrade: string;
    Field_Of_Study: string;
    Work_Experience: string;
    Special_Work_Experience: string
    Sector: string;
    Special_Year_Of_Experience: number;
    Remark: string
    Details?: string
}

export class QualificationAndExperience {
    id?: number;
    Qualification: string;
    Experience: number;
}

export class job {
    JobRequirment: JobRequirment;
    QualificationAndExperience: QualificationAndExperience[] = []
}