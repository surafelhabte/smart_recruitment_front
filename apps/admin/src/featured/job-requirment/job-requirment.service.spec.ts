import { JobRequirmentService } from './job-requirment.service';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { job, JobRequirment } from './job-requirment.interface';

fdescribe('JobRequirmentService', () => {
  let reqService: JobRequirmentService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    reqService = TestBed.get(JobRequirmentService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should create job requirment', () => {
    const requirment: JobRequirment = {
      Position: 'Accountant',
      Department: 'department',
      Division: 'division',
      JobGrade: 'job grade',
      Field_Of_Study: 'software',
      Work_Experience: '1',
      Sector: 'sector',
      Special_Work_Experience: 'accounting',
      Special_Year_Of_Experience: 1,
      Remark: 'remark'
    };
    reqService.create(requirment).subscribe((data: any) => {
      expect(data.JobGrade).toBe('job grade');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(requirment);
    httpMock.verify();
  });

  it('Should get and list job requirments', () => {
    const requirment: JobRequirment = {
      Position: 'Accountat',
      Department: 'department',
      Division: 'division',
      JobGrade: 'job grade',
      Field_Of_Study: 'software',
      Work_Experience: '1',
      Sector: 'sector',
      Special_Work_Experience: 'accounting',
      Special_Year_Of_Experience: 1,
      Remark: 'remark'
    };

    reqService.get_jobs().subscribe((data: any) => {
      expect(data).toBe(requirment);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(requirment);

    httpMock.verify();
  });

  it('Should get job successfull', () => {
    const requirment: JobRequirment = {
      id: 2,
      Position: 'accountant',
      Department: 'department',
      Division: 'division',
      JobGrade: 'job grade',
      Field_Of_Study: 'software',
      Work_Experience: '1',
      Sector: 'sector',
      Special_Work_Experience: 'accounting',
      Special_Year_Of_Experience: 1,
      Remark: 'remark'
    };

    reqService.get_job(requirment.id).subscribe((data: any) => {
      expect(data).toBe(requirment);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(requirment);

    httpMock.verify();
  });

  it('Should update job requirment', () => {
    const requirment: JobRequirment = {
      id: 2,
      Position: 'accountant',
      Department: 'department',
      Division: 'division',
      JobGrade: 'job grade',
      Field_Of_Study: 'software',
      Work_Experience: '1',
      Sector: 'sector',
      Special_Work_Experience: 'accounting',
      Special_Year_Of_Experience: 1,
      Remark: 'remark'
    };

    reqService.update(requirment.id).subscribe((data: any) => {
      expect(data.Sector).toBe('sector');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(requirment);

    httpMock.verify();
  });

  it('Should delete job requirment', () => {
    reqService.delete(12).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(12);

    httpMock.verify();
  });
});
