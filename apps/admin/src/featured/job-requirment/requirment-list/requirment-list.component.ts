import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectionStrategy
} from '@angular/core';
import { JobRequirmentService } from '../job-requirment.service';
import { Router } from '@angular/router';
import { GridModel, GroupSettingsModel } from '@syncfusion/ej2-grids';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';

@Component({
  selector: 'smart-recruitment-frontend-requirment-list',
  templateUrl: './requirment-list.component.html',
  styleUrls: ['./requirment-list.component.css']
})
export class RequirmentListComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent
  public jobs = [];
  public childData = [];
  public columensToDisplay: any[] = [];
  public searchField = 'Position';
  public groupOptions: GroupSettingsModel = { showDropArea: true };
  public groupBy = ['JobGrade'];
  public incomingCommand = ['edit', 'delete'];

  public childGrid: GridModel = {
    dataSource: this.childData,
    queryString: 'JobId',
    columns: [
      { field: 'Qualification', headerText: 'Qualification', width: 150 },
      { field: 'Experience', headerText: 'Experience', width: 150 }
    ]
  };
  constructor(private Service: JobRequirmentService, private router?: Router) {
    this.columensToDisplay.push({
      field: 'Position',
      headerText: 'Position',
      textAlign: 'left',
      width: 120
    });
    this.columensToDisplay.push({
      field: 'Department',
      headerText: 'Department',
      textAlign: 'left',
      width: 90
    });
    this.columensToDisplay.push({
      field: 'Division',
      headerText: 'Division',
      textAlign: 'left',
      width: 90
    });
    this.columensToDisplay.push({
      field: 'JobGrade',
      headerText: 'Grade',
      textAlign: 'left',
      width: 50
    });
    this.columensToDisplay.push({
      field: 'Field_Of_Study',
      headerText: 'Field of studies',
      textAlign: 'left',
      width: 120
    });
    this.columensToDisplay.push({
      field: 'Work_Experience',
      headerText: 'Work experience',
      textAlign: 'left',
      width: 100
    })
  }

  ngOnInit() {
    this.Service.get_jobs().subscribe((data: any[]) => {
      this.jobs = data;
      this.jobs.forEach(element => {
        element.QualificationAndExperience.forEach(elem => {
          this.childData.push(elem);
        });
      });
    });

    this.groupOptions = {
      disablePageWiseAggregates: false,
      showDropArea: true,
      columns: ['JobGrade']
    };
  }

  edit($event) {
    this.router.navigate(['job-requirment/' + $event.JobId + '/update']);
  }

  delete(item: any) {
    const deleteInfo = { table: 'Jobs', id: item.JobId };
    this.Service.delete(deleteInfo).subscribe((response: any) => {
      if (response.status === true) {
        this.notif.Show({ title: 'Success', content: response.message, cssClass: 'e-toast-success' })
        this.ngOnInit();
      } else {
        this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' })
      }
    });
  }

  addJob() {
    this.router.navigate(['job-requirment/create']);
  }
}
