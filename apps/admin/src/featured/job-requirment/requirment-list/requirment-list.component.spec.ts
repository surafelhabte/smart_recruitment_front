import { RequirmentListComponent } from "./requirment-list.component";
import { JobRequirmentService } from '../job-requirment.service';
import { async, ComponentFixture, TestModuleMetadata, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { SecurityService } from 'apps/admin/src/Core/Security/security-service';

const crypto = require('crypto');
Object.defineProperty(global, 'crypto', {
  value: {
    getRandomValues: arr => crypto.randomBytes(arr.length)
  }
});
describe('RequirmentListComponent', () => {
  let component: RequirmentListComponent;
  let fixture: ComponentFixture<RequirmentListComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        RouterTestingModule, DataGridModule, HttpClientTestingModule,BasicModule
      ],
      declarations: [RequirmentListComponent],
      providers: [JobRequirmentService, SecurityService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(RequirmentListComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
