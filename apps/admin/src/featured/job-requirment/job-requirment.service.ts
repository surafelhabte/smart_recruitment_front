import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JobRequirmentService {
  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   create(job: any): any {
    return this.http.post(this.url + 'job/create', job)
   }

   get_jobs() {
    return this.http.get(this.url + 'job/Getjobs')
   }

   get_job(id: any) {
    return this.http.get(this.url + 'job/Getjob/' + id)
   }

   update(job: any) {
    return this.http.post(this.url + 'job/Update', job)
   }

  delete(deleteInfo) {
    return this.http.post(this.url + 'job/delete', deleteInfo)
  }

  load_data(query:any){
    return this.http.post(this.url + 'lookup/load_lookups', query)
  }


  get_position_info(data: any) {
    return this.http.post(this.url + 'job/GetPositionInfo', data)
   }

}
