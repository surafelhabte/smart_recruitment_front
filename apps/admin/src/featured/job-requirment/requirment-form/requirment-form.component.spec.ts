import { RequirmentFormComponent } from "./requirment-form.component";
import { ComponentFixture, TestModuleMetadata, TestBed, async } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { JobRequirmentService } from '../job-requirment.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


const crypto = require('crypto');
Object.defineProperty(global, 'crypto', {
  value: {
    getRandomValues: arr => crypto.randomBytes(arr.length)
  }
});

describe('RequirmentFormComponent', () => {
  let component: RequirmentFormComponent;
  let fixture: ComponentFixture<RequirmentFormComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        BasicModule,
        NgbModule
      ],
      declarations: [RequirmentFormComponent],
      providers: [JobRequirmentService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(RequirmentFormComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on submit', () => {
    it('Submit should be called', () => {
      const submitFun = jest.spyOn(component, 'onSubmit');
      component.onSubmit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Requirment form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.jobRequirmentForm.valid).toBeFalsy();
    });
    it('requirment fields validity sample for division', () => {
      const reqField = component.division;
      expect(reqField.valid).toBeFalsy();
      reqField.setValue('division');
      expect(reqField.valid).toBeTruthy();
    });
  });
});
