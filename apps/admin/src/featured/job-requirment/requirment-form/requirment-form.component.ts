import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { JobRequirmentService } from '../job-requirment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { job, QualificationAndExperience } from '../job-requirment.interface';
import { Location } from '@angular/common'
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { distinctUntilChanged } from 'rxjs/operators'; 

@Component({
  selector: 'smart-recruitment-frontend-requirment-form',
  templateUrl: './requirment-form.component.html',
  styleUrls: ['./requirment-form.component.css']
})
export class RequirmentFormComponent implements OnInit, AfterViewInit {
  jobRequirmentForm: FormGroup
  isUpdate = false;
  jobId : number;
  jobList: any
  lookUpdata = ['Division','Department',"FieldOfStudy","WorkExperience","Sector","Qualification", "JobGrade"];
  fieldOfStudies: any = [];
  divisions: any[] = [];
  workExperiences: any = [];
  sectors: any []= [];
  departments: any []= []
  qualifications: any [] = [];
  grades: any = [];
  deptFields: any;
  submitted = false;
  public Editor = ClassicEditor;
  formarrayValidity = true;
  public System_Message : any = {show: false, content: '', cssStyle: ''};
  public fields: any = { text: 'dep_name', value: 'dep_id' };

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private jobReqService: JobRequirmentService, private location: Location, private router: Router) { this.createForm() }

  ngOnInit() {
    this.jobId = +this.activatedRoute.snapshot.paramMap.get("jobId");
    if(this.jobId) {
      this.isUpdate = true;
      this.jobReqService.get_job(this.jobId).subscribe((data: any) => {
      this.jobList = data;
       this.initializeForm(this.jobList);
    this.deptFields = [{text: this.jobRequirmentForm.controls.department.value, value: this.jobRequirmentForm.controls.department.value}]
      })
    }
    this.jobReqService.load_data(this.lookUpdata).subscribe((data: any) => {
        this.divisions = data.Division;
        this.qualifications = data.Qualification;
        this.fieldOfStudies = data.FieldOfStudy;
        this.workExperiences = data.WorkExperience
        this.sectors = data.Sector
    })
  }

  createForm(): void {
    this.jobRequirmentForm = this.formBuilder.group({
      position: ['', Validators.required],
      department: ['', Validators.required],
      division: ['', Validators.required],
      jobGrade: ['', Validators.required],
      fieldOfStudy: ['', Validators.required],
      workExperience: [''],
      sector: [''],
      specialYearExperience: [''],
      specialWorkExperience: [''],
      remark: [''],
      jobDescription: [''],
      qualAndExp: this.formBuilder.array([this.createQualAndExp()])
    });
  }

  initializeForm(jobs: job) {
      this.jobRequirmentForm = this.formBuilder.group({
      id: [jobs[0].id],
      position: [jobs[0].Position, Validators.required],
      department: [jobs[0].Department, Validators.required],
      division: [jobs[0].Division, Validators.required],
      jobGrade: [jobs[0].JobGrade, Validators.required],
      fieldOfStudy: [jobs[0].Field_Of_Study, Validators.required],
      workExperience: [jobs[0].Work_Experience],
      specialWorkExperience: [jobs[0].Special_Work_Experience],
      sector: [jobs[0].Sector],
      specialYearExperience: [jobs[0].Special_Year_Of_Experience],
      remark: [jobs[0].Remark],
      jobDescription: [jobs[0].Details],
      qualAndExp: this.formBuilder.array([])
    });

    jobs[0].QualificationAndExperience.forEach(element => {
      this.qualAndExp.controls.push(this.initializeQE(element));
    });
  }

  addQualAndExp(): void {
    this.qualAndExp.controls.push(this.createQualAndExp());
  }

  createQualAndExp() {
    return this.formBuilder.group({
      levelOfQualification: ['',Validators.required],
      yearExperience: ['', Validators.required]
    })
  }

  initializeQE(QE: QualificationAndExperience): FormGroup {
    return this.formBuilder.group({
      id: [QE.id, Validators.required],
      levelOfQualification: [QE.Qualification, Validators.required],
      yearExperience: [QE.Experience, Validators.required]
    });
  }

  deleteQualAndExp(index) {
    const deletedControlId = this.qualAndExp.controls[index].get('id');
    if(deletedControlId) {
      const conf = confirm("Are you sure you want to delete this item?")
      if(conf) {
        const itemInfo = {table: "Qualification_And_Experience", id: deletedControlId.value}
        this.jobReqService.delete(itemInfo).subscribe((data: any) => {
          alert(data.message);
        })  
      }
      this.qualAndExp.removeAt(index);
    }
    else {
      this.qualAndExp.removeAt(index);
    }
  }

  get position() : FormControl {
    return this.jobRequirmentForm.get("position") as FormControl;
  }

  get division() : FormControl {
    return this.jobRequirmentForm.get("division") as FormControl;
  }

  get department() : FormControl {
    return this.jobRequirmentForm.get("department") as FormControl;
  }
  
  get jobGrade() : FormControl {
    return this.jobRequirmentForm.get("jobGrade") as FormControl;
  }

  get fieldOfStudy() : FormControl {
    return this.jobRequirmentForm.get("fieldOfStudy") as FormControl;
  }

  get workExperience() : FormControl {
    return this.jobRequirmentForm.get("workExperience") as FormControl;
  }

  get specialWorkExperience() : FormControl {
    return this.jobRequirmentForm.get("specialWorkExperience") as FormControl;
  }

  get sector() : FormControl {
    return this.jobRequirmentForm.get("sector") as FormControl;
  }

  get specialYearExperience() : FormControl {
    return this.jobRequirmentForm.get("specialYearExperience") as FormControl;
  }

  get remark(): FormControl {
    return this.jobRequirmentForm.get('remark') as FormControl;
  }

  get qualAndExp(): FormArray {
    return this.jobRequirmentForm.get('qualAndExp') as FormArray;
  }

  get jobDesciption(): FormControl {
    return this.jobRequirmentForm.get('jobDescription') as FormControl;
  }

  prepareFormData(): job | null {
    if(this.qualAndExp.controls.length > 0) {
      this.qualAndExp.controls.forEach(element => {
       this.formarrayValidity = element.valid
      })
    }
      let jobs = new job();
    if (this.jobRequirmentForm.valid && this.formarrayValidity) {
      if(this.isUpdate) {
        jobs.JobRequirment = {
        id: this.jobId,  
        Position: this.position.value,
        Department: this.department.value,
        Division: this.division.value,
        JobGrade: this.jobGrade.value,
        Field_Of_Study: JSON.stringify(this.fieldOfStudy.value),
        Work_Experience: JSON.stringify(this.workExperience.value),
        Special_Work_Experience: this.specialWorkExperience.value,
        Sector: JSON.stringify(this.sector.value),
        Special_Year_Of_Experience: this.specialYearExperience.value,
        Remark: this.remark.value,
        Details: this.jobDesciption.value
      }; 
      this.qualAndExp.controls.forEach(element => { 
        jobs.QualificationAndExperience.push({
          Qualification: element.value.levelOfQualification,
          Experience: element.value.yearExperience,
          id: element.value.id
        })
      })
      } 
      else { 
        jobs.JobRequirment = {
        Position: this.position.value,
        Department: this.department.value,
        Division: this.division.value,
        JobGrade: this.jobGrade.value,
        Field_Of_Study: JSON.stringify(this.fieldOfStudy.value),
        Work_Experience: JSON.stringify(this.workExperience.value),
        Special_Work_Experience: this.specialWorkExperience.value,
        Sector: JSON.stringify(this.sector.value),
        Special_Year_Of_Experience: this.specialYearExperience.value,
        Remark: this.remark.value,
        Details: this.jobDesciption.value
      };  
      this.qualAndExp.controls.forEach(element => {
        jobs.QualificationAndExperience.push({
          Qualification: element.value.levelOfQualification,
          Experience: element.value.yearExperience,
        })
      })
      }

      return jobs
    } else { 
      return null;
    } 
  }

  onSubmit() {
    const payload = this.prepareFormData();
     this.submitted = true;
    if (payload) {
      if(this.jobId) {
         this.jobReqService.update(payload).subscribe((response: any) => {
           if(response.status){
          this.router.navigate(['/job-requirment'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
           } else {
            this.System_Message = {show: true, content: response.message, cssStyle: 'danger'};
           }
        });
      }
      else {
        this.jobReqService.create(payload).subscribe((response: any) => {
          if(response.status){
          this.router.navigate(['/job-requirment'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          } else {
           this.System_Message = {show: true, content: response.message, cssStyle: 'danger'};
          }
        });
      }
    }
  }

  ngAfterViewInit() {
  this.specialWorkExperience.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
  if (value) {
    this.specialYearExperience.setValidators([Validators.required])
  }
  else {
    this.specialYearExperience.setValidators(null);
  }
    this.specialYearExperience.updateValueAndValidity();
});
this.specialYearExperience.valueChanges.pipe(distinctUntilChanged()).subscribe(value => {
  if (value) {
    this.specialWorkExperience.setValidators([Validators.required])
  }
  else {
    this.specialWorkExperience.setValidators(null);
  }
    this.specialWorkExperience.updateValueAndValidity();
});
  }

  onPositionSelect(event){
    this.position.setValue(event.position);
    this.jobGrade.setValue(event.job_grade);
    this.departments = [];
    this.departments.push({ 'dep_name':event.dep_name, 'dep_id':event.dep_id });
    this.department.setValue(event.dep_id);
  }

}
