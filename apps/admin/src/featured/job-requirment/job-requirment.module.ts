import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequirmentFormComponent } from './requirment-form/requirment-form.component';
import { Routes, RouterModule } from '@angular/router';
import { RequirmentListComponent } from './requirment-list/requirment-list.component';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { JobRequirmentService } from './job-requirment.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';

const routes: Routes = [
  {
    path: 'create',
    component: RequirmentFormComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Create', role: 'createJobRequirment' }
  },
  {
    path: ':jobId/update',
    component: RequirmentFormComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Edit', role: 'editJobRequirment' }
  },
  {
    path: '',
    component: RequirmentListComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'list', role: 'viewJobRequirments' }
  }
];

@NgModule({
  declarations: [RequirmentFormComponent, RequirmentListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BasicModule,
    DataGridModule,
    NgbModule
  ],
  providers: [JobRequirmentService],
  exports: [RouterModule]
})
export class JobRequirmentModule { }
