import { Component} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DetailsComponent } from '../details/details.component';
import { ApplicationService } from '../../application/application.service';

@Component({
  selector: 'app-secondary',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
})

export class ViewComponent {
  public Applications: any[] = [];
  public Vacancies: any[] = [];
  public current_Step: any;
  public selectedApplicants: any[] = [];
  public System_Message: any = { show: false, content: '', cssStyle: '' };

  vacancy = new FormControl('');

  constructor(public service: ApplicationService, private router?: Router, private modalService?: NgbModal) {
    this.load_Vacancies();
  }

  ShowDetail(id) {
    const modalRef = this.modalService.open(DetailsComponent, { size: 'xl' });
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.current_Step = this.current_Step;
    modalRef.componentInstance.vacancy = this.vacancy.value;
  }

  load_Vacancies() {
    this.service.get_vacancies()
    .subscribe((data: any[]) => {
      const incomingData = data;
      if (incomingData.length > 0) {
        incomingData.forEach(element => {
          this.Vacancies.push({ value: element.value, text: element.text, date: element.date });
        });
      }
    });
  }

  load_Application(value) {
    this.service.get_applications_second(value)
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.Applications = data;
          this.current_Step = data[0].Shortlist;
        } else {
          this.Applications = [];
        }
      });
  }

  select_Applicant(array_index: any, applicant_id: any, checked) {
    if (checked) {
      this.selectedApplicants.push({ index: array_index, id: applicant_id });
    } else {
      const z: number = this.selectedApplicants.indexOf({ index: array_index, id: applicant_id });
      this.selectedApplicants.splice(z, 1);
    }
  }

  takeAction(Status: any, warning: any) {
    if (confirm('are you sure you want to ' + warning + ' the following application ?')) {
      const applications: any[] = [];
      let message: any = '';
      if (Status === 'Failed') {
        message = { success: 'Application Dropped Successfully.', failure: 'Unable to Drop Application.' };
        this.selectedApplicants.forEach((element, index) => {
          applications.push({ id: element.id, Status: 'Failed' });
        });
      } else {
        message = { success: 'Applications Updated Successfully.', failure: 'Application Not Updated Successfully.' };
        this.selectedApplicants.forEach((element, index) => {
          applications.push({ id: element.id, Shortlist: this.current_Step === "First" ? 'Second' : null });
        });
      }

      this.service.update({ applications: applications, vacancy_Id: this.vacancy.value })
        .subscribe((response: any) => {
          if (response.status) {
            this.System_Message = { show: true, content: message.success, cssStyle: 'success' };
            this.selectedApplicants = [];
            this.load_Application(this.vacancy.value);
          } else {
            this.System_Message = { show: true, content: message.failure, cssStyle: 'danger' };
          }
        });
    }
  }

}
