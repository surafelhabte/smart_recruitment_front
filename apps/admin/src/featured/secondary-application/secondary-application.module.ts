import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ViewComponent } from './view/view.component';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { HtmlSanitaizerModule } from './htmlSanitaizer/html-sanitaizer.module';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';


const routes: Routes = [
  {
    path: '',
    component: ViewComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'List', role: "managePriliminaryListedApplications" }
  },
  {
    path: 'details/:id',
    component: DetailsComponent,
    canActivate: [AuthGuardGuard],
  }
];

@NgModule({
  imports: [
    BasicModule,
    NgbModule,
    HtmlSanitaizerModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ViewComponent, DetailsComponent],
  exports: [RouterModule]
})
export class NgbdTableCompleteModule { }
