import { DetailsComponent } from './details.component';

import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';

import { DebugElement } from '@angular/core';

import { RouterTestingModule } from '@angular/router/testing';

import { DataGridModule } from '@smart-recruitment-frontend/data-grid';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ApplicationService } from '../view/application.service';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HtmlSanitaizerModule } from '../htmlSanitaizer/html-sanitaizer.module';

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        RouterTestingModule,
        DataGridModule,
        HttpClientTestingModule,
        NgbModule,
        HtmlSanitaizerModule
      ],
      declarations: [DetailsComponent],
      providers: [ApplicationService, NgbActiveModal]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
