import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';
import { ApplicationService } from '../../application/application.service';

@Component({
  selector: 'smart-recruitment-frontend-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  public BioData: any;
  public Qualifications: any;
  public Experiences: any;
  public References: any;
  public Trainings: any;
  @Input() id;
  @Input() current_Step;
  @Input() vacancy;
  @ViewChild('notif', { static: true }) notif: ToastComponent;
  constructor(public service: ApplicationService, private router?: Router, public activeModal?: NgbActiveModal) {
  }

  ngOnInit(): void {
    if (this.id !== undefined) {
      this.service.get_application(this.id)
        .subscribe((data: any) => {
          if (data) {
            this.BioData = data.bio_data;
            this.Qualifications = data.Qualifications;
            this.Experiences = data.Experiences;
            this.References = data.References;
            this.Trainings = data.Trainings;
          }
        });
    }
  }

  takeAction(id: any, Status: any, warning: any) {
    if (confirm('are you sure you want to ' + warning + ' the following application ?')) {
      let message: any = '';
      let application: any = '';
      if (Status === 'Failed') {
        message = { success: 'Application Dropped Successfully.', failure: 'Unable to Drop Application.' };
        application = { id: id, Status: 'Failed' };
      } else {
        message = {
          success: this.current_Step === null ? 'Applications Submit Successfully.' : 'Applications Approved Successfully.',
          failure: this.current_Step === null ? 'Unable to Submit Applications.' : 'Unable to Approve Applications.'
        };
        application = { id: id, Shortlist: this.current_Step === null ? 'Second' : 'Completed' }
      }
      this.service.update({ applications: [application], vacancy_Id: this.vacancy })
        .subscribe((response: any) => {
          if (response.status) {
            alert(message.success);
            this.activeModal.dismiss('Cross click');
            this.router.navigate(['application'], { state: { data: { title: 'Success', content: message.success, cssClass: 'e-toast-success' } } });
          } else {
            this.router.navigate(['application'], { state: { data: { title: 'Success', content: message.failure, cssClass: 'e-toast-danger' } } });
          }
        });
    }
  }

  printApplication() {
    var domClone = document.getElementById('doc').cloneNode(true);

    var $printSection = document.getElementById("printSection");

    if (!$printSection) {
      $printSection = document.createElement("div");
      $printSection.id = "printSection";
      document.body.appendChild($printSection);
    }

    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
  }

}
