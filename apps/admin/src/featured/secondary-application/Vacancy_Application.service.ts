import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Vacancy_Application {

  private url = environment.baseUrl;

  constructor(private http?: HttpClient) { }

  get_applications(vacancyId: any) {
    return this.http.get(this.url + 'Application/Get_First_Shortlisted/' + vacancyId)
  }

  get_application(id: any) {
    return this.http.get(this.url + 'Application/GetApplication/' + id)
  }

  update(application: any) {
    return this.http.post(this.url + 'Application/Update', application)
  }

  filter_applications(data: any) {
    return this.http.post(this.url + 'Application/Get_Shortlisted_Applicant/', data)
  }

  get_vacancies() {
    return this.http.get(this.url + 'Application/GetVacancies')
  }
}
