import { NgModule } from '@angular/core';
import { CreateComponent } from './create/create.component';
import { Routes, RouterModule } from '@angular/router';
import { LookupService } from './lookup.service';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { ViewComponent } from './view/view.component';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';

const routes: Routes = [
  {
    path: 'create',
    component: CreateComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Create', role: 'createLookup' }
  },
  {
    path: '',
    component: ViewComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'List', role: 'viewLookups' }
  }
];

@NgModule({
  declarations: [CreateComponent, ViewComponent],
  imports: [BasicModule, RouterModule.forChild(routes), DataGridModule, NgbModule],
  providers: [LookupService],
  exports: [RouterModule]
})
export class LookupModule { }
