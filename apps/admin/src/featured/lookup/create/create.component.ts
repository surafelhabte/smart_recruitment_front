import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormArray
} from '@angular/forms';
import { LookupService } from '../lookup.service';
import { Location } from '@angular/common';

@Component({
  selector: 'smart-recruitment-frontend-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public form: FormGroup;

  public formSubmitted = false;
  public invalidClass = false;
  public hideCreateButton = true;
  public hideSaveChangesButton = false;
  public currentEditingID: any;
  public title_Data: any[] = [];
  public fields: any = { text: 'text', value: 'value' };
  public System_Message : any = {show: false, content: '', cssStyle: ''};

  constructor(
    private Service: LookupService,
    private fb?: FormBuilder,
    private router?: Router,
    private actRoute?: ActivatedRoute,
    private location?: Location
  ) {
    this.form = this.fb.group({
      Lookups: this.fb.array([])
    });
  }

  get get_Lookups() {
    return this.form.get('Lookups') as FormArray;
  }

  ngOnInit() {
    this.add_Lookup();
  }

  onSubmit(): any {
    if (!this.checkFormArrayError()) {
      this.formSubmitted = true;
      this.invalidClass = true;
      this.System_Message = {show: true, content: 'please fill the required fields.', cssStyle: 'info'};
      return;
    } else {
      let Lookups: any[] = [];
      this.get_Lookups.controls.forEach(element => {
        const lookup = {
          Title: element.value.Title,
          Value: element.value.Value,
          Details: element.value.Details
        };
        Lookups.push(lookup);
      });

      return this.Service.create({
        Lookups: JSON.stringify(Lookups)
      }).subscribe(response => {
        if (response.status) {
          this.router.navigate(['/lookup'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
        } else {
          this.System_Message = {show: true, content: response.message, cssStyle: 'danger'};
        }
      });
    }
  }

  Create_Lookup(): FormGroup {
    return this.fb.group({
      Title: ['', Validators.required],
      Value: ['', Validators.required],
      Details: ['']
    });
  }

  add_Lookup() {
    this.load_title_data();
    const LLA = this.Create_Lookup();
    this.get_Lookups.push(LLA);
  }

  Remove_lookup(index: number) {
    this.get_Lookups.removeAt(index);
  }

  load_title_data() {
    this.Service.load_lookup_data().subscribe((data: any[]) => {
      if (data.length > 0) {
        data.forEach(element => {
          this.title_Data.push({ value: element.value, text: element.text });
        });
      }
    });
  }

  getFormGroup(index): FormGroup {
    const formGroup = this.get_Lookups.controls[index] as FormGroup;
    return formGroup;
  }

  checkFormArrayError(): boolean {
    const FormArrayErrors: any[] = [];
    if (this.get_Lookups.length !== 0) {
      const FormArrayData = this.get_Lookups.controls;
      FormArrayData.forEach(element => {
        if (element.invalid) {
          FormArrayErrors.push(true);
        }
      });
      if (FormArrayErrors.length === 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
  
}
