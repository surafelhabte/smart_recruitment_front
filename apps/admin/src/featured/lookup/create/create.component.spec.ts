import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { LookupService } from '../lookup.service';
import { CreateComponent } from './create.component';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';

describe('Create Lookup component', () => {
  let Service: LookupService;
  let page: CreateComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, BasicModule],
      providers: [LookupService]
    });

    page = new CreateComponent(Service, new FormBuilder());
    Service = TestBed.get(LookupService);
  });

  it('form should have a control', () => {
    expect(page.form.contains('Lookups')).toBeTruthy();
  });

  it(' Should create FormArray', () => {
    page.add_Lookup();
    expect(page.get_Lookups.length).toBeGreaterThan(0);
  });

  it(' Should Remove FormArray', () => {
    const spy = spyOn(window, 'confirm');
    const removeFun = jest.spyOn(page, 'Remove_lookup');
    page.Remove_lookup(0);
    expect(page.get_Lookups.length).not.toBeGreaterThan(0);
    expect(removeFun).toHaveBeenCalled();
  });

  it('Should create Lookups successfull and redirect the to list', () => {
    page.add_Lookup();
    let Lookups: any[] = [];

    page.get_Lookups.controls.forEach(element => {
      const lookup = {
        Title: element.value.Title,
        Value: element.value.Value,
        Details: element.value.Details
      };
      Lookups.push(lookup);
    });

    const router = TestBed.get(Router);
    const router_spy = spyOn(router, 'navigateByUrl');
    const window_spy = spyOn(window, 'alert');

    Service.create({ Lookups: JSON.stringify(Lookups) }).subscribe(
      (response: any) => {
        if (response.status == true) {
          expect(window_spy).toHaveBeenCalledWith([response.message]);
          expect(router_spy).toHaveBeenCalledWith(['lookup/view']);
        } else {
          expect(window_spy).toHaveBeenCalledWith([response.message]);
        }
      }
    );
  });

  it(' : Should load Title Data and populate drop down list', () => {
    Service.load_lookup_data().subscribe((data: any) => {
      expect(page.title_Data).toBeGreaterThan(0);
    });
  });
});
