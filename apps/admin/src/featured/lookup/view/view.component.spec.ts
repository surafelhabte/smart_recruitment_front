import { TestBed  } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { LookupService } from '../lookup.service';
import { ViewComponent } from './view.component';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';


describe('View Lookup component', () => {
    let Service: LookupService;
    let page: ViewComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule, BasicModule],
        providers: [LookupService]
      });
      page = new ViewComponent(Service, new FormBuilder);

      // inject the service
      Service = TestBed.get(LookupService);
  });

    it(': form should have a control', () => {
      expect(page.form.contains('Title')).toBeTruthy();
      expect(page.form.contains('Value')).toBeTruthy();
      expect(page.form.contains('Details')).toBeTruthy();
  });

  // validation
    it(': form control should have a validation required', () => {
    const Title = page.form.get('Title');
    const Value = page.form.get('Value');

    Title.setValue('');
    expect(Title.valid).toBeFalsy();

    Value.setValue('');
    expect(Value.valid).toBeFalsy();

  });

    it(' : Should featch and fill the the requested data into form controls', () => {
      const Title = page.form.controls.Title;
      const Value = page.form.controls.Value;
      const Details = page.form.controls.Details;
    
      Service.get_lookup(0).subscribe((data: any) => {
        Title.setValue(data.Title);
        Value.setValue(data.Value);
        Details.setValue(data.Details);
      
        expect(Title.value).not.toBe('');
        expect(Value.value).not.toBe('');
        expect(Details.value).not.toBe('');
      });
  });

    it('Should update lookup successfull and redirect to lookups list', () => {
    const lookup = { id: '1', Title: 'City', Value: 'Addis Ababa', Details : 'Yelebe Ketema'};

    const Title = page.form.get('Title');
    const Value = page.form.get('Value');
    const Details = page.form.get('Details');
    
    const spy = spyOn(page, 'ngOnInit');
    const spy2 = spyOn(window, 'alert');

    Title.setValue('City');
    Value.setValue('Addis Ababa');
    Details.setValue('Yelebe Ketema');

    Service.update(lookup).subscribe((response: any) => {
      if(response.status == true){
          expect(spy2).toHaveBeenCalledWith([response.message]); 
          expect(spy).toHaveBeenCalled();
          expect(Title.setValue).toBeNull();
          expect(Value.setValue).toBeNull();
          expect(Details.setValue).toBeNull();
      } else {
        expect(spy2).toHaveBeenCalledWith([response.message]); 
      }
   });

  });

});
