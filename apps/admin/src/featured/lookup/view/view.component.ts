import { Component, OnInit, ViewChild } from '@angular/core';
import { LookupService } from '../lookup.service';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';

@Component({
  selector: 'smart-recruitment-frontend-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent
  public form: FormGroup;
  public formSubmitted = false;
  public invalidClass = false;

  public lookups: any[] = [];
  public lookup = 'lookup';
  public fields: Object;
  public title_Data: any[] = [];
  public columensToDisplay: any[] = [];
  public searchField = 'Title';
  public incomingToolbar = [
    { text: 'Add', tooltipText: 'Add Items' },
    { text: 'Filter', tooltipText: 'Filter' },
    { text: 'Search', tooltipText: 'Search Items' }
  ];
  public incomingCommand = ['edit', 'delete'];
 
  public current_editing_id: any = null;
  public System_Message: any = { show: false, content: '', cssStyle: '' };

  constructor(
    private Service: LookupService,
    private fb?: FormBuilder,
    private router?: Router
  ) {
    this.fields = { value: 'value', text: 'text' };
    this.form = this.fb.group({
      Title: ['', Validators.required],
      Value: ['', Validators.required],
      Details: ['']
    });

    this.columensToDisplay.push({
      field: 'Text',
      headerText: 'Title',
      textAlign: 'left',
      width: 90
    });
    this.columensToDisplay.push({
      field: 'Value',
      headerText: 'Value',
      textAlign: 'left',
      width: 100
    });
    this.columensToDisplay.push({
      field: 'Details',
      headerText: 'Details',
      textAlign: 'left',
      width: 90,
      clipMode: 'EllipsisWithTooltip'
    });
  }

  get get_Title() {
    return this.form.get('Title') as FormControl;
  }

  get get_Value() {
    return this.form.get('Value') as FormControl;
  }

  get get_Details() {
    return this.form.get('Details') as FormControl;
  }

  ngOnInit() {
    this.load_title_data();
    this.Service.get_lookups().subscribe((data: any[]) => {
      this.lookups = data;
    });
  }

  load_title_data() {
    this.Service.load_lookup_data().subscribe((data: any[]) => {
      if (data.length > 0) {
        data.forEach(element => {
          this.title_Data.push({ value: element.value, text: element.text });
        });
      }
    });
  }

  onSubmit(): any {
    if (this.get_Title.errors || this.get_Value.errors) {
      this.formSubmitted = true;
      this.invalidClass = true;
      this.System_Message = { show: true, content: 'please fill the required fields.', cssStyle: 'info' };
      return;
    } else {
      this.invalidClass = false;
      this.Service.update({
        id: this.current_editing_id,
        Title: this.form.controls.Title.value,
        Value: this.form.controls.Value.value,
        Details: this.form.controls.Details.value
      }).subscribe((response: any) => {
        if (response.status) {
          this.System_Message = { show: true, content: response.message, cssStyle: 'success' };
          this.get_Title.setValue(null);
          this.get_Value.setValue('');
          this.get_Details.setValue('');
          this.current_editing_id = null;
          this.ngOnInit();
        } else {
          this.System_Message = { show: true, content: response.message, cssStyle: 'danger' };
        }
      });
    }
  }

  edit($event) {
    this.form.enable();
    this.lookups.forEach(lookup => {
      if (lookup.id.indexOf($event.id) > -1) {
        this.get_Title.setValue(lookup.Title);
        this.get_Value.setValue(lookup.Value);
        this.get_Details.setValue(lookup.Details);
        this.current_editing_id = $event.id;
      }
    });
  }

  delete(item: any) {
    this.Service.delete(item.id).subscribe((response: any) => {
      if (response.status === true) {
        this.notif.Show({ title: 'Success', content: response.message, cssClass: 'e-toast-info' })
        this.ngOnInit();
      } else {
        this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' })
      }
    });
  }

  addNew() {
    this.router.navigate(['lookup/create']);
  }

}
