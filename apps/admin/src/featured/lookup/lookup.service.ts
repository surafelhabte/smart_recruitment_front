import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LookupService {
  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   create(lookup: any): any {
    return this.http.post(this.url + 'lookup/create', lookup)
   }

   get_lookups() {
    return this.http.get(this.url + 'lookup/GetLookups')
   }

   get_lookup(id: any) {
    return this.http.get(this.url + 'lookup/GetLookup/' + id)
   }

   update(lookup: any) {
    return this.http.post(this.url + 'lookup/Update', lookup)
   }

  delete(id: any) {
    return this.http.delete(this.url + 'lookup/delete/' + id)
  }

  load_lookup_data() {
    return this.http.get(this.url + 'lookup/Load_title')
   }
}
