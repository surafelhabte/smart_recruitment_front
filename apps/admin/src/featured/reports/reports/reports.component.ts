import { Component, OnInit } from '@angular/core';
import { ReportService } from 'apps/admin/src/Shared/Services/report.service';

@Component({
  selector: 'smart-recruitment-frontend-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  public columnsToDisplay: any[] = [];
  public GridData: any;
  public saved_report: any;
  public incomingToolbar = [{ text: 'ExportExcel', prefixIcon: "e-Excel_Export", tooltipText: 'Export' }, { text: 'Print', tooltipText: 'Print' },];

  constructor(Service: ReportService) { 
    if ( Service.saved_report !== undefined) {
      this.saved_report = Service.saved_report;
      Service.saved_report = undefined;
    }
  }

  ngOnInit() { }

  public loadData(event) {
    event.options.fields.forEach(element => {
      this.columnsToDisplay.push({
        field: element.field,
        headerText: element.header,
        textAlign: 'left',
        width: 120
      });
    });
    this.GridData = event.results;
  }

}
