import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormArray,
  FormControl,
  Validators
} from '@angular/forms';
import { ReportService } from 'apps/admin/src/Shared/Services/report.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'smart-recruitment-frontend-report-option',
  templateUrl: './report-option.component.html',
  styleUrls: ['./report-option.component.css']
})
export class ReportOptionComponent implements OnInit {
  @Input() saved_report: any;
  public form: FormGroup;
  public input = new FormData();
  public formSubmitted: any = false;
  public multiFields: Object = { text: 'label', value: 'filed' };
  public aggregate_criteria_operators: any[] = [
    {value:'Group By', text: 'Group By'},
    {value:'Max',text: 'Max()'},
    {value:'Min',text: 'Min()'},
    {value:'Sum',text: 'Sum()'},
    {value:'Count',text: 'Count()'},
    {value:'Avg',text: 'Average()'}
  ];
  public filter_criteria_operators: any = [
    { value :'both', text: 'Contain'},
    { value :'after', text: 'Start With'},
    { value :'before', text: 'End With'},
    { value :'=', text: 'Equals To'},
    { value :'!=', text: 'Not Equals To'},
    { value :'>', text: 'Greater Than From'},
    { value :'=', text: 'Greater Than or Equals To'},
    { value :'<', text: 'Less Than From'},
    { value :'<=', text : 'Less Than or Equals To'},
    { value :'<>', text: 'Between'}
  ];
  public columens: any[] = [];
  @Output()
  public GridInfo: EventEmitter<any> = new EventEmitter();
  public invalidClass: any = false;
  public isUpdate: boolean = false;
  public invalid_saveReport_Class: any = false;

  public default_request = {
    report_name: {},
    filter_criteria: {},
    aggregate_criteria: {},
    per_page: '20',
    currentPage: '0',
    sort_order: 'Asc',
    sort_by: '',
    fields: {},
    chart: {},
    event: '',
    chanageEvent: true
  };

  public System_Message : any = {show: false, content: '', cssStyle: ''};
  public aggregate_submitted: boolean = false;
  public filter_submitted: boolean = false;
  constructor(
    public service: ReportService,
    private router?: Router,
    private fb?: FormBuilder,
    private actRoute?: ActivatedRoute
  ) {
    this.form = this.fb.group({
      filter: this.fb.array([]),
      aggregate: this.fb.array([]),
      fieldToView : new FormControl(''),
      report_title: new FormControl('', Validators.required),
      report_description: new FormControl('')
    });
  }

  ngOnInit(): void {
    let sample_result =  {
      results: [
        {
          reg_no: 'NRC/AA/16/W/2036',
          full_name: 'ABABO GUTA ADUGNA',
          sex: 'Female',
          age: '21',
          disablity: 'No'
        },
        {
          reg_no: 'ANP/AA/16/W/0147',
          full_name: 'ABABU BEKELE SENBETA',
          sex: 'Male',
          age: '30',
          disablity: 'NO'
        },
        {
          reg_no: 'CON/AA/16/W/2680',
          full_name: 'ABABU MEKONEN ARGAW',
          sex: 'Male',
          age: '18',
          disablity: 'NO'
        }
      ],
      title: 'Candidate Report',
      options: {
        per_page: '20',
        currentPage: '0',
        sort_order: 'Asc',
        sort_by: 'full_name',
        fields: [
          { field: 'reg_no', header: 'Registration Number' },
          { field: 'full_name', header: 'Full Name' },
          { field: 'sex', header: 'Sex' },
          { field: 'age', header: 'Age' },
          { field: 'disablity', header: 'Disablity' }
        ]
      },
      available_field: [
        {
          label: 'Reg No',
          filed: 'reg_no'
        },
        {
          label: 'Full Name',
          filed: 'full_name'
        },
        {
          label: 'Sex',
          filed: 'sex'
        },
        {
          label: 'Age',
          filed: 'age'
        },
        {
          label: 'Disablity',
          filed: 'disablity'
        },
        {
          label: 'Nationality',
          filed: 'nationality'
        }
      ],
      sorted_field: [
        {
          label: 'Age',
          filed: 'age'
        },
        {
          label: 'Birth Date',
          filed: 'birth_date'
        },
        {
          label: 'Black Listed',
          filed: 'black_listed'
        }
      ],
      total_row: '28709'
    };
    const id = this.actRoute.snapshot.params.id;
    if (id !== undefined) {
      this.default_request.report_name = id;
      this.service.intialize(this.default_request).subscribe((data: any) => {
        this.columens = sample_result.available_field;
      });

      if (this.saved_report !== undefined) {
        this.isUpdate = true;
        this.process_saved_report(this.saved_report);
      } else {
        this.add_Aggregate();
        this.add_Filter();
      } 
      this.GridInfo.emit(sample_result);
    }
  }

  get get_fieldToView() {
    return this.form.get('fieldToView') as FormControl;
  }
  get get_filter() {
    return this.form.get('filter') as FormArray;
  }
  get get_aggregates() {
    return this.form.get('aggregate') as FormArray;
  }
  get get_report_title() {
    return this.form.get('report_title') as FormControl;
  }

  get_filter_FormGroup(index): FormGroup {
    const formGroup = this.get_filter.controls[index] as FormGroup;
    return formGroup;
  }

  get_aggregates_FormGroup(index): FormGroup {
    const formGroup = this.get_aggregates.controls[index] as FormGroup;
    return formGroup;
  }

  checkFormArrayError(formArray: FormArray): boolean {
    const FormArrayErrors: any[] = [];
    if (formArray.length !== 0) {
      formArray.controls.forEach(element => {
        if (element.invalid) {
          FormArrayErrors.push(true);
        }
      });
      if (FormArrayErrors.length === 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  Create_Filter(): FormGroup {
    return this.fb.group({
      fields: new FormControl('', Validators.required),
      operator: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });
  }

  add_Filter() {
    const LLA = this.Create_Filter();
    this.get_filter.push(LLA);
  }

  Remove_Filter(index: number) {
    const data = this.get_filter;
    data.removeAt(index);
  }

  Create_Aggregate(): FormGroup {
    return this.fb.group({
      function: new FormControl('', Validators.required),
      fields: new FormControl('', Validators.required)
    });
  }

  add_Aggregate() {
    const LLA = this.Create_Aggregate();
    this.get_aggregates.push(LLA);
  }

  Remove_Aggregate(index: number) {
    const data = this.get_aggregates;
    data.removeAt(index);
  }

  prepare_filter_criteria() {
    let filter_criteria: any[] = [];
    if(this.get_filter.controls.length > 0){
      this.get_filter.controls.forEach(element => {
        filter_criteria.push({
          fields: element.value.fields,
          operator: element.value.operator,
          value: element.value.value
        });
      });
      return filter_criteria;
    } else {
      return {};
    }
  }

  prepare_aggregate_criteria() {
    let aggregate_criteria: any[] = [];
    if(this.get_aggregates.controls.length > 0){
      this.get_aggregates.controls.forEach(element => {
        aggregate_criteria.push({
          function: element.value.function,
          fields: element.value.fields
        });
      });
      return aggregate_criteria;
    } else {
      return {};
    }
  }

  Send_Request(event: any) {
    this.default_request.event = event;
    this.default_request.fields = this.get_fieldToView.value;

    switch(event){
      case 'Select':
        this.default_request.fields = this.get_fieldToView.value;
      break;
      case 'aggregate':
        if (!this.checkFormArrayError(this.get_aggregates)) {
          this.aggregate_submitted = true;
          return;
        } else {
          this.default_request.aggregate_criteria = this.prepare_aggregate_criteria();
          this.default_request.fields = this.get_fieldToView.value;
          this.default_request.filter_criteria = this.prepare_filter_criteria();
        }
      break;
      case 'Filter':
        if (!this.checkFormArrayError(this.get_filter)) {
          this.filter_submitted = true;
          return;
        } else {
          this.default_request.aggregate_criteria = this.prepare_aggregate_criteria();
          this.default_request.fields = this.get_fieldToView.value;
          this.default_request.filter_criteria = this.prepare_filter_criteria();
        }
      break;
      case 'default':
        return;
      break;
    }

    this.service.send_request(this.default_request).subscribe((data: any[]) => {
      const myData = {
        results: [
          {
            reg_no: 'NRC/AA/16/W/2036',
            full_name: 'ABABO GUTA ADUGNA',
            sex: 'Female',
            age: '21',
            disablity: 'No'
          },
          {
            reg_no: 'ANP/AA/16/W/0147',
            full_name: 'ABABU BEKELE SENBETA',
            sex: 'Male',
            age: '30',
            disablity: 'NO'
          },
          {
            reg_no: 'CON/AA/16/W/2680',
            full_name: 'ABABU MEKONEN ARGAW',
            sex: 'Male',
            age: '18',
            disablity: 'NO'
          }
        ],
        title: 'Candidate Report',
        options: {
          per_page: '20',
          currentPage: '0',
          sort_order: 'Asc',
          sort_by: 'full_name',
          fields: [
            { field: 'reg_no', header: 'Reg no' },
            { field: 'full_name', header: 'Full Name' },
            { field: 'sex', header: 'Sex' },
            { field: 'age', header: 'Age' },
            { field: 'disablity', header: 'Disablity' }
          ]
        },
        available_field: [
          {
            label: 'Reg No',
            filed: 'reg_no'
          },
          {
            label: 'Full Name',
            filed: 'full_name'
          },
          {
            label: 'Sex',
            filed: 'sex'
          },
          {
            label: 'Age',
            filed: 'age'
          },
          {
            label: 'Nationality',
            filed: 'nationality'
          }
        ],
        sorted_field: [
          {
            label: 'Age',
            filed: 'age'
          },
          {
            label: 'Birth Date',
            filed: 'birth_date'
          },
          {
            label: 'Black Listed',
            filed: 'black_listed'
          }
        ],
        total_row: '28709'
      };
      this.GridInfo.emit(myData);
    });
  }

  save_report(){
    this.formSubmitted = true;
    if (this.get_report_title.errors) {
      this.invalid_saveReport_Class = true;    
      return;
    } else {
      this.invalidClass = false;
      this.formSubmitted = true;
      if(this.default_request.event === ''){
        this.System_Message = {show: true, content: 'Send the Request First to Save the Report.', cssStyle: 'info'};
        return;
      } else {
        this.input.append('report_title', this.form.controls.report_title.value);
        this.input.append('report_description', this.form.controls.report_description.value);
        this.input.append('report_body', JSON.stringify(this.default_request));
        this.input.append('report_for', this.actRoute.snapshot.params.id);
        if(!this.isUpdate){
          this.service.save_report(this.input)
          .subscribe((response: any) => {
            if(response.status) {
              this.router.navigate(['/'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
            } else {
              this.System_Message = {show: true, content: response.message, cssStyle: 'danger'};
            } 
          });
        } else {
          this.input.append('id', this.saved_report.id);
          this.service.update_report(this.input)
          .subscribe((response: any) => {
            if(response.status) {
              this.router.navigate(['/'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
            } else {
              this.System_Message = {show: true, content: response.message, cssStyle: 'danger'};
            } 
          });
        }
      }
    }
  }

  process_saved_report(data: any){
    let report_body = JSON.parse(data.report_body)

    if(!Array.isArray(report_body.aggregate_criteria)){
        this.add_Aggregate();
    } else {
        report_body.aggregate_criteria.forEach(element => {
          const aggregate_criteria = this.Create_Aggregate();
          aggregate_criteria.controls.fields.setValue(element.fields);
          aggregate_criteria.controls.function.setValue(element.function);
          this.get_aggregates.push(aggregate_criteria);
        });
    }

    if(!Array.isArray(report_body.filter_criteria)) {
      this.add_Filter();
    } else {
      report_body.filter_criteria.forEach(element => {
        const filter_criteria = this.Create_Filter();

        filter_criteria.controls.fields.setValue(element.fields);
        filter_criteria.controls.operator.setValue(element.operator);
        filter_criteria.controls.value.setValue(element.value);

        this.get_filter.push(filter_criteria);
      });
    }

    this.default_request.report_name = data.report_for;
    this.get_report_title.setValue(data.report_title);
    this.form.controls.report_description.setValue(data.report_description);
    this.get_fieldToView.setValue(report_body.fields);
  }

}