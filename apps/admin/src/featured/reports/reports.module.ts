import { NgModule } from '@angular/core';
import { ReportsComponent } from './reports/reports.component';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccordionModule } from '@syncfusion/ej2-angular-navigations';
import { ReportOptionComponent } from './report-option/report-option.component';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';
import { ViewSavedComponent } from './view-saved/view-saved.component';

const routes: Routes = [
  {
    path: 'generate/:id',
    component: ReportsComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Reports', role: 'manageReport' }
  },
  {
    path: 'view/saved',
    component: ViewSavedComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Saved Reports', role: 'saveReport' }
  }
];

@NgModule({
  imports: [
    BasicModule,
    NgbModule,
    AccordionModule,
    RouterModule.forChild(routes),
    DataGridModule
  ],
  declarations: [ReportsComponent, ReportOptionComponent, ViewSavedComponent],
  providers: [],

  exports: [RouterModule]
})
export class ReportsModule { }
