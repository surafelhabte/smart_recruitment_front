import { TestBed  } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { ReportService } from 'apps/admin/src/Shared/Services/report.service';
import { ViewSavedComponent } from './view-saved.component';


describe('View Saved Report component', () => {
    let Service: ReportService;
    let page: ViewSavedComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule, BasicModule],
        providers: [ReportService]
      });
      Service = TestBed.get(ReportService);
      page = new ViewSavedComponent(Service);
  });

    it(' : Should load all Saved Reports', () => {
      Service.get_saved_reports().subscribe((data: any) => {
        expect(page.reports.length).toBeGreaterThan(0);
   });

  });

});
