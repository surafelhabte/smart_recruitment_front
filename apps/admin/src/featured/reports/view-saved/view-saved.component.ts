import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';
import { ReportService } from 'apps/admin/src/Shared/Services/report.service';

@Component({
  selector: 'smart-recruitment-frontend-view-saved',
  templateUrl: './view-saved.component.html',
  styleUrls: ['./view-saved.component.css']
})
export class ViewSavedComponent implements OnInit {

  @ViewChild('notif', { static: true }) notif: ToastComponent
  public reports: any[] = [];
  public columensToDisplay: any[] = [];
  public searchField = 'Title';
  public incomingToolbar = [
    { text: 'Filter', tooltipText: 'Filter data' },
    { text: 'Search', tooltipText: 'Search Items' }
  ];
  public incomingCommand = { edit: true, delete: true };

  constructor(private Service: ReportService, private router?: Router) {
    this.columensToDisplay.push({
      field: 'report_title',
      headerText: 'Title',
      textAlign: 'center',
      width: 120
    });
    this.columensToDisplay.push({
      field: 'report_description',
      headerText: 'Description',
      textAlign: 'center',
      width: 120
    });
  }

  ngOnInit() {
    this.Service.get_saved_reports().subscribe((data: any[]) => {
      this.reports = data;
    });
  }

  edit($event) {
    this.Service.saved_report = $event;
    this.router.navigate(['reports/generate/' + $event.report_for]);
  }

  delete(item: any) {
    this.Service.delete_saved_report(item.id).subscribe((response: any) => {
      if (response.status === true) {
        this.notif.Show({ title: 'Success', content: response.message, cssClass: 'e-toast-success' })
        this.ngOnInit();
      } else {
        this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' })
      }
    });
  }

}
