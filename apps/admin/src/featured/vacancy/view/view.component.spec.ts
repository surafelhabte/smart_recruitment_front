import { TestBed  } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ViewComponent } from './view.component';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { VacancyService } from '../vacancy.service';


describe('View Vacancy component', () => {
    let Service: VacancyService;
    let page: ViewComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule, BasicModule],
        providers: [VacancyService]
      });
      Service = TestBed.get(VacancyService);
      page = new ViewComponent(Service);
  });

    it(' : Should load all Vacancies', () => {
      Service.get_vacancys().subscribe((data: any) => {
        expect(page.vacancies.length).toBeGreaterThan(0);
});

  });

});
