import { Component, OnInit, ViewChild } from '@angular/core';
import { VacancyService } from '../vacancy.service';
import { Router } from '@angular/router';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';

@Component({
  selector: 'smart-recruitment-frontend-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent
  public vacancies: any[] = [];
  public columensToDisplay: any[] = [];
  public searchField = 'Title';
  public incomingToolbar = [
    { text: 'Add', tooltipText: 'Add Items' },
    { text: 'Filter', tooltipText: 'Filter data' },
    { text: 'Search', tooltipText: 'Search Items' }
  ];
  public incomingCommand = ['edit', 'delete'];

  constructor(private Service: VacancyService, private router?: Router) {
    this.columensToDisplay.push({ field: 'Title', headerText: 'Title', textAlign: 'left', width: 120 });
    this.columensToDisplay.push({ field: 'Location', headerText: 'Location', textAlign: 'left', width: 70 });
    this.columensToDisplay.push({ field: 'Availability', headerText: 'Availability', textAlign: 'left', width: 70 });
    this.columensToDisplay.push({ field: 'Vacancy_Type', headerText: 'Vacancy Type', textAlign: 'left', width: 70 });
    this.columensToDisplay.push({ field: 'Term_Of_Employement', headerText: 'Term Of Employement', textAlign: 'left', width: 70 });
    this.columensToDisplay.push({ field: 'Dead_line', headerText: 'Deadline', textAlign: 'left', width: 55, format: 'yMd' });
    this.columensToDisplay.push({ field: 'Hired', headerText: 'Hired', textAlign: 'left', width: 40, displayAsCheckBox: 'true' });
  }

  ngOnInit() {
    this.Service.get_vacancys().subscribe((data: any[]) => {
      this.vacancies = data;
    });
  }

  edit($event) {
    this.router.navigate(['vacancy/edit/' + $event.id]);
  }

  addNew() {
    this.router.navigate(['vacancy/create']);
  }

  delete(item: any) {
    this.Service.delete(item.id).subscribe((response: any) => {
      if (response.status === true) {
        this.notif.Show({ title: 'Success', content: response.message, cssClass: 'e-toast-success' })
        this.ngOnInit();
      } else {
        this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' })
      }
    });
  }

}
