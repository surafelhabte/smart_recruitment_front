import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { VacancyService } from '../vacancy.service';
import { Router, ActivatedRoute } from '@angular/router';
import { JobRequirmentService } from '../../job-requirment/job-requirment.service';
import { SharedLookupService } from 'apps/admin/src/Shared/Services/shared-lookup.service';
import { Location } from '@angular/common';

@Component({
  selector: 'smart-recruitment-frontend-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public form: FormGroup;
  public formSubmitted = false;
  public invalidClass = false;
  public isUpdate = false;
  public currentEditingID: any;
  public jobId: any;
  public dateValue: Date;
  public id;
  public input = new FormData();

  public System_Message: any = { show: false, content: '', cssStyle: '' };
  public Location: any[] = [];
  public JobsId: any[] = [];
  public Vacancy_Type: any[] = [];
  public TermofEmployments: any[] = [];
  public fields: any = { value: 'value', text: 'text' };
  loadLookupData: any[] = ['VacancyType', 'TermOfEmployment', 'Location'];
  constructor(
    private Service: VacancyService,
    private Job_Service: JobRequirmentService,
    private fb?: FormBuilder,
    private router?: Router,
    private actRoute?: ActivatedRoute,
    private lookupService?: SharedLookupService,
    private location?: Location
  ) {
    this.form = this.fb.group({
      Title: ['', Validators.required],
      JobId: ['', Validators.required],
      Location: ['', Validators.required],
      Availability: ['', Validators.required],
      Vacancy_Type: ['', Validators.required],
      Term_Of_Employement: ['', Validators.required],
      DeadLine: ['', Validators.required],
      NumberOfRequired: ['', Validators.required]
    });
  }

  get get_Title() {
    return this.form.get('Title') as FormControl;
  }
  get get_JobId() {
    return this.form.get('JobId') as FormControl;
  }
  get get_Location() {
    return this.form.get('Location') as FormControl;
  }
  get get_Availability() {
    return this.form.get('Availability') as FormControl;
  }
  get get_Vacancy_Type() {
    return this.form.get('Vacancy_Type') as FormControl;
  }
  get get_TermOfEmployement() {
    return this.form.get('Term_Of_Employement') as FormControl;
  }
  get get_DeadLine() {
    return this.form.get('DeadLine') as FormControl;
  }
  get get_NumberOfRequired() {
    return this.form.get('NumberOfRequired') as FormControl;
  }
  
  ngOnInit() {
    this.load_jobId();
    this.id = this.actRoute.snapshot.params.id;
    if (this.id) {
      this.Service.get_vacancy(this.id).subscribe((data: any) => {
        this.get_Title.setValue(data.Title);
        this.get_JobId.setValue(data.JobId);
        this.get_Location.setValue(data.Location);
        this.get_Availability.setValue(data.Availability);
        this.get_Vacancy_Type.setValue(data.Vacancy_Type);
        this.get_TermOfEmployement.setValue(data.Term_Of_Employement);
        this.get_DeadLine.setValue(data.Dead_line);
        this.get_NumberOfRequired.setValue(data.NumberOfRequired);

        this.jobId = data.JobId;
        this.currentEditingID = data.id;
        this.isUpdate = true;
      });

    }

    this.lookupService.load_data(this.loadLookupData).subscribe((data: any) => {
      this.Vacancy_Type = data.VacancyType;
      this.TermofEmployments = data.TermOfEmployment;
      this.Location = data.Location;
    });

    this.load_jobId();
  }

  onSubmit(): any {
    if (!this.form.valid) {
      this.formSubmitted = true;
      this.invalidClass = true;
      this.System_Message = { show: true, content: 'please fill the required fields.', cssStyle: 'info' };
      return;
    } else {
      const formData = this.prepareSave();
      if (this.isUpdate) {
        return this.Service.update(formData).subscribe((response: any) => {
          if (response.status === true) {
            this.router.navigate(['/vacancy'], { state: { data: { title: 'Success', content: response.message, cssClass: 'e-toast-success' } } });
          } else {
            this.System_Message = { show: true, content: response.message, cssStyle: 'danger' };
          }
        });
      } else {
        return this.Service.create(formData).subscribe((response: any) => {
          if (response.status === true) {
            this.router.navigate(['/vacancy'], { state: { data: { title: 'Success', content: response.message, cssClass: 'e-toast-success' } } });
          } else {
            this.System_Message = { show: true, content: response.message, cssStyle: 'danger' };
          }
        });
      }
    }
  }

  load_jobId() {
    this.Job_Service.get_jobs().subscribe((data: any[]) => {
      if (data.length) {
        data.forEach(element => { this.JobsId.push({value: element.JobId, text: element.Position });});
      }
    });
  }

  private prepareSave(): any {
    if (this.form.valid) {
      if (this.id) {
        return {
          Title: this.get_Title.value,
          JobId:
            typeof this.jobId === 'object'
              ? this.jobId.itemData.value
              : this.jobId,
          Location: this.get_Location.value,
          Availability: this.get_Availability.value,
          Vacancy_Type: this.get_Vacancy_Type.value,
          Term_Of_Employement: this.get_TermOfEmployement.value,
          Dead_line: new Date(this.get_DeadLine.value + "UTC"),
          NumberOfRequired: this.get_NumberOfRequired.value,
          id: this.id
        };
      } else {
        return {
          Title: this.get_Title.value,
          JobId:
            typeof this.jobId === 'object'
              ? this.jobId.itemData.value
              : this.jobId,
          Location: this.get_Location.value,
          Availability: this.get_Availability.value,
          Vacancy_Type: this.get_Vacancy_Type.value,
          Term_Of_Employement: this.get_TermOfEmployement.value,
          Dead_line: new Date(this.get_DeadLine.value + "UTC"),
          NumberOfRequired: this.get_NumberOfRequired.value,
        };
      }
    } else return null;
  }

}
