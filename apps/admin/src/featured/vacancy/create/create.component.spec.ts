import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CreateComponent } from './create.component';
import { VacancyService } from '../vacancy.service';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { JobRequirmentService } from '../../job-requirment/job-requirment.service';

describe('Create Vacancy component', () => {
  let vacancy_Service: VacancyService;
  let job_Service: JobRequirmentService;
  let page: CreateComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, BasicModule]
    });

    // inject the service
    vacancy_Service = TestBed.get(VacancyService);
    job_Service = TestBed.get(JobRequirmentService);

    page = new CreateComponent(vacancy_Service, job_Service, new FormBuilder());
  });

  it('form should have a control', () => {
    expect(page.form.contains('Title')).toBeTruthy();
    expect(page.form.contains('JobId')).toBeTruthy();
    expect(page.form.contains('Location')).toBeTruthy();
    expect(page.form.contains('Availability')).toBeTruthy();
    expect(page.form.contains('Vacancy_Type')).toBeTruthy();
    expect(page.form.contains('Term_Of_Employement')).toBeTruthy();
    expect(page.form.contains('DeadLine')).toBeTruthy();
    expect(page.form.contains('NumberOfRequired')).toBeTruthy();
  });

  // validation
  it('form control should have a validation required', () => {
    const Title = page.form.get('Title');
    const JobId = page.form.get('JobId');
    const Location = page.form.get('Location');
    const Availability = page.form.get('Availability');
    const Vacancy_Type = page.form.get('Vacancy_Type');
    const Term_Of_Employement = page.form.get('Term_Of_Employement');
    const DeadLine = page.form.get('DeadLine');
    const NumberOfRequired = page.form.get('NumberOfRequired');

    Title.setValue('');
    expect(Title.valid).toBeFalsy();

    JobId.setValue('');
    expect(JobId.valid).toBeFalsy();

    Location.setValue('');
    expect(Location.valid).toBeFalsy();

    Availability.setValue('');
    expect(Availability.valid).toBeFalsy();

    Vacancy_Type.setValue('');
    expect(Vacancy_Type.valid).toBeFalsy();

    Term_Of_Employement.setValue('');
    expect(Term_Of_Employement.valid).toBeFalsy();

    DeadLine.setValue('');
    expect(DeadLine.valid).toBeFalsy();

    NumberOfRequired.setValue('');
    expect(NumberOfRequired.valid).toBeFalsy();
  });

  // expecting the correct(but faked) result: propery with value
  it('Should create vacancy successfull and redirect to vacancies list', () => {
    const vacancy = {
      Title: 'title',
      JobId: 'jobid',
      Location: 'location',
      Availability: 'Availability',
      Vacancy_Type: 'Vacancy_Type',
      Term_Of_Employement: 'Term_Of_Employement',
      Dead_line: '2012-01-02',
      NumberOfRequired: '2'
    };
    const Title = page.form.get('Title');
    const JobId = page.form.get('JobId');
    const Location = page.form.get('Location');
    const Availability = page.form.get('Availability');
    const Vacancy_Type = page.form.get('Vacancy_Type');
    const Term_Of_Employement = page.form.get('Term_Of_Employement');
    const DeadLine = page.form.get('DeadLine');
    const NumberOfRequired = page.form.get('NumberOfRequired');

    Title.setValue('title');
    JobId.setValue('jobid');
    Location.setValue('location');
    Availability.setValue('Availability');
    Vacancy_Type.setValue('Vacancy_Type');
    Term_Of_Employement.setValue('Term_Of_Employement');
    DeadLine.setValue('DeadLine');
    NumberOfRequired.setValue('NumberOfRequired');

    const router = TestBed.get(Router);
    const router_spy = spyOn(router, 'navigateByUrl');
    const window_spy = spyOn(window, 'alert');

    vacancy_Service.create(vacancy).subscribe((response: any) => {
      if (response.status == true) {
        expect(window_spy).toHaveBeenCalledWith([response.message]);
        expect(router_spy).toHaveBeenCalledWith(['vacancy/view']);
      } else {
        expect(window_spy).toHaveBeenCalledWith([response.message]);
      }
    });
  });
});
