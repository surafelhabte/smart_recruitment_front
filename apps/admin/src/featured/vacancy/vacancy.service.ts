import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VacancyService {
  private url = environment.baseUrl;

  constructor(private http?: HttpClient) { }

  create(vacancy: any): any {
    return this.http.post(this.url + 'vacancy/Create', vacancy);
  }

  get_vacancys() {
    return this.http.get(this.url + 'vacancy/GetVacancies');
  }

  get_vacancy(id: any) {
    return this.http.get(this.url + 'vacancy/GetVacancy/' + id);
  }

  update(vacancy: any) {
    return this.http.post(this.url + 'vacancy/Update', vacancy);
  }

  delete(id: any) {
    return this.http.delete(this.url + 'vacancy/Delete/' + id);
  }

  load_data(query: any) {
    return this.http.post(this.url + 'lookup/load_lookups', query);
  }

  getCurrentAssessment(vacacnyId: any) {
    return this.http.post(
      this.url + 'AssessmentSchedule/GetCurrentStep',
      vacacnyId
    );
  }

  /* this if for scheduling */
  getLastVacancy() {
    return this.http.get(this.url + 'Vacancy/Get_Vacancies_Have_NextStep');
  }

  get_vacancies_have_nextstep() {
    return this.http.get(this.url + 'vacancy/Get_Vacancies_Have_Nextstep')
  }
}
