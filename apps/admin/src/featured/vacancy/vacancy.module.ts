import { NgModule } from '@angular/core';
import { CreateComponent } from './create/create.component';
import { Routes, RouterModule } from '@angular/router';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { VacancyService } from './vacancy.service';
import { ViewComponent } from './view/view.component';
import { JobRequirmentService } from '../job-requirment/job-requirment.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';

const routes: Routes = [
  {
    path: 'create',
    component: CreateComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Create', role: "createVacancy" }
  },
  {
    path: 'edit/:id',
    component: CreateComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Edit', role: "editVacancy" }
  },
  {
    path: '',
    component: ViewComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'List', role: "viewVacancies" }
  }
];

@NgModule({
  declarations: [CreateComponent, ViewComponent],
  imports: [BasicModule, RouterModule.forChild(routes), DataGridModule, NgbModule],
  providers: [VacancyService, JobRequirmentService],
  exports: [RouterModule]
})
export class VacancyModule { }
