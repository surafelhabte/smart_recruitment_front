import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecruitService {

  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   load_applications(vacancy_Id: any) {
    return this.http.get(this.url + 'Recruit/Load_Applications/' + vacancy_Id)
   }

   save(evaluation: any) {
    return this.http.post(this.url + 'Recruit/Save', evaluation)
   }

   get_vacancies() {
    return this.http.get(this.url + 'Recruit/GetVacancies')
   }
}
