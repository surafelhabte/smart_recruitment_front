import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BasicModule } from '../../Shared/Modules/basic.module';
import { Routes, RouterModule } from '@angular/router';
import { ViewComponent } from './view/view.component';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { DialogModule } from '@syncfusion/ej2-angular-popups';

const routes: Routes = [
  {
    path: '',
    component: ViewComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: "List", role: "manageRecruitment" }
  },
];

@NgModule({
  imports: [
    BasicModule,
    NgbModule,
    DataGridModule,
    DialogModule,
    RouterModule.forChild(routes),
    NgbModule
  ],
  declarations: [ViewComponent],
  exports: [RouterModule]
})
export class NgbdTableCompleteModule { }
