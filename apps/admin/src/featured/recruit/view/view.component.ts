import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { RecruitService } from '../Recruit.service';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';


@Component({
   selector: 'app-recruit',
   templateUrl: './view.component.html',
   styleUrls: ['./view.component.css'],
  })
  
export class ViewComponent implements OnInit {
  @ViewChild('ejDialog', {static: false}) ejDialog: DialogComponent;
  @ViewChild('container', { read: ElementRef, static: false }) container: ElementRef;
  public targetElement: HTMLElement;

  public position = { X: 'center', Y: 'top' };
  public types: any [] = [
    {text: 'Hired', value: 'Hired'},
    {text: 'Rejected', value: 'Rejected'},
  ];

  public applications: any[] = [];
  public vacancies: any[] = [];

  public columens: any[] = [];

  public message: any = { show: false, content: '', cssStyle: '' };
  public fields: any = { value: 'value', text: 'text' };

  public toolbar = [{ text: 'Search', tooltipText: 'Search Items' }];
  public command = ['detail'];

  public loading: boolean = false;
  public vacancy_id: any; 
  public profile_detail: any = null;

  public vacancy = new FormControl('');
  public formgroup : FormGroup;
  public formSubmitted = false;


  constructor(public service: RecruitService, public router: Router) {
    this.formgroup = new FormGroup({
      option : new FormControl('', Validators.required)
    });
  }

  ngOnInit(): void {
    this.load_Vacancies();
    this.columens.push({field: 'Photo',headerText: 'Photo',textAlign: 'center',width: 40, img: true });
    this.columens.push({field: 'FirstName',headerText: 'First Name',textAlign: 'center',width: 90 });
    this.columens.push({field: 'Surname',headerText: 'Last Name',textAlign: 'center',width: 90 });
    this.columens.push({field: 'Gender',headerText: 'Gender',textAlign: 'center',width: 35 });
    this.columens.push({field: 'Hiring_Status',headerText: 'Hiring Status',textAlign: 'center',width: 45 });
  }

  load_Vacancies(){
    this.service.get_vacancies()
    .subscribe((data: any[]) => {
      if (data.length > 0) {
        data.forEach(element => { this.vacancies.push({value: element.value, text: element.text, date: element.date}); });
      }
    });
  }

  load_Applications(vacancy_id){
    this.service.load_applications(vacancy_id)
    .subscribe((data: any) => {
      if(data.status){
        this.applications = data.message.applications;
        this.vacancy_id = this.applications[0] !== undefined ? this.applications[0].Vacancy_Id : '';
        this.message = { show: false, content: '', cssStyle: '' };
     
      } else {
        this.applications = [];
        this.message = {show: true, content: data.message, cssStyle: 'info'};      
      }
    });
  }

  public initilaizeTarget: EmitType<object> = () => {
    this.targetElement = this.container.nativeElement.parentElement;
  }
  
  public hideDialog: EmitType<object> = () => {
    this.ejDialog.hide();
    this.formSubmitted = false;
  }

  public buttons: object = [
    { click: this.Save.bind(this),buttonModel: { content: 'Save', isPrimary: true} },
    { click: this.hideDialog.bind(this), buttonModel: { content: 'Cancel' } }
  ];

  ShowDetail(data){
    this.profile_detail = data;
    this.initilaizeTarget();
    this.ejDialog.show(); 
  }

  Save(){
    if(confirm('Are you sure you want to save ?')){
      if(this.formgroup.invalid){
        this.formSubmitted = true;
        return;

      } else {
        const application =  { id : this.profile_detail.id, Hiring_Status : this.formgroup.controls.option.value };
  
        return this.service.save({ Vacancy_Id: this.vacancy_id, Applications : JSON.stringify(application)})
        .subscribe((response: any) => {
          if(response.status === true){
            // this.router.navigate(['/offering'], { state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}} });
            this.ngOnInit();
          } else {
            this.message = { show: true, content: response.message, cssStyle: 'danger' };
          }          
        });

      }
    }
  }

}