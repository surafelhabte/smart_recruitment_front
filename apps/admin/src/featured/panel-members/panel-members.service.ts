import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PanelMembersService {
  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }

   create(Panel: any) {
    return this.http.post(this.url + 'Panel/Create', Panel)
   }

   update(Panel: any) {
    return this.http.post(this.url + 'Panel/Update', Panel)
   }

   gets() {
    return this.http.get(this.url + 'Panel/Gets')
   }

   get(id:any) {
    return this.http.get(this.url + 'Panel/Get/' + id)
   }

   delete(data: any) {
    return this.http.post(this.url + 'Panel/Delete',data)
   }

   remove(id: any) {
    return this.http.delete(this.url + 'Panel/Remove/' + id)
   }
   
   get_vacancies() {
    return this.http.get(this.url + 'Panel/GetVacancies')
   }

   get_steps(vac_id: any) {
    return this.http.get(this.url + 'Panel/GetSteps/' + vac_id)
   }

   get_employees(filterInfo) {
    return this.http.post(this.url + 'Panel/GetEmployees/', filterInfo);
  }

   get_panel_members(data) {
    return this.http.post(this.url + 'Panel/GetPanelMembers/', data);
  }



}
