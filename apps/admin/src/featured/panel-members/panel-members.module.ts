import { NgModule } from '@angular/core';
import { ViewComponent } from './view/view.component';
import { CreateComponent } from './create/create.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { PanelMembersService } from './panel-members.service';
import { DialogModule } from '@syncfusion/ej2-angular-popups';

const routes: Routes = [
  {
    path: '',
    component: ViewComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: "List", role: "viewPanelMembers"}
  },
  {
    path: 'create',
    component: CreateComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: "Create", role: "createPanelMembers"}
  },
  {
    path: 'edit/:id',
    component: CreateComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: "Create", role: "editPanelMembers"}
  },
];

@NgModule({
  declarations: [ViewComponent, CreateComponent],
  imports: [
    BasicModule,
    NgbModule,
    DataGridModule,
    DialogModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  providers: [PanelMembersService]

})
export class PanelMembersModule { }
