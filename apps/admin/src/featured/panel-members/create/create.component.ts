import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { PanelMembersService } from '../panel-members.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormArray, Validators, FormGroup } from '@angular/forms';
import { Query } from '@syncfusion/ej2-data';
import { FilteringEventArgs } from '@syncfusion/ej2-angular-dropdowns';
import { EmitType } from '@syncfusion/ej2-base';


@Component({
  selector: 'smart-recruitment-frontend-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public employees: any[] = [];
  public employees_exclude: any[] = [];

  public form: FormGroup;
  public formSubmitted = false;
  public id: any;
  public isUpdate:boolean = false;
  public vacancies: any;
  public steps: any;

  public fields: any = { text: 'text', value: 'value' };
  public message : any = {show: false, content: '', cssStyle: ''};

  constructor(private Service: PanelMembersService,private fb?: FormBuilder,private router?: Router,
    private actRoute?: ActivatedRoute,private location?: Location) {
    this.form = this.fb.group({
      vacancy_id: this.fb.control('',Validators.required),
      step: this.fb.control('',Validators.required),
      members: this.fb.array([])
    });
  }

  ngOnInit() {
    this.load_vacancies();
    this.employees_exclude.push('');

    this.id = this.actRoute.snapshot.params.id;
    if (this.id) {
      this.Service.get(this.id).subscribe((data: any) => {
        if(data.length > 0){
          this.load_steps(data[0].vacancy_id);
          data.forEach(element => { 
            this.Create(element); 
            this.employees.push({ value : element.value, text : element.text });
            this.form.controls.step.setValue(data[0].step);
            this.form.controls.vacancy_id.setValue(data[0].vacancy_id);          

          });
          this.isUpdate = true;
        }
      });
    } else {
      this.Create();
    }

  }

  onSubmit(): any {
    if (this.form.invalid) {
      this.formSubmitted = true;
      return;

    } else {
      if(!this.isUpdate){
        return this.Service.create(this.form.value).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/panel'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
  
          } else {
            this.message = {show: true, content: response.message, cssStyle: 'danger'};
          }
        });

      } else {
        return this.Service.update(this.form.value).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/panel'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
  
          } else {
            this.message = { show: true, content: response.message, cssStyle: 'danger' };
          }
        });
      }
    }
  }

  Create(data: any = null) {
    this.get_members.push(this.fb.group(
      { 
        id: [ data ? data.id : ''], 
        employee_id: [ data ? data.value : '', Validators.required] 
      }));
  }

  Remove(index: number, form: FormGroup) {
    if(confirm('are you sure you want to remove this panel member ?')){
      let id = form.controls.id.value;
      if(id !== ""){
        this.Service.remove(id).subscribe((response: any) => {
          if (response.status) {
            this.get_members.removeAt(index);
  
          } else {
            alert(response.message);
          }
        });
  
      } else {
        this.get_members.removeAt(index);
      }
    }
  }

  get_form_group(index): FormGroup{
    return this.get_members.controls[index] as FormGroup;
  }

  get get_members(): FormArray {
    return this.form.get('members') as FormArray;
  }

  cancel(){
    this.location.back();
  }

  load_vacancies(){
    this.Service.get_vacancies().subscribe((data: any) => {
      this.vacancies = data;
    });
  }

  load_steps(vac_id: any){
    this.Service.get_steps(vac_id).subscribe((data: any[]) => {
      this.steps = data;
    });
  }

  public onFilteringGuarantors: EmitType<any> = (e: FilteringEventArgs) => {
    let query = new Query();
    query = e.text != '' ? query.where('full_name', 'contains', e.text, true) : query;

    const filterInfo = { keyword: e.text, emp_id: this.employees_exclude };

    this.Service.get_employees(filterInfo).subscribe((data: any) => {
      e.updateData(data);
    });

  }

  public onSelect: EmitType<any> = (index,data) => {
    this.employees_exclude.splice(index + 1, 1, data.value);
  }


}
