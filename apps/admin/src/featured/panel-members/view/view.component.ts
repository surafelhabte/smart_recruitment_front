import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';
import { PanelMembersService } from '../panel-members.service';
import { Router } from '@angular/router';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';


@Component({
  selector: 'smart-recruitment-frontend-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent

  @ViewChild('ejDialog', { static: false }) ejDialog: DialogComponent;
  @ViewChild('container', { read: ElementRef, static: false }) container: ElementRef;
  public targetElement: HTMLElement;

  public position = { X: 'center', Y: 'top' };
  
  public panel_members: any[] = [];
  public columens: any[] = [];
  public searchField = 'Title';
  public toolbar = [
    { text: 'Add', tooltipText: 'Add Items' },
    { text: 'Search', tooltipText: 'Search Items' }
  ];
  public Command = ['detail','edit', 'delete'];
  public member_detail: any[];

  constructor(private Service: PanelMembersService, private router?: Router) {
    this.columens.push({ field: 'Title',headerText: 'Vacancy',textAlign: 'center',width: 90 });
    this.columens.push({ field: 'Step',headerText: 'Step',textAlign: 'center',width: 90 });
    this.columens.push({ field: 'total_members',headerText: 'Members',textAlign: 'center',width: 90 });
    this.columens.push({ field: 'date',headerText: 'Date',textAlign: 'center',width: 90 });

  }

  ngOnInit() {
    this.Service.gets().subscribe((data: any[]) => {
      this.panel_members = data;
    });
  }

  edit($event) {
    this.router.navigate(['panel/edit/' + $event.id]);
  }

  addNew() {
    this.router.navigate(['panel/create']);
  }

  delete(data: any) {
    this.Service.delete({ vacancy_id: data.vacancy_id, step: data.step }).subscribe((response: any) => {
      if (response.status === true) {
        this.notif.Show({ title: 'Success', content: response.message, cssClass: 'e-toast-success' })
        this.ngOnInit();
      } else {
        this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' })
      }
    });
  }

  public initilaizeTarget: EmitType<object> = () => {
    this.targetElement = this.container.nativeElement.parentElement;
  }
  
  public hideDialog: EmitType<object> = () => {
    this.ejDialog.hide();
  }

  public buttons: object = [
    { click: this.hideDialog.bind(this), buttonModel: {content: 'Cancel'} }
  ];

  ShowDetail(data){
    this.initilaizeTarget();
    this.Service.get_panel_members({ vacancy_id: data.vacancy_id, step: data.step }).subscribe((data: any[]) => {
      if(data.length > 0){
        this.member_detail = data;
        this.ejDialog.show(); 
      }

    });

  }

}
