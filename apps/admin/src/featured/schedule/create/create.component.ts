import { filter } from 'rxjs/internal/operators/filter';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { ScheduleService } from '../schedule.service';
import { Router, ActivatedRoute } from '@angular/router';
import { VacancyService } from '../../vacancy/vacancy.service';
import { Location } from '@angular/common';
import { SharedLookupService } from 'apps/admin/src/Shared/Services/shared-lookup.service';
@Component({
  selector: 'smart-recruitment-frontend-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public form: FormGroup;
  public formSubmitted = false;
  public invalidClass = false;
  public create_operation = true;
  public currentEditingID: any;
  public VacancyId: any;
  public currentStep: any;
  public id: any;

  public input = new FormData();

  public Schedule_Date: any[] = [];
  public Vacancies: any = [];
  public assessmentTypes: any[] = [];
  public Assessment_Location: any[] = [];
  public vacancyFields = { text: 'text', value: 'value' };
  public System_Message : any = {show: false, content: '', cssStyle: ''};
  public loadLookupData: any[] = ['Location'];
  public is_panel_required: boolean = false;
  public panel_members: any[] = [];

  constructor(
    private Service: ScheduleService,
    private vacancy_service: VacancyService,
    private fb?: FormBuilder,
    private router?: Router,
    private actRoute?: ActivatedRoute,
    private lookupService?: SharedLookupService,
    private location?: Location
  ) {
    this.load_Vacancies();
    this.Vacancies.length > 0 ? (this.vacancyFields = { text: 'text', value: 'value' }) : null;

    this.form = this.fb.group({
      Title: new FormControl('', Validators.required),
      VacancyId: new FormControl('', Validators.required),
      Schedule_Date: new FormControl('', Validators.required),
      Assesement_For: new FormControl('', Validators.required),
      Assesement_Location: new FormControl('', Validators.required),
      Send_Schedule: new FormControl(false)
    });
  }

  get get_Title() {
    return this.form.get('Title') as FormControl;
  }
  get get_VacancyId() {
    return this.form.get('VacancyId') as FormControl;
  }
  get get_Schedule_Date() {
    return this.form.get('Schedule_Date') as FormControl;
  }
  get get_Assesement_For() {
    return this.form.get('Assesement_For') as FormControl;
  }
  get get_Assesement_Location() {
    return this.form.get('Assesement_Location') as FormControl;
  }
  get get_Send_Schedule() {
    return this.form.get('Send_Schedule') as FormControl;
  }

  ngOnInit() {
    this.get_Assesement_For.disable();
    this.id = this.actRoute.snapshot.params.id;
    if (this.id) {
      this.Service.get_schedule(this.id).subscribe((data: any) => {
        this.get_Title.setValue(data.Title);
        this.get_VacancyId.setValue(data.VacancyId);
        this.get_Schedule_Date.setValue(data.Date);
        this.get_Assesement_For.setValue(data.AssesementFor);
        this.get_Assesement_Location.setValue(data.AssesementLocation);
        this.get_Send_Schedule.setValue(data.SendSchedule === 'True' ? true : false);

        this.VacancyId = data.VacancyId;
        this.currentEditingID = data.id;
        this.create_operation = false;
      });
    }

    this.lookupService.load_data(this.loadLookupData).subscribe((data: any) => {
      this.Assessment_Location = data.Location;
    });
  }

  onSubmit(): any {
    if(this.form.invalid || (this.is_panel_required && this.panel_members.length === 0)) {
      this.formSubmitted = true;
      this.invalidClass = true;
      return;

    } else {
      const formData = this.prepareSave();
      if (this.create_operation) {
        return this.Service.create(formData).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/schedule'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          } else {
            this.System_Message = {show: true, content: response.message, cssStyle: 'danger'};
          }
        });
      } else {
        return this.Service.update(formData).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/schedule'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          } else {
            this.System_Message = {show: true, content: response.message, cssStyle: 'danger'};
          }
        });
      }
    }
  }

  load_Vacancies() {
    this.vacancy_service.getLastVacancy().subscribe((data: any) => {

      if (data.length > 0) {
        data.forEach(element => {
          this.Vacancies.push({ text: element.text, value: element.value , panel: element.panel, step: element.Current_Step });
        });
      }
    });
  }

  private prepareSave(): any {
    if (this.id) {
      return {
        Title: this.get_Title.value,
        VacancyId: this.get_VacancyId.value,
        Date: new Date(this.get_Schedule_Date.value + "UTC"),
        AssesementFor: this.get_Assesement_For.value,
        AssesementLocation: this.get_Assesement_Location.value,
        SendSchedule: this.get_Send_Schedule.value ? 'True' : 'False',
        Step: this.currentStep,
        id: this.id
      };
    } else {
      return {
        Title: this.get_Title.value,
        VacancyId: this.get_VacancyId.value,
        Date: new Date(this.get_Schedule_Date.value + "UTC"),
        AssesementFor: this.get_Assesement_For.value,
        AssesementLocation: this.get_Assesement_Location.value,
        SendSchedule: this.get_Send_Schedule.value ? 'True' : 'False',
        Step: this.currentStep
      };
    }
  }

  onValueSelect(data) {
    const vacancyId = data.itemData.value;
    let currentAssessment = '';
    this.vacancy_service
      .getCurrentAssessment({ vacancyId: vacancyId })
      .subscribe((data: any) => {
        data ? (currentAssessment = data.Assessment) && (this.currentStep = data.Step)
             : this.System_Message = { show: true, content: 'You can not create a schedule for this vacancy, please check if you are on the last step!', cssStyle: 'danger'};
        
        this.get_Assesement_For.setValue(currentAssessment);

      }); 
      
      this.Vacancies.filter((ele) => {
        if(ele.value === vacancyId){
          ele.panel === '1' ? this.is_panel_required = true : this.is_panel_required = false; 
        }

      });

      this.Service.check_panel_members({ vacancy_id: vacancyId, step: data.itemData.step }).subscribe((data: any[]) => {
        this.panel_members = data;
      });

  }

}
