import { TestBed  } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CreateComponent } from './create.component';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { ScheduleService } from '../schedule.service';
import { VacancyService } from '../../vacancy/vacancy.service';


describe('Create Schedule component', () => {
    let Service: ScheduleService;
    let vacancy_Service: VacancyService;
    let page: CreateComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule, BasicModule],
        providers: [ScheduleService, VacancyService]
      });

      // inject the service
      Service = TestBed.get(ScheduleService);
      vacancy_Service = TestBed.get(VacancyService);

    page = new CreateComponent(Service, vacancy_Service, new FormBuilder);
    });

    it(': form should have a control', () => {
      expect(page.form.contains('Title')).toBeTruthy();
      expect(page.form.contains('VacancyId')).toBeTruthy();
      expect(page.form.contains('Schedule_Date')).toBeTruthy();
      expect(page.form.contains('Assesement_For')).toBeTruthy();
      expect(page.form.contains('Assesement_Location')).toBeTruthy();
      expect(page.form.contains('Send_Schedule')).toBeTruthy();
    });

     // validation
    it(': form control should have a validation required', () => {
    const Title = page.form.get('Title');
    const VacancyId = page.form.get('VacancyId');
    const Schedule_Date = page.form.get('Schedule_Date');
    const Assesement_For = page.form.get('Assesement_For');
    const Assesement_Location = page.form.get('Assesement_Location');
    

    Title.setValue('');
    expect(Title.valid).toBeFalsy();

    VacancyId.setValue('');
    expect(VacancyId.valid).toBeFalsy();
        
    Schedule_Date.setValue('');
    expect(Schedule_Date.valid).toBeFalsy();

    Assesement_For.setValue('');
    expect(Assesement_For.valid).toBeFalsy();

    Assesement_Location.setValue('');
    expect(Assesement_Location.valid).toBeFalsy();

    });

    // expecting the correct(but faked) result: propery with value
    it('Should create schedule successfull and redirect to schedules list', () => {
    const schedule = { 
        Title: 'title', 
        VacancyId: 'jobid', 
        Assesement_For : 'Interview',
        Assesement_Location : 'Sengatera',
        Send_Schedule: 'Availability',
        Schedule_Date : '2012-01-02'
    };
    const Title = page.form.get('Title');
    const VacancyId = page.form.get('VacancyId');
    const Assesement_For = page.form.get('Assesement_For');
    const Assesement_Location = page.form.get('Assesement_Location');
    const Send_Schedule = page.form.get('Send_Schedule');
    const Schedule_Date = page.form.get('Schedule_Date');
    

    Title.setValue('title');
    VacancyId.setValue('VacancyId');
    Assesement_For.setValue('Assesement_For');
    Assesement_Location.setValue('Assesement_Location');
    Send_Schedule.setValue('Send_Schedule');
    Schedule_Date.setValue('Schedule_Date');
    

    const router = TestBed.get(Router);
    const router_spy = spyOn(router, 'navigateByUrl');
    const window_spy = spyOn(window, 'alert');

    Service.create(schedule).subscribe((response: any) => {
        if(response.status == true){
            expect(window_spy).toHaveBeenCalledWith([response.message]); 
            expect(router_spy).toHaveBeenCalledWith(['schedule/view']);
        } else {
            expect(window_spy).toHaveBeenCalledWith([response.message]); 
        }
    });
});

it('Should update schedule successfull and redirect to schedules list', () => {
    const schedule = { 
        Title: 'title', 
        VacancyId: 'jobid', 
        Assesement_For : 'location',
        Assesement_Location : 'Sengatera',
        Send_Schedule: 'Availability',
        Schedule_Date : '2012-01-02'
    };
    const Title = page.form.get('Title');
    const VacancyId = page.form.get('VacancyId');
    const Assesement_For = page.form.get('Assesement_For');
    const Assesement_Location = page.form.get('Assesement_Location');
    const Send_Schedule = page.form.get('Send_Schedule');
    const Schedule_Date = page.form.get('Schedule_Date');
    

    Title.setValue('title');
    VacancyId.setValue('VacancyId');
    Assesement_For.setValue('Assesement_For');
    Assesement_Location.setValue('Assesement_Location');
    Send_Schedule.setValue('Send_Schedule');
    Schedule_Date.setValue('Schedule_Date');
    

    const router = TestBed.get(Router);
    const router_spy = spyOn(router, 'navigateByUrl');
    const window_spy = spyOn(window, 'alert');

    Service.create(schedule).subscribe((response: any) => {
        if(response.status == true){
            expect(window_spy).toHaveBeenCalledWith([response.message]); 
            expect(router_spy).toHaveBeenCalledWith(['schedule/view']);
        } else {
            expect(window_spy).toHaveBeenCalledWith([response.message]); 
        }
    });
});

it('Should load all vacancies', () => {
    vacancy_Service.get_vacancys().subscribe((data: any[]) => {
        expect(page.Vacancies.length).toBeGreaterThanOrEqual(0); 
    });
});

});
