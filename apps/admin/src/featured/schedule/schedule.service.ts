import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {
  private url = environment.baseUrl;

  constructor(private http?: HttpClient) {}

  create(schedule: any) {
    return this.http.post(this.url + 'AssessmentSchedule/Create', schedule);
  }

  get_schedules() {
    return this.http.get(this.url + 'AssessmentSchedule/GetSchedules');
  }

  get_schedule(id: any) {
    return this.http.get(this.url + 'AssessmentSchedule/GetSchedule/' + id);
  }

  update(schedule: any) {
    return this.http.post(this.url + 'AssessmentSchedule/Update', schedule);
  }

  delete(id: any) {
    return this.http.delete(this.url + 'AssessmentSchedule/Delete/' + id);
  }

  check_panel_members(data) {
    return this.http.post(this.url + 'AssessmentSchedule/CheckPanelMembers', data);
  }

}
