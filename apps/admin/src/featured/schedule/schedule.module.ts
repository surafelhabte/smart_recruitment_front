import { NgModule } from '@angular/core';
import { BasicModule } from '../../Shared/Modules/basic.module';
import { DataGridModule } from '@smart-recruitment-frontend/data-grid';
import { RouterModule, Routes } from '@angular/router';
import { ScheduleService } from './schedule.service';
import { CreateComponent } from './create/create.component';
import { VacancyService } from '../vacancy/vacancy.service';
import { ViewComponent } from './view/view.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuardGuard } from '../../Core/Security/auth-guard.guard';

const routes: Routes = [
  {
    path: 'create',
    component: CreateComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'Create', role: "createSchedules" }
  },
  {
    path: 'edit/:id',
    component: CreateComponent,
    canActivate: [AuthGuardGuard],
    data: {
      breadCrum: 'Edit', role: "editSchedules"
    }
  },
  {
    path: '',
    component: ViewComponent,
    canActivate: [AuthGuardGuard],
    data: { breadCrum: 'List', role: "viewSchedules" }
  }
];

@NgModule({
  declarations: [CreateComponent, ViewComponent],
  imports: [BasicModule, RouterModule.forChild(routes), DataGridModule, NgbModule],
  providers: [ScheduleService, VacancyService],
  exports: [RouterModule]
})
export class ScheduleModule { }
