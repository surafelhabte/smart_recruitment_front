import { Component, OnInit, ViewChild } from '@angular/core';
import { ScheduleService } from '../schedule.service';
import { Router } from '@angular/router';
import { ToastComponent } from 'libs/toast/src/lib/toast/toast.component';

@Component({
  selector: 'smart-recruitment-frontend-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  @ViewChild('notif', { static: true }) notif: ToastComponent
  public Schedules: any[] = [];
  public columensToDisplay: any[] = [];
  public searchField = 'Title';
  public incomingToolbar = [
    { text: 'Add', tooltipText: 'Add Items' },
    { text: 'Filter', tooltipText: 'Filter data' },
    { text: 'Search', tooltipText: 'Search Items' }
  ];
  public incomingCommand = ['edit', 'delete'];
  public System_Message: any = { show: false, content: '', cssStyle: '' };
  constructor(private Service: ScheduleService, private router?: Router) {
    this.columensToDisplay.push({
      field: 'Title',
      headerText: 'Title',
      textAlign: 'left',
      width: 170
    });
    this.columensToDisplay.push({
      field: 'VacancyTitle',
      headerText: 'Vacancy',
      textAlign: 'left',
      width: 150
    });
    this.columensToDisplay.push({
      field: 'AssesementFor',
      headerText: 'Assesement Type',
      textAlign: 'left',
      width: 120
    });
    this.columensToDisplay.push({
      field: 'Date',
      headerText: 'Schedule date',
      textAlign: 'left',
      width: 70
    });
    this.columensToDisplay.push({
      field: 'SendSchedule',
      headerText: 'Schedule sent?',
      textAlign: 'left',
      width: 70,
      displayAsCheckBox: 'true'
    });
  }

  ngOnInit() {
    this.Service.get_schedules().subscribe((data: any[]) => {
      this.Schedules = data;
    });
  }

  edit($event) {
    this.router.navigate(['schedule/edit/' + $event.id]);
  }

  addNew() {
    this.router.navigate(['schedule/create']);
  }

  delete(item: any) {
    this.Service.delete(item.id).subscribe((response: any) => {
      if (response.status === true) {
        this.notif.Show({ title: 'Success', content: response.message, cssClass: 'e-toast-success' })
        this.ngOnInit();
      } else {
        this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' })
      }
    });
  }
  
}
