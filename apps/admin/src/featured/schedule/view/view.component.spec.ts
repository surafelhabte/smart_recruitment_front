import { TestBed  } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ViewComponent } from './view.component';
import { BasicModule } from 'apps/admin/src/Shared/Modules/basic.module';
import { ScheduleService } from '../schedule.service';


describe('View Schedules component', () => {
    let Service: ScheduleService;
    let page: ViewComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule, BasicModule],
        providers: [ScheduleService]
      });
      Service = TestBed.get(ScheduleService);
      page = new ViewComponent(Service);
  });

    it(' : Should load all Schedules', () => {
      Service.get_schedules().subscribe((data: any) => {
        expect(page.Schedules.length).toBeGreaterThanOrEqual(0);
});

  });

});
